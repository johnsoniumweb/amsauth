<?php
//===============================================================================================================================================
// Generic Variables
//===============================================================================================================================================
$authAppName                    =   "AMS Authorization";                                        //Set Application name
$authUserViewName               =   "AMSAuth_vUsers_all";                                       //All user view (Do not use filtered inactive user view)
$authTOCTableName               =   "AMSAuth_TOC";                                              //TOC Table Name
$authCountriesTableName         =   "AMSAuth_Countries_2FA";                                    //2FA Countries Table Name
$authLogTableName               =   "AMSAuth_Log";                                              //Log Table
$authLogin                      =   "authLogin.html";

//Available variables from globals.php:
//$isProduction = false;
//$logoutPath = "";
//$rootpath = "";
//$caspioLibrary = "";

//===============================================================================================================================================
// Variables for global.php
//===============================================================================================================================================
$productionDomain               =   "agencymaniasolutions.com";
$testDomain                     =   "test.agencymaniasolutions.com";
$authPathVariable               =   "/authtest/";                                  // Part of URL String that seperates production from Test
$authProductionLogOut           =   "https://clarw315.caspio.com/folderlogout/";                // Set Caspio Log out path for production
$authTestLogOut                 =   "https://b6.caspio.com/folderlogout/";                      // Set Caspio Log out path for test
$caspioProduction               =   "https://clarw315.caspio.com/scripts/embed.js";                // Set Caspio Log out path for production
$caspioDev                      =   "https://b6.caspio.com/scripts/e1.js";                      // Set Caspio Log out path for test


//===============================================================================================================================================
// Variables authorization expiration (keepAlive.php & header.php)
//===============================================================================================================================================
$authExpiration                 =   60;                                                         // Time before auth expires with no activity in minutes.
$authExpirationCheck            =   10;                                                         // Time between auth checks in minutes.

//===============================================================================================================================================
// Limbo Variables
//===============================================================================================================================================
$authCurrentTOS                 =   "v1";                                                        // [v($i)|blank]Current TOS version place 'v' before and increment number. Blank means TOS not required
$authPasswordExpiration         =   90;                                                           // Days before password expiration.
$authMaxLoginAttempts           =   5;                                                           // Max login attempts before locking account
$authLockedDuration             =   5000;                                                          // lock out duration in minutes
$authLogout                     =   'security/AMSLogOut.php';                                   //Login page path
$authHomePage                   =   'home.html';                                                 //Home page
$authAppSessionEmailName        =   'AMSAuth';                                                   // Set appname for session Auth variables

//===============================================================================================================================================
// Locked Variables
//===============================================================================================================================================
$authLockedMessage               =   "This account is currently locked. If you believe you have reached this in error, or have made no attempts to login recently please contact your system administrator at <a href='mailto:support@agencymaniasolutions.com'>support@agencymaniasolutions.com</a>.";          // Current locked out message.
$authExceededMessage             =   "You have exceeded the login attempts for this account and have been locked out of the system. Please try again later.";   // Message on initial lockout.
$authPasswordExpirationMessage   =   "Your password has expired or has not been set. Please reset your password here: <a href='authPasswordReset.html?cbResetParam=1'>Reset Password</a>.";
$authNewUserMessage              =   "A password has not been set for this account. Please create a new account password by clicking here: <a href='authPasswordReset.html?cbResetParam=1'>Set Password</a>";
$authSSOFailMessage              =   "Your single sign on has failed. If this is your first time seeing this message please try again. Otherwise please contact support at: <a href='mailto:support@agencymaniasolutions.com'>support@agencymaniasolutions.com</a> ";

//===============================================================================================================================================
// 2-Factor Variables
//===============================================================================================================================================

$authCodeNumeric                 =   "alphaNumeric";                                                    // [numeric | noCaseAlphaNumeric | alphaNumeric] (blank) or numeric = numeric, else string [noCaseAlphaNumeric] = non case sensitive, [AlphaNumeric] = case sensitive;
$authCodeLength                  =   6;                                                                 // [int] default = 6, Sets length of 2-factor code.
$authCodeExpiration              =   10;                                                                //  [int] Time set in minutes.
$authFactorExpiration            =   90;                                                                //  [int] Time set in days.
$auth2FAMaxAttempts              =   5;
$auth2FactorText                 =   "In order to continue you will need to complete 2-Factor verification. To proceed send code:";

//===============================================================================================================================================
// Phone Number Update
//===============================================================================================================================================

$authSystemAdminName            =   "support@agencymaniasolutions.com";                             // This is the display name for the support mailto
$authSystemAdminEmail           =   "support@agencymaniasolutions.com";                             // This is the email for the support link mailto
$authPhoneNumberUpdateText      =   "In order to update this number you must first authenticate your old number. To do so click the 'verify' button bellow and a verification code will be sent to you. If you do not have access to your old number you will need to contact your system administrator here: <a href='mailto:".$authSystemAdminEmail."'>".$authSystemAdminName."</a> ";
