<?php

// namespace AgencyMania;

require_once($_SERVER['DOCUMENT_ROOT'].'/REST/caspio_class.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/getCaspioData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/add2EQM_fn.php');

// FUTURE: fold getCaspioData into the class? Shaun says no (at least not now)

// NOTE: getCaspioData() returns array of results
// NOTE: full results from Caspio methods is an object (due to json_encode() not being called with ASSOC param)

class CaspioDataManager
{
    // FUTURE: add 7.0 compatible typehinting (bool, float, int, string)
    // FUTURE: add 7.0 return types `function sum(float $a, float $b): float { return $a + $b; }`
    // FUTURE: add 7.1 compatible typehinting (iterable)
    // FUTURE: add 7.2 compatible typehinting (object)
    // FUTURE: get rid of the "row" hardcoding
    // FUTURE: get rid of the "Inactive" hardcoding

    // XXX: would be awesome if we could get git to tag this for us auto-magically
    private $ver = '2.1.1';

    // Caspio() object
    private $caspio;

    // configuration array
    private $config = [];

    // SESSION key to save the state in
    private $state_id = '';

    // cache for option list lookups
    private $option_list_cache = [];

    // boolean has authorization flag
    private $has_auth = NULL;

    // user defined callbacks
    private $callbacks = [];

    // these shouldn't update so we can capture them as part of the object
    private $caspio_apiurl;
    private $caspio_token;

    // boolean flag for debug mode
    private $debug = FALSE;

    /**
     * CaspioDataManager constructor.
     *
     * @param  array   $config
     * @throws Exception if could not be authenticated
     * @throws InvalidArgumentException if $config is invalid
     */
    function __construct(array $config)
    {
        // enforce a session status
        if (session_status() != PHP_SESSION_ACTIVE)
        {
            throw new Exception('Sessions MUST be active');
        }

        // check auth
        if ( !$this->hasAuth() )
        {
            throw new Exception('Could not authenticate to API subsystem');
        }

        // check config before assignment, MUST have 'setup' and 'row'
        //  - 'setup' MUST have 'tableName' and 'keyField'
        //  - 'row' MUST not be empty
        if ( !is_array($config) || empty($config) ) { throw new InvalidArgumentException('Invalid config: empty'); }
        if ( !key_exists('setup', $config) ) { throw new InvalidArgumentException('Invalid config: missing setup'); }
        if ( !key_exists('tableName', $config['setup']) ) { throw new InvalidArgumentException('Invalid config: missing tableName'); }
        if ( !key_exists('keyField', $config['setup']) ) { throw new InvalidArgumentException('Invalid config: missing keyField'); }
        if ( !key_exists('row', $config) ) { throw new InvalidArgumentException('Invalid config: missing row'); }
        if ( empty($config['row']) ) { throw new InvalidArgumentException('Invalid config: row empty'); }

        // config passed check, save for object use
        $this->config = $config;

        // save the key which we will store the state under
        $this->state_id = ( key_exists('sessionID', $config['setup']) && !empty($config['setup']['sessionID']) )
            ? $config['setup']['sessionID']
            : $config['setup']['tableName'];

        // check if these globals are available
        if ( !key_exists('Caspio_apiURL', $GLOBALS) || !key_exists('Caspio_token', $GLOBALS))
        {
            throw new Exception('Caspio Globals are unavailable');
        }

        $this->caspio_apiurl = $GLOBALS['Caspio_apiURL'];
        $this->caspio_token  = $GLOBALS['Caspio_token'];

        // instantiate a new Caspio object
        $this->caspio = new Caspio();
    }


    /**
     * CaspioDataManager destructor.
     */
    function __destruct()
    {
        // XXX: any cleanup we NEED to do?
        // unset($this->caspio); # closes the cUrl resource
    }


    /**
     * @param $level
     * @param $message
     * @param array $content
     */
    private function log($level, $message, array $content = [])
    {
        // IDEA: full PSR-3 Logger? (https://www.php-fig.org/psr/psr-3/)
        if (($level != 'DEBUG') || $this->debug)
        {
            error_log(sprintf('[%s] %s %s', strtoupper($level), $message, !empty($content) ? json_encode($content) : ''));
        }
    }


    /**
     * Checks if the visitor has authorization
     *
     * @return boolean
     */
    private function hasAuth()
    {
        if ( !is_null($this->has_auth) )
        {
            return $this->has_auth;
        }

        $this->has_auth = caspioAuth();

        return $this->has_auth;
    }


    /**
     * Set the DataManager debug mode
     *
     * @param  boolean $flag
     * @return void
     */
    public function setDebug($flag)
    {
        $this->debug = (bool) $flag;
    }


    /**
     * Process the input from the user, usually from the POST
     *
     * @param  array $post the data array
     * @return array the results of the processing
     */
    public function process(array $post)
    {
        // defined by DataTables/Editor
        $output = [ 'data' => [], 'options' => [], 'error' => '', 'fieldErrors' => [], 'files' => [], 'upload' => [] ];

        // TODO: basic sanitation of values in POST before use
        // IDEA: use the filter_* functions to sanitize the $post array

        // put POST info into $data and $params
        $data   = ( key_exists('data', $post) && is_array($post['data']) ) ? $post['data'] : [];
        $params = ( key_exists('params', $post) && is_array($post['params']) ) ? $post['params'] : [];

        // this matches the behaviour of DataTables Editor PHP library
        if (!key_exists('action', $post))
        {
            $post['action'] = 'read';
        }

        // if we are loading, we blank out the state
        if ($post['action'] == 'load')
        {
            $this->clearState();
        }

        // $state = $this->getState();

        // switch to support various actions
        //    load   : get the initial data set.
        //    create : DataTable Editor plugin makes this call when a record needs added.
        //    edit   : DataTable Editor plugin makes this call when a record needs updated.
        //    remove : DataTable Editor plugin makes this call when a record needs deleted.
        //    options: used for getting <option> values in a cascading select situation.
        //    get    : gets one or more data records using the id (or id's) passed in an array under 'params'
        //
        switch ($post['action'])
        {
            case 'read':
            # NOTE: both of these are AMS created
            case 'load':
            case 'get': // same as load but require IDS in the params

                if ($post['action'] == 'get')
                {
                    if (empty($params))
                    {
                        $output['error'] = 'No IDs were provided';
                        break;
                    }
                }

                $data = $this->loadData($post['action'], $params, $error);

                if ($data === FALSE || !empty($error))
                {
                    // determine error based on $data
                    $output['error'] = ($data === FALSE)
                        ? "Problem loading data: {$error} ({$GLOBALS['Caspio_message']})"
                        : $error;
                }
                else
                {
                    $output['data'] = $data;

                    // deal with the options
                    $options = [];

                    if (!empty($this->option_list_cache))
                    {
                        foreach ($this->option_list_cache as $id => $option_list)
                        {
                            $options[ $id ] = [];

                            foreach ($option_list as $option)
                            {
                                if (!$option['inactive'])
                                {
                                    $options[ $id ][] = $option;
                                }
                            }
                        }
                    }

                    $output['options'] = $options;
                }

                break;

            case 'create':
            case 'edit': // same as create, BUT uses updateData instead of addData

                // check if there is an issue with the data
                $errors = $this->isError($data, $params);

                if ($errors === FALSE)
                {
                    // counter
                    $i = 0;

                    // loop through the data and add/edit the records
                    foreach ($data as $key => $val) // TODO: rename this to something more meaningful?
                    {
                        $row = $val['row'];

                        if ($post['action'] == 'edit')
                        {
                            $result = $this->updateData($key, $row, $params, $error);
                        }
                        else
                        {
                            $result = $this->addData($row, $params, $error);
                        }

                        if ($result === FALSE || $result <= 0)
                        {
                            // concatenating, so determine separator
                            $sep = ($i > 0) ? ' | ' : ''; ## XXX: are the separators used to determine which record had the error?
                            $output['error'] .= $sep . ($result === FALSE)
                                ? $error
                                : 'Failure accessing database: ' . $result;
                        }
                        else
                        {
                            $output['data'][] = $result;
                        }

                        $i++;
                    }
                }
                else
                {
                    #$output['error'] = key_exists('error', $errors) ? $errors['error'] : '';
                    #$output['fieldErrors'] = key_exists('fieldErrors', $errors) ? $errors['fieldErrors'] : [];

                    // merge the error fields onto the existing output
                    $output = array_merge($output, $errors);

                    $output['data'] = []; // optional, as it should already be blank
                }

                break;

            case 'remove':

                $keys = array_unique( array_keys($data) );

                if ( !empty($keys) )
                {
                    $result = $this->deleteData($keys, $error);

                    if ($result === FALSE)
                    {
                        $output['error'] = $error;
                    }
                    else if ($result == count($keys))
                    {
                        // full deletion of all keys
                        $output['data'] = [];
                    }
                    else
                    {
                        // XXX: we had a partial delete
                        $this->log('CRITICAL', 'CaspioDataManger >> PARTIAL DELETION HAPPENED!');
                        // TODO: we really should get the remaining records based on the keys
                        //  ie; $this->loadData('get', $data, $error); ???
                        $output['data'] = [];
                    }
                }

                break;

            # NOTE: another AMS creation
            case 'options':

                $options = $this->getOptions($params, $error);

                if ($options === FALSE)
                {
                    $output['error'] = $error;
                }
                else
                {
                    $output['options'] = $options;
                }

                // only when we are asking for just options can we remove the data field
                unset($output['data']);

                break;

            case 'upload':
                // FUTURE: deal with a file upload(s)
                // break;

            default:
                ## FAILURE
                $output['error'] = 'Invalid Action given. Contact support@agencymaniasolutions.com.';
        }

        // remove unused parts of the output
        if (key_exists('options', $output) && empty($output['options'])) unset($output['options']);
        if (key_exists('error', $output) && empty($output['error'])) unset($output['error']);
        if (key_exists('fieldErrors', $output) && empty($output['fieldErrors'])) unset($output['fieldErrors']);
        if (key_exists('files', $output) && empty($output['files'])) unset($output['files']);
        if (key_exists('upload', $output) && empty($output['upload'])) unset($output['upload']);

        return $output;
    }


    /**
     * Get the full state from storage
     *
     * @return array
     */
    private function getState()
    {
        return $_SESSION[ $this->state_id ];
    }


    /**
     * Set some state information
     *
     * @param  string $section the section to save the data under
     * @param  string $field   the key field to save the data
     * @param  mixed  $state   the information to save in the state
     * @return void
     */
    private function setState($section, $field, $state)
    {
        $_SESSION[ $this->state_id ][ $section ][ $field ] = $state;
    }


    /**
     * Clear the entire state of data
     *
     * @return void
     */
    private function clearState()
    {
        $_SESSION[ $this->state_id ] = [];
    }


    /**
     * Loads all data that matches the initial configuration
     *
     * @param  string      $action
     * @param  array       $params
     * @param  string      &$error_message
     * @return array|false
     */
    private function loadData($action, array $params, &$error_message)
    {
        // build the select clause
        $select = sprintf('%s AS DT_RowID, *', $this->config['setup']['keyField']);

        // get where clause: initial setup 'where' plus customized.
        $where  = key_exists('where', $this->config['setup']) ? $this->config['setup']['where'] . ' ' : '';
        $where .= $this->customizeWhere($action, $params);

        if ($action === 'get')
        {
            $where .= sprintf(' AND %s IN (%s)', $this->config['setup']['keyField'], implode(', ', $params) );
        }

        $query_array = [
            'table'  => $this->caspio_apiurl . rtrim($this->config['setup']['tableName'], '/') . '/rows',
            'token'  => $this->caspio_token,
            'select' => $select,
            'where'  => $where
        ];

        if ($this->debug)
        {
            $_SESSION['debug']['loadData'] = $query_array;
        }

        // Get the data
        $data = getCaspioData(
            $this->caspio,
            $query_array,
            $message
        );

        // if catastrophic query failure, return false
        if ($data === FALSE)
        {
            $error_message = 'Invalid call to Database. A message has been sent to the System Administrators. Please try to load this page again.';

            // IDEA: only have this trigger if debug is false
            alertSupport(
                "CaspioDataManager {$this->ver}",
                'Query failure on Load',
                sprintf('Message: %s<br>Select: %s<br>Where: %s<br>', $message, $select, $where)
            );

            return FALSE;
        };

        // build the array to send back to the client
        $ret = [];

        if (!empty($data))
        {
            foreach ($data as $row)
            {
                // format the data into the proper response the client is expecting (including option values)
                $ret[] = $this->getSingleRecordResponse($row, $params);
            }
        }
        else
        {
            // run this once just to get the options cache
            $this->getSingleRecordResponse( (object) [], $params);
        }

        return $ret;
    }


    /**
     * Inserts data into the database
     *
     * @param  array       $insert_data
     * @param  array       $params
     * @param  string      &$error_message
     * @return array|false data as inserted or FALSE on error
     */
    private function addData(array $insert_data, array $params, &$error_message)
    {
        // local nicety variables
        $setup = $this->config['setup'];

        // separate the custom and non-custom fields
        $selects = [];
        $insert_fields = [];
        $custom_fields = [];

        foreach ($this->config['row'] as $field => $cfg)
        {
            if (key_exists('custom', $cfg) && is_bool($cfg['custom']) && $cfg['custom'])
            {
                if (key_exists($field, $insert_data))
                {
                    $custom_fields[ $field ] = $insert_data[ $field ];
                }
            }
            else
            {
                // if custom not config'd or it is but it's false
                $selects[] = $field;

                if (key_exists($field, $insert_data) && !(key_exists('readonly', $cfg) && is_bool($cfg['readonly']) && $cfg['readonly']))
                {
                    $insert_fields[ $field ] = $insert_data[ $field ];  // array of fields that can be written to the DB.
                }
            }
        }

        // $insert_fields will not be updated by this function
        $reactivate_value = $this->getReactivationValue($insert_fields, $custom_fields, $params);

        // running preCreateOrEdit() after getReactivationValue() so legacy code's last pass over $insert_fields is what is used
        $this->preCreateOrEdit('create', $insert_fields, $custom_fields, $params); // FUTURE: should this be cancellable?
        $this->scrub($insert_fields);

        if ($this->debug)
        {
            $_SESSION['debug']['addData']['reactivate_value'] = $reactivate_value;
            $_SESSION['debug']['addData']['reactivate_field'] = $setup['reactivateUnique'];
        }

        // we actually insert here - the method may do an update instead of insert if record was inactive
        $key = ( key_exists('reactivateUnique', $setup) && (trim($setup['reactivateUnique']) !== '') )
            ? $this->addOrReactivate($setup['tableName'], $setup['keyField'], $insert_fields, $setup['reactivateUnique'], $reactivate_value, $setup['inactiveField'])
            : $this->addOrReactivate($setup['tableName'], $setup['keyField'], $insert_fields);

        if ($key === FALSE)
        {
            $error_message = 'Add Request failed. Contact support@agencymaniasolutions.com';
            return FALSE;
        }

        if ($key < 0)
        {
            $error_message = "Unexpected Error #: {$key} Message: {$GLOBALS['Caspio_message']}";
            return FALSE;
        }

        if ($key === 0)
        {
            $error_message = ( key_exists('message', $this->config) && key_exists('recordExists', $this->config['message']) )
                ? $this->config['message']['recordExists']
                : 'Record could not be added because this data already exists.';

            if ($this->debug)
            {
                $error_message .= json_encode($insert_fields).'|'.$reactivate_value;
            }

            return FALSE;
        }

        // build the query to get the record we just added
        $select = sprintf('%s AS DT_RowID, %s', $setup['keyField'], implode(', ', $selects));
        $where  = "{$setup['keyField']} = {$key}";
        $query  = ['q' => json_encode( compact('select', 'where') )];

        // intentionally left as ExecGetRequest not getCaspioData to limit change. This is only a single record get so the 1000 limit is not an issue
        $data = $this->caspio->ExecGetRequest(
            $this->caspio_apiurl . rtrim($setup['tableName'], '/') . '/rows',
            $this->caspio_token,
            $query
        );

        if ($data === FALSE)
        {
            $error_message = sprintf('Could not retrieve inserted data: %s %s', $GLOBALS['Caspio_message'], json_encode($query));
            return FALSE;
        }

        if (count($data->Result) === 0)
        {
            $error_message = 'Inserted record not found.';
            return FALSE;
        }

        // add back the custom fields
        $record = $data->Result[0];

        // we put the custom fields back on the result as if we inserted it
        foreach ($custom_fields as $key => $val)
        {
            $record->$key = $val;
        }

        $this->postCreateOrEdit('create', $record, $params);

        return $this->getSingleRecordResponse($record, $params);
    }


    /**
     * Updates records in the database
     *
     * @param  integer     $pk_value       The Primary Key values for the record to update
     * @param  array       $update_data
     * @param  array       $params
     * @param  string      &$error_message
     * @return array|false data as updated or FALSE on error
     */
    private function updateData($pk_value, array $update_data, array $params, &$error_message)
    {
        // update a record

        ## XXX: update to grab pk_value from the data array?

        $table  = $this->caspio_apiurl . rtrim($this->config['setup']['tableName'], '/') . '/rows';
        $where  = "{$this->config['setup']['keyField']} = {$pk_value}";
        $update = [ 'q' => json_encode(compact('where')) ];

        // separate the custom and non-custom fields
        $selects = [];
        $update_fields = [];
        $custom_fields = [];

        foreach ($this->config['row'] as $field => $cfg)
        {
            if (key_exists('custom', $cfg) && is_bool($cfg['custom']) && $cfg['custom'])
            {
                if (key_exists($field, $update_data))
                {
                    $custom_fields[ $field ] = $update_data[ $field ];
                }
            }
            else
            {
                // if custom not config'd or it is but it's false
                $selects[] = $field;

                if (key_exists($field, $update_data) && !(key_exists('readonly', $cfg) && is_bool($cfg['readonly']) && $cfg['readonly']))
                {
                    $update_fields[ $field ] = $update_data[ $field ];  // array of fields that can be written to the DB.
                }
            }
        }

        $this->preCreateOrEdit('edit', $update_fields, $custom_fields, $params); // we don't care about return for edit
        $this->scrub($update_fields);

        // update the data; FieldArray is blank if the update is just to a custom field (which is wierd but possible with inline editing)
        if ( !empty($update_fields) )
        {
            $count = $this->caspio->ExecUpdateRequest($table, $this->caspio_token, $update, $update_fields);

            if ($this->debug)
            {
                $_SESSION['debug']['updateData']['count'] = $count;
                $_SESSION['debug']['updateData']['update'] = $update;
                $_SESSION['debug']['updateData']['update_fields'] = $update_fields;
            }

            if ( ($count === FALSE) || ($count == -1) )
            {
                // BUG: in a table with a unique field, editing a record to a value that already exists will throw this error
                //  Caspio_message will be 'Data request failure: 400'
                $error_message = sprintf('Could not update: %s %s %s', json_encode($update), json_encode($update_fields), $GLOBALS['Caspio_message']);
                return FALSE;
            }
        }

        // build the select clause
        $select = sprintf('%s AS DT_RowID, %s', $this->config['setup']['keyField'], implode(', ', $selects));
        $limit = 1000;
        $query = [ 'q' => json_encode(compact('select', 'where', 'limit')) ];

        // intentionally left as ExecGetRequest not getCaspioData to limit change. This is only a single record get so the 1000 limit is not an issue
        $data = $this->caspio->ExecGetRequest($table, $this->caspio_token, $query);

        if ($data === FALSE)
        {
            $error_message = sprintf('Could not retrieve updated data: %s %s', $GLOBALS['Caspio_message'], json_encode($query));
            return FALSE;
        }

        if (count($data->Result) === 0)
        {
            $error_message = 'Updated record not found.';
            return FALSE;
        }

        // add back the custom fields
        $record = $data->Result[0];

        foreach ($custom_fields as $key => $val)
        {
            $record->$key = $val;
        }

        $this->postCreateOrEdit('edit', $record, $params);

        return $this->getSingleRecordResponse($record, $params);
    }


    /**
     * Deletes or Inactivates records in the database
     *
     * @param  integer[]     $pk_values      the primary key values of rows to be removed
     * @param  string        &$error_message message on error
     * @return integer|false count of rows removed or FALSE on error
     */
    private function deleteData(array $pk_values, &$error_message)
    {
        // we know the table, remove/inactivate the keys
        $table = $this->caspio_apiurl . rtrim($this->config['setup']['tableName'], '/') . '/rows';
        $query = ['q' => json_encode([
            'where' => sprintf('%s in (%s)', $this->config['setup']['keyField'], implode(',', $pk_values))
        ])];

        if (key_exists('inactiveField', $this->config['setup']) && ($this->config['setup']['inactiveField'] !== ''))
        {
            // if Inactive name is NOT blank, Update to "Inactivate"
            $count = $this->caspio->ExecUpdateRequest(
                $table,
                $this->caspio_token,
                $query,
                [ $this->config['setup']['inactiveField'] => TRUE ]
            );
        }
        else
        {
            // if Inactive name is blank, then physically delete
            $count = $this->caspio->ExecDeleteRequest( $table, $this->caspio_token, $query );
        }

        if ($count === FALSE)
        {
            $error_message = "Database Error attempting Delete: {$GLOBALS['Caspio_message']}";
            return FALSE;
        }

        return $count;
    }


    /**
     * Add a new record or reactivate an inactive record
     *
     * @param string $table_name        the Caspio table/view in the form \tables\filename\
     * @param string $keyFieldName      the table key field name (i.e UserKey). The value of this field will be returned back to you
     * @param array  $fieldValueArray   the field value array that has data for update/insert. must have every field necessary for adding a record.
     * @param string $uniqueFieldName
     *      Either holds the unique_field name that will be used to see if it already exists...
     *      Or a '*' to indicate a custom where statement (that will return a unique record) will be passed as the unique field value.
     *      Or is an empty string which indicates to just do a blind add.
     * @param string $uniqueFieldValue
     *      If uniqueFieldName is provided, this is REQUIRED, otherwise blank string
     *      This is the value (i.e. 'shaun@agencymaniasolutions.com' that will be used to try and find the record to reinstate.
     * @param string $inactiveFieldName name of the field that indicates active or inactive. Default = 'Inactive'
     * @return bool|int integer > 0 on success, everything else is a failure
     *  Boolean : FALSE (boolean) means there was a serious error attempting to call the caspio API. Likely because the SQL statement was wrong or we couldn't authenticate.
     *  integer = 0 : not added because a record with that unique value already exists AND is not inactivated.
     *  integer > 0 : the value of the field (identified by the "keyFieldName" parameter) for the record added.
     *  integer =  -1 : Couldn't authenticate to the caspio API
     *  integer =  -2 : Update was attempted to reinstate but the query failed. See $Caspio_message
     *  integer =  -3 : Add was attempted but the query failed. See $Caspio_message
     *  integer =  -4 : Query to see if unqique key existed failed. See $Caspio_message
     *  integer =  -5 : More than one record returned.
     *  integer =  -6 : Record was added but the return key was not returned -- probably because it is invalid.
     */
    private function addOrReactivate($table_name, $keyFieldName, $fieldValueArray, $uniqueFieldName = '', $uniqueFieldValue = '', $inactiveFieldName = 'Inactive')
    {
        // XXX: this seems redundant
        if (!$this->hasAuth())
        {
            return -1;
        }

        // local nicety variables
        $table_path = $this->caspio_apiurl . rtrim($table_name, '/') . '/rows';

        $uniqueWhere = '';

        // if an asterisk is passed, then the field value will be SQL for the WHERE clause
        if (trim($uniqueFieldName) === '*')
        {
            $uniqueWhere = $uniqueFieldValue;
        }
        else if (strpos($uniqueFieldName, '|') !== FALSE)
        {
            $arr1 = explode('|', $uniqueFieldName);
            $arr2 = explode('|', $uniqueFieldValue);
            $tmp = [];

            foreach ($arr1 as $key => $val)
            {
                $tmp[] = "{$val} = '{$arr2[ $key ]}'";
            }

            $uniqueWhere = implode(' AND ', $tmp);
        }
        else if (trim($uniqueFieldName) !== '')
        {
            $uniqueWhere = "{$uniqueFieldName} = '{$uniqueFieldValue}'";
        }

        if ($this->debug)
        {
            $_SESSION['debug']['addOrReactivate']['uniqueWhere'] = $uniqueWhere;
        }

        // if a uniqueFieldName is provided, then try to update to reactivate first
        if (trim($uniqueWhere) !== '')
        {
            // check if the record already exists
            $get_params = ['q' => json_encode([
                'select' => "{$keyFieldName} AS xyzKey, {$inactiveFieldName} AS xyzInactive",
                'where' => $uniqueWhere
            ])];

            // DB call to check if the record exists
            $check = $this->caspio->ExecGetRequest(
                $table_path,
                $this->caspio_token,
                $get_params
            );

            if ($this->debug)
            {
                $_SESSION['debug']['addOrReactivate']['get_params'] = $get_params;
            }

            if ($check === FALSE )
            {
                $GLOBALS['Caspio_message'] .= "<br>".json_encode($get_params);

                return -4;
            }

            // if it exists and it is not inactive, then return back that this is a duplicate.
            if (!empty($check->Result) && !$check->Result[0]->xyzInactive)
            {
                return 0;  // means the record was found.
            }
            // else if(!empty($check->Result) && count($check->Result) > 1) return -5;


            // if it exists, just reactivate it using update
            if (!empty($check->Result))
            {
                $get_params = ['q' => json_encode([
                    'where' => "{$keyFieldName} = '{$check->Result[0]->xyzKey}'"
                ])];

                $fieldValueArray[ $inactiveFieldName ] = FALSE;  // make sure inactive field name is set to false;

                // DB call to reactivate the record
                $result = $this->caspio->ExecUpdateRequest(
                    $table_path,
                    $this->caspio_token,
                    $get_params,
                    $fieldValueArray
                );

                if (($result === FALSE) || ($result < 0))
                {
                    // unsuccessful reactivation
                    return -2;
                }

                // return the key value of the reactivated record
                return $check->Result[0]->xyzKey;
            }
        }

        // If we get to here, we know we just need to add the record because  either
        //    1)  There was no unique field to test for
        //       -- OR --
        //    2)  The unqiue key value was not found so the record doesn't exist.
        $result = $this->caspio->ExecInsertRequest(
            $table_path,
            $this->caspio_token,
            $fieldValueArray,
            $keyFieldName
        );

        if ($result === FALSE)
        {
            return -3;
        }

        return $result;  // in this case the $result contains the value of the $keyFieldName
    }


    /**
     * Get all the field options available based on the configuration
     *
     * @param  array       $params
     * @param  string      &$error_message
     * @return array|false
     */
    private function getOptions(array $params, &$error_message)
    {
        $ret = [];

        // make sure the changed field name and ID from previous field were passed.
        if (empty($params) || !isset($params['fieldName']) || !isset($params['id']))
        {
            $error_message = 'Invalid syntax on attempt to get more options';

            return FALSE;
        }

        // remove the "row." from the prefix if necessary
        $field_name = preg_replace('#^row\.#i', '', $params['fieldName']);

        $id = $params['id'];

        // check for combinations of lookup/groupParent/cascadeValueField that need to be present for this to work.
        if (   !key_exists('row', $this->config)
            || !key_exists($field_name, $this->config['row'])
            || (!key_exists('lookup', $this->config['row'][ $field_name ])
                && !key_exists('groupParent', $this->config['row'][ $field_name ]))
            || (key_exists('lookup', $this->config['row'][ $field_name ])
                && !key_exists('cascadeValueField', $this->config['row'][ $field_name ]['lookup'])))
        {
            $error_message = "Invalid configuration: {$field_name}";
            return FALSE;
        }

        // remember this request in case it is needed for future cascading.
        $this->setState( 'groupCascade', $field_name, ['value' => $id] );  // fieldname without the row. is the current value;

        if (!key_exists('groupParent', $this->config['row'][ $field_name ]) && key_exists('lookup', $this->config['row'][ $field_name ]))
        {
            // process normally
            $lookup = $this->config['row'][ $field_name ]['lookup'];

            $data = $this->readOptions($field_name, $lookup); // removed $id, retrieves ALL options

            // if catastrophic query failure, return false
            if ($data === FALSE)
            {
                $error_message = "Invalid call to Database. Could not retrieve Options. Contact support@agencymaniasolutions.com".$GLOBALS['Caspio_message'].$field_name;
                return FALSE;
            }
            else
            {
                $previousLabel = '';
                $previousValue = '';
                $newArray = [];

                foreach ($data as $option)
                {
                    if ( (trim(strtolower($option->label)) != $previousLabel) && (trim(strtolower($option->value)) != $previousValue) )
                    {
                        $previousLabel = trim(strtolower($option->label));
                        $previousValue = trim(strtolower($option->value));
                        $newArray[] = $option;
                    }
                }

                $ret["row.{$field_name}"] = $newArray;
            }
        }

        if ($this->debug)
        {
            $_SESSION['debug']['getOptions'][ $field_name ] = json_encode($this->getState());
        }

        return $ret;
    }


    /**
     * Gets the cascading chain for a specific option field
     *
     * @param  string $field_name
     * @param  array  $lookup_config
     * @return array
     */
    private function readOptions($field_name, array $lookup_config)
    {
        // If this field is part of a cascading chain, we need to build a where clause by
        //  climbing back UP the chain to take into account parts of the cascade that are set.
        //  FIELD NAME for the cascade is in the $config, and the previous link is determined by the "cascadeValueField"
        //  VALUE field for each cascade field is maintained in the $config['state']['groupCascade']  field;
        $wheres = [];
        $iLookupConfig = $lookup_config;
        $iFieldName = $field_name;

        // standard where still applies
        if (key_exists('where', $lookup_config) && !empty($lookup_config['where']))
        {
            $wheres[] = $lookup_config['where'];
        }

        $state = $this->getState();

        while (key_exists('cascadeValueField', $iLookupConfig)) // as long as there is a cascade field, iterate
        {
            $cascadeFieldName = $iLookupConfig['cascadeValueField'];                    //   grab the name of the cascade field for convienienc

            if (key_exists($cascadeFieldName, $this->config['row'])                     // if the config field referenced by the cascadeField exists
                && key_exists('lookup', $this->config['row'][ $cascadeFieldName ]))     //   and it has it's own lookup configuration, we're in business
            {
                // BUG: race condition can happen here where $iFieldName is not yet set in the SESSSION
                if (!key_exists($iFieldName, $state['groupCascade']))
                {
                    $this->log('ERROR', 'Cascade Race Condition', $state['groupCascade']);
                }

                $value = $state['groupCascade'][ $iFieldName ]['value'];                // Value is held in the 'state' cache under the current field name
                $wheres[] = "{$cascadeFieldName} = '{$value}'";                         //   keep track of this FIELDNAME=VALUE combination

                $iLookupConfig = $this->config['row'][ $cascadeFieldName ]['lookup'];   // Advance the iteration back one link in the chain
                $iFieldName = $cascadeFieldName;                                        //   the current field name is now back one field too.
            }
        }

        if ($this->debug)
        {
            $_SESSION['debug']['readOptions'][ $field_name ] = json_encode($lookup_config);
        }

        // setup for the Caspio API call using information from the $config
        $query = [
            'table'   => $this->caspio_apiurl . rtrim($lookup_config['tableName'], '/') . '/rows',
            'token'   => $this->caspio_token,
            'select'  => $lookup_config['select'],
            'where'   => implode(' AND ', $wheres)
        ];

        if ( key_exists('orderby', $lookup_config) )
        {
            $query['orderby'] = $lookup_config['orderby'];
        }

        // Get and return the data.
        ## NOTE: $message will not be accessible
        return getCaspioData($this->caspio, $query, $message);
    }


    /**
     * Adds the related fields, options, and custom values to the data record
     *
     * @param  object $data_record
     * @param  array  $params
     * @return array
     */
    private function getSingleRecordResponse($data_record, array $params)
    {
        $record = [];
        $row = [];

        foreach ($this->config['row'] as $field => $cfg)
        {
            // know whether the current field was configured as 'custom'  (or virtual in the caspio terminology)
            $is_custom_field = key_exists('custom', $cfg)       // 'custom' option was provided
                && is_bool($cfg['custom'])                      // 'custom' option is a boolean
                && $cfg['custom'];                              // 'custom' option is true

            $is_refer_field = (key_exists('lookup', $cfg)
                && key_exists('groupParent', $cfg['lookup'])    // 'groupParent' option was provided
                && ($cfg['groupParent'] != ''));                // 'groupParent' option is not blank

            $is_list = (key_exists('lookup', $cfg)
                && key_exists('listField', $cfg['lookup'])
                && ($cfg['lookup']['listField'] === TRUE));

            // If this is a lookup field, update the options cache. (it will look first to see if it already exists).
            if (key_exists('lookup', $cfg))
            {
                $this->updateOptionListCache($field, $cfg);
            }

            if ($is_refer_field)
            {
                // BUG: race condition?
                $value = $this->option_list_cache[ $cfg['lookup']['groupParent'] ][ $field ];
            }
            else
            {
                // set the field value (either from the database record itself or custom value function)
                if ($is_custom_field)
                {
                    $value = $this->customizeCustomValue($field, $data_record, $params);
                }
                else if ($is_list && is_object($data_record->$field))
                {
                    // WARN: ListFields with nothing selected will NOT make it into here (they are NULL and thus not objects)
                    $value = array_keys( (array) $data_record->$field );
                }
                else
                {
                    $value = property_exists($data_record, $field) ? $data_record->$field : '';
                }
            }

            $row[ $field ] = $this->customizeFormat($field, $value);

            // if this is a lookup field, find the proper lookup label based on the value;
            if (key_exists('lookup', $cfg))
            {
                // Find the proper option value
                $option_values = [];

                ## NOTE: OnZeroLabel is inconsistent with the rest of the field names
                if ( ($value == 0) && key_exists('OnZeroLabel', $cfg['lookup']) )
                {
                    // WARN: NULL/empty values of ListFields will show up here because (NULL == 0)
                    $record[ $cfg['lookup']['id'] ] = [ 'label' => $cfg['lookup']['OnZeroLabel'] ];
                }
                else
                {
                    if ( !is_array($this->option_list_cache["row.{$field}"]) )
                    {
                        $record[ $cfg['lookup']['id'] ] = [
                            'label' => "Invalid Option Structure({$field}): " . json_encode( $this->option_list_cache["row.{$field}"] )
                        ];
                    }
                    else
                    {
                        if ($is_list)
                        {
                            // if this is a list field, get the selected options and put them in a comma delimited list
                            $selected = [];

                            if (!is_null($value))
                            {
                                foreach ($this->option_list_cache["row.{$field}"] as $option)
                                {
                                    if (in_array($option['value'], $value))
                                    {
                                        $selected[] = $option['label'];
                                    }
                                }
                            }

                            // return the appropriate label(s)
                            $option_values = [ 'label' => implode(', ', $selected) ];
                        }
                        else
                        {
                            foreach ($this->option_list_cache["row.{$field}"] as $option)
                            {
                                // if the value is found, return the appropriate label
                                if ($option['value'] == $value)
                                {
                                    $option_values = [ 'label' => $option['label'] ];
                                    break;
                                }
                            }
                        }

                        $record[ $cfg['lookup']['id'] ] = !empty($option_values) ? $option_values : ['label' => "No Valid Options for: {$field}"];
                    }
                }
            }
        }

        if (!empty((array) $data_record)) // casting to array since empty() doesn't work with objects
        {
            // need to put in the unique key
            $record['DT_RowId'] = $data_record->DT_RowID;
            $record['row'] = $row;
        }

        return $record;
    }


    /**
     * Populates the instance's cache for all the options of a certain field
     *
     * @param  string $field        field name
     * @param  array  $field_config the configuration for just this field
     * @return void
     */
    private function updateOptionListCache($field, array $field_config)
    {
        $field_name = "row.{$field}";
        $lookup_config = $field_config['lookup'];      // shortcut
        $do_inactives_exist = key_exists('inactiveField', $lookup_config) && (trim($lookup_config['inactiveField']) !== '');

        // if this is a lookup field and we don't already have the data we need cached, we need to go get it.
        // the cache key is the field name of the ID
        if ( !key_exists($field_name, $this->option_list_cache) )
        {
            if ( !$this->hasAuth() ) // XXX: this seems redundant and unnecessary
            {
                $this->option_list_cache[ $field_name ][] = [ 'label' => 'no value [API did not authenticate]', 'value' => 0 ];
            }
            else
            {
                // lookups can happen in two ways:  Records from a table, or from a field type in caspio called "ListField"
                if (key_exists('listField', $lookup_config) && is_bool($lookup_config['listField']) && $lookup_config['listField'])
                {
                    // if we are getting from a list Field, use the appropriate Caspio call
                    $rest_path = $this->caspio_apiurl . rtrim($lookup_config['tableName'], '/') . '/columns/' . $field;
                    $list_data = $this->caspio->ExecGetRequest($rest_path, $this->caspio_token, []);

                    // format this data into the same format as if we had retrieved the data from rows from a lookup table.
                    if ( ($list_data !== FALSE) && (count($list_data->Result) > 0) )
                    {
                        // faking a result set from Caspio
                        $data = [];

                        foreach ($list_data->Result->ListField as $key => $val)
                        {
                            $data[] = (object) [ 'value' => (int) $key, 'label' => $val, 'Inactive' => FALSE ];
                        }
                    }
                    else
                    {
                        ## NOTE: this will be FALSE or empty result set

                        // assign to data (now it looks just like we got it from a true lookup table rather than a List Field
                        $data = ($list_data === FALSE) ? $list_data : $list_data->Result;
                    }
                }
                else
                {
                    // so it's a normal lookup table (not a list field). Process accordingly.
                    $select  = $lookup_config['select'];
                    $orderby = ( key_exists('orderby', $lookup_config) && !empty($lookup_config['orderby']) )
                        ? $lookup_config['orderby']
                        : '';
                    $groupby = ( key_exists('groupby', $lookup_config) && !empty($lookup_config['groupby']) )
                        ? $lookup_config['groupby']
                        : '';

                    // if inactives are a thing, we need to extend the sql
                    if ($do_inactives_exist)
                    {
                        $select  .= sprintf(', %s AS Inactive', $lookup_config['inactiveField']);
                        $orderby .= ( key_exists('orderby', $lookup_config) && !empty($lookup_config['orderby']) )
                            ? sprintf(', %s', $lookup_config['inactiveField'])
                            : '';
                        $groupby .= ( key_exists('groupby', $lookup_config) && !empty($lookup_config['groupby']) )
                            ? sprintf(', %s', $lookup_config['inactiveField'])
                            : '';
                    }

                    $query = [
                        'table'   => $this->caspio_apiurl . rtrim($lookup_config['tableName'], '/') . '/rows',
                        'token'   => $this->caspio_token,
                        'select'  => $select,
                        'where'   => $lookup_config['where'],
                        'orderby' => $orderby,
                        'groupby' => $groupby
                    ];

                    $data = getCaspioData($this->caspio, $query, $message);
                }

                // Regardless of whether the option data came from a listField or rows from a lookup table,
                // process it the same because $data now looks identical
                if ($data === FALSE)
                {
                    $this->option_list_cache[ $field_name ][] = [ 'label' => 'Could not get Option Label.', 'value' => 0, 'inactive' => FALSE ];
                }
                else
                {
                    if (count($data) === 0)
                    {
                        $this->option_list_cache[ $field_name ][] = [ 'label' => 'No Options Found.', 'value' => 0, 'inactive' => FALSE ];
                    }
                    else
                    {
                        foreach ($data as $result)
                        {
                            // XXX: long way around just spitting all the fields into $out when `$out = (array) $result;` would work just as well in 99% of cases

                            $out = [
                                'label'     => $result->label,
                                'value'     => $result->value,
                                'inactive'  => $do_inactives_exist ? $result->Inactive : FALSE
                            ];

                            if (key_exists('parentValue', $result))
                            {
                                $out['parentValue'] = $result->parentValue;
                            }

                            if (key_exists('groupFields', $lookup_config) && is_array($lookup_config['groupFields']) && !empty($lookup_config['groupFields']))
                            {
                                foreach ($lookup_config['groupFields'] as $field)
                                {
                                    $out[ $field ] = $result->$field;
                                }
                            }

                            $this->option_list_cache[ $field_name ][] = $out;
                        }
                    }
                }
            }
        }
    }


    /**
     * Basic sanitation of data for input based on settings in the config
     *
     * @param  array &$submitted_data
     * @return void
     */
    private function scrub(array &$submitted_data)
    {
        $row = $this->config['row'];

        foreach ($submitted_data as $field => &$value)
        {
            if (key_exists($field, $row))
            {
                $type = key_exists('type', $row[ $field ])
                    ? strtolower($row[ $field ]['type'])
                    : 'text255';

                $do_strip_tags = !( key_exists('allowHTML', $row[ $field ]) && $row[ $field ]['allowHTML'] );

                switch ($type)
                {
                    case 'num' :
                    case 'number' :
                    case 'percent' :
                    case 'int' :
                    case 'integer' :

                        $value = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

                        if ($type == 'percent') { $value /= 100; }
                        if ($type == 'int' || $type == 'integer') { $value = intval($value); }

                        break;

                    case 'bool' :
                    case 'boolean' :
                        break;

                    case 'list' :
                        if (is_array($value))
                        {
                            foreach ($value as $i => $v)
                            {
                                $value[ $i ] = (int) $v;
                            }
                        }
                        break;

                    case 'text64000':
                        if ($do_strip_tags)
                        {
                            $value = strip_tags($value);
                        }
                        break;

                    case 'text255':
                    default:
                        if ($do_strip_tags)
                        {
                            $value = strip_tags($value);
                        }

                        if (strlen($value) > 255)
                        {
                            $value = substr($value, 0, 255);
                        }
                        break;
                }
            }
            ## XXX: unset if not in config? (probably not, DT_RowID would get scrubbed)
        }
    }


    /**
     * Set custom overrides for certain processing functions
     *
     * @param  string   $callback name of the callback being set
     * @param  callable $function either a string of the function name or a closure
     * @throws InvalidArgumentException if callback name invalid or function is not callable
     * @return void
     */
    public function setCallback($callback, callable $function)
    {
        $callbacks = ['isError', 'customizeWhere', 'customizeFormat', 'customizeCustomValue', 'preCreateOrEdit', 'postCreateOrEdit', 'getReactivationValue'];

        if (!in_array($callback, $callbacks))
        {
            throw new InvalidArgumentException('Invalid callback');
        }

        // check if callable (works for string of function name or anonymous function)
        if (!is_callable($function))
        {
            throw new InvalidArgumentException('Callback is not callable');
        }

        $this->callbacks[$callback] = $function;
    }


    /**
     * Run custom data validator callback
     *
     * @param  array       $data   data as supplied from DataTables
     * @param  array       $params
     * @return array|false array of error messages or false if no errors
     */
    private function isError(array $data, array $params)
    {
        ## NOTE: legacy caspioDataManagerV2 added passing $config as third parameter

        if (key_exists('isError', $this->callbacks) && !empty($this->callbacks['isError']))
        {
            return call_user_func($this->callbacks['isError'], $data, $params);
        }

        if (function_exists('caspioDataManager_isError'))
        {
            return caspioDataManager_isError($data, $params);
        }

        ## WARN: can not stack this AND a custom (so we lose this if we don't dupe in custom)
        ## IDEA: split into two functions (one basic that is always run, the other the custom)

        // our own basic validation
        $err = [
            'error' => '', // a general error message
            'fieldErrors' => [ // an array of specific fields (name) that had an issue (status)
                # ex; [ 'name' => 'FieldName', 'status' => 'The text was too long' ]
            ]
        ];

        $row = $this->config['row'];

        foreach ($data as $idx => $record)
        {
            foreach ($record['row'] as $field => $value)
            {
                if (key_exists($field, $row))
                {
                    $type = key_exists('type', $row[$field])
                        ? strtolower($row[$field]['type'])
                        : 'text255';

                    switch ($type)
                    {
                        ## IDEA: add email type?
                        case 'text64000':
                            if (strlen($value) > 64000)
                            {
                                $err['fieldErrors'][] = [
                                    'name' => "row.{$field}",
                                    'status' => 'Must be shorter than 64k characters'
                                ];
                            }
                            break;

                        case 'text255':
                            if (strlen($value) > 255)
                            {
                                $err['fieldErrors'][] = [
                                    'name' => "row.{$field}",
                                    'status' => 'Must be shorter than 255 characters'
                                ];
                            }
                            break;

                        default:
                            // noop
                            break;
                    }
                }
            }
        }

        return ( !empty($err['fieldErrors']) || !empty($err['error']) ) ? $err : FALSE;
    }


    /**
     * Get any customized where SQL
     *
     * @param  string $action
     * @param  array  $params
     * @return string
     */
    private function customizeWhere($action, array $params)
    {
        if (key_exists('customizeWhere', $this->callbacks) && !empty($this->callbacks['customizeWhere']))
        {
            return call_user_func($this->callbacks['customizeWhere'], $action, $params);
        }

        if (function_exists('caspioDataManager_customizeWhere'))
        {
            return caspioDataManager_customizeWhere($action, $params);
        }

        return '';
    }


    /**
     * Get the customized value of a field based on the data of the record
     *
     * @param  string $field
     * @param  object $data_record
     * @param  array  $params
     * @return mixed
     */
    private function customizeCustomValue($field, $data_record, array $params)
    {
        if (key_exists('customizeCustomValue', $this->callbacks) && !empty($this->callbacks['customizeCustomValue']))
        {
            return call_user_func($this->callbacks['customizeCustomValue'], $field, $data_record, $params, $this->option_list_cache);
        }

        if (function_exists('caspioDataManager_customizeCustomValue'))
        {
            // $field contains the name of the field without the "row." prefix
            // $dataRecord contains the data that was retrieved from the table (all fields)
            // $params is an option parameter set passed through ajax.
            // $this->option_list_cache is an array of options and option list;  $this->option_list_cache['fieldname'] = array("label"=>"Value", "label"=>"value)
            return caspioDataManager_customizeCustomValue($field, $data_record, $params, $this->option_list_cache);
        }

        // first check if the value is already in the data Record
        if ( !empty((array) $data_record) && property_exists($data_record, $field) )
        {
            return $data_record->$field;
        }

        // unaccounted form custom fields get a blank.
        return '';
    }


    /**
     * Get the customized format of a field
     *
     * @param  string $field
     * @param  mixed  $value
     * @return mixed
     */
    private function customizeFormat($field, $value)
    {
        if (key_exists('customizeFormat', $this->callbacks) && !empty($this->callbacks['customizeFormat']))
        {
            return call_user_func($this->callbacks['customizeFormat'], $field, $value, $this->config);
        }

        if (function_exists('caspioDataManager_customizeFormat'))
        {
            // $field contains the name of the field without the "row." prefix
            // $value to be formatted
            // $config
            return caspioDataManager_customizeFormat($field, $value, $this->config);
        }

        $ret = '';

        if ( !is_null($value) )
        {
            // set default value
            $ret = $value;

            /* ### EXAMPLE ONLY ###
            switch ($field)
            {
                case 'CreateDate':
                case 'LastUpdated':
                    if ( ($tstamp = strtotime($value)) !== FALSE )
                    {
                        $ret = date('Y-m-d', $tstamp);
                    }
                    break;
            }
            */
        }

        return $ret;
    }


    /**
     * pre add or update processing of custom fields
     *
     * This function is run right before a Caspio record is either added or updated.
     *
     * The primary function accomplished by this function:
     *   The field array can be manipulated to add new fields (useful on create especially) or modify existing values
     *
     * @param  string $action either 'create' or 'edit'
     * @param  array  &$submitted_data
     * @param  array  $custom_data
     * @param  array  $params
     */
    private function preCreateOrEdit($action, array &$submitted_data, array $custom_data, array $params)
    {
        if (key_exists('preCreateOrEdit', $this->callbacks) && !empty($this->callbacks['preCreateOrEdit']))
        {
            // would rather use call_user_func() but it does not handle references
            $this->callbacks['preCreateOrEdit']($action, $this->config, $submitted_data, $params);
        }

        if (function_exists('caspioDataManager_preCreateOrEditWithCustom'))
        {
            caspioDataManager_preCreateOrEditWithCustom($action, $this->config, $submitted_data, $custom_data, $params);
        }
        else if (function_exists('caspioDataManager_preCreateOrEdit'))
        {
            caspioDataManager_preCreateOrEdit($action, $this->config, $submitted_data, $params);
        }
    }


    /**
     * Gets the value of the fields concerning reactivating inactive records
     *
     * This function is run right before a Caspio record is added
     *
     * There are two things accomplished by this function:
     *   REQUIRED: It must return the value for the unique field used to reactivate an inactive record (or, blank, if it doesn't exist)
     *   OPTIONAL:  The field array can be manipulated to add new fields (useful on create especially) or modify existing values
     *
     * CUSTOMIZE? Only if you must concatenate multiple fields to get the unique value (common in share tables to concatenate two ID's to match a unique formula field)
     *
     * @param  array  &$submitted_data
     * @param  array  $custom_data
     * @param  array  $params
     * @return string the unique reactivation field value or empty string
     */
    private function getReactivationValue(array &$submitted_data, array $custom_data, array $params)
    {
        if (key_exists('getReactivationValue', $this->callbacks) && !empty($this->callbacks['getReactivationValue']))
        {
            return call_user_func($this->callbacks['getReactivationValue'], $this->config, $submitted_data, $params);
        }

        // We only have this here for backward compatibility; to be able to do a drop in replacement
        if (function_exists('caspioDataManager_preCreateOrEditWithCustom'))
        {
            return caspioDataManager_preCreateOrEditWithCustom('create', $this->config, $submitted_data, $custom_data, $params);
        }
        else if (function_exists('caspioDataManager_preCreateOrEdit'))
        {
            return caspioDataManager_preCreateOrEdit('create', $this->config, $submitted_data, $params);
        }

        // this only needs to be customized if you need to WEIRDLY concatenate multiple fields to get the
        // unique value needed to find a record to reactivate.
        $ret = '';

        if (key_exists('reactivateUnique', $this->config['setup'])  && !empty($this->config['setup']['reactivateUnique']))
        {
            // split the string on a | and iterate through the array building the value string
            $arr = explode('|', $this->config['setup']['reactivateUnique']);

            $tmp = [];

            foreach ($arr as $val)
            {
                if (key_exists($val, $submitted_data))
                {
                    // single field from the table can be use find record to reactivate.
                    // eg; if email is the unique field in the DB, then the email address in the record to be added
                    $tmp[] = $submitted_data[ $val ];
                }
            }

            $ret = implode('|', $tmp);
        }

        return $ret;
    }


    /**
     * This function is run right AFTER Caspio record is either added or updated.
     *
     * This is useful for triggering additional actions, such as sending emails/notices
     *
     * @param  string $action is either 'create' or 'edit'
     * @param  object $record is the data record read back from what was added or updated. On an add the key field is in $record->DT_RowID;
     * @param  array  $params
     * @return void
     */
    private function postCreateOrEdit($action, $record, array $params)
    {
        if (key_exists('postCreateOrEdit', $this->callbacks) && !empty($this->callbacks['postCreateOrEdit']))
        {
            call_user_func($this->callbacks['postCreateOrEdit'], $action, $this->config, $record, $params);
        }

        if (function_exists('caspioDataManager_postCreateOrEdit'))
        {
            caspioDataManager_postCreateOrEdit($action, $this->config, $record, $params);
        }
    }

}
