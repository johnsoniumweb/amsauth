<?php
ini_set('display_errors',1);
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/phpspreadsheet/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/getCaspioData.php');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;


class importExcelClass
{
    // DOCUMENTATION -- Meta documentation at bottom of class

    // Public uses settings
    public $dataArray = [];                 // public because this can be very large so don't want to duplicate space
    public $errorsArray = [];               // public because this can be very large so don't want to duplicate space
    public $spreadsheet;                    // spreadsheet object as defined by https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Spreadsheet.html

    // Operational Settings
    private $dataOnly = false;              // holds setting that determines whether spreadsheet formatting will be loaded.
    private $headerRow = 1;                 // holds setting that determines where header values are (data presumes to start in the next row)
    private $filename;                      // name of excel file
    private $sheetname = "";                // name of specific spreadsheet to import
    private $template = [];                 // template (see TEMPLATE - Documentation)

    // informational
    private $lastError = [
        'code'=>0,
        'message'=>''
    ];             // holds the most recent error code and message that can be inspected by the programmer


    private $sizeofErrorsArray = 0;         // number of rows in the errorsArray
    private $sizeofDataArray = 0;           // number of rows in the dataArray
    private $templateMissingRequired = [];  // array of strings with list of REQUIRED fields not found in spreadsheet (key is same key as $template)
    private $templateMissing = [];          // array of strings with list of fields not found in spreadsheet (key is same key as $template)
    private $unrecognizedHeaders = [];      // array of strings with list of columns in the spreadsheet not found in $template

    // configuration settings
    private $separator = "<|>";             // string used to separate concatenated fields for multi-field lookup. Changed by setSeparator();
    private $messageText = [
        "intTooBig" => "Number is > ",
        "intTooSmall" => "Number is < ",
        "intInvalid" => "Invalid number ",
        "numberTooBig" => "Number is > ",
        "numberTooSmall" => "Number is < ",
        "numberInvalid" => "Invalid number ",
        "dateTooBig" => "Date is after ",
        "dateTooSmall" => "Date is before ",
        "dateInvalid" => "Invalid date",
        "requiredValue" => "Required",
        "invalidSingleLookup" => "Value not valid",
        "invalidMultiLookup" => "Invalid combination of values",
        "invalidSheet" => "Sheet Name Not Found",
        "requiredField" => "Required Fields are Missing",
        "unrecognizedBool" => "Invalid value",

    ];           // error text values can be updated by user through setMessageText()

    // internal use
    private $templateMap = [];              // copy of template where the key is the column index
    private $isTemplateApplied = false;     // internal tracker to check whether the template has been applied.
    private $singleFieldLookupArray = [];   // holds the lookup data for single fields (one cell value needed to lookup)
    private $multiFieldLookupArray = [];    // holds the lookup date for multi fields (multiple cell values needed to concatenate
    private $newSpreadsheet = true;

    /**
     * importExcel constructor.
     * @param $filename
     * @param string $sheetname
     * @param array $template
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    function __construct($filename, $sheetname="", $template=[])
    {
        $this->spreadsheet = new Spreadsheet();
        $this->newSpreadsheet = true;
        $this->filename = $filename;
        $this->setTemplate($template);
        $this->sheetname = trim($sheetname);
    }

    /**
     * importExcel destructor
     */
    function __destruct()
    {
        if (isset($this->spreadsheet) && !$this->newSpreadsheet) {
            $this->spreadsheet->disconnectWorksheets();
            unset($this->spreadsheet);
        }

    }

    /**
     * setTemplate loads the template (field configuration) into the system
     * @param array $template
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function setTemplate($template = []) {
        if (is_array($template)) {
            $this->template = $template;
            if (!$this->newSpreadsheet)
                $this->applyTemplate();
        }

    }

    /**
     * @return array|bool
     */
    public function getMissingFields() {
        if ($this->isTemplateApplied)
            return $this->templateMissing;
        else {
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function getMissingRequiredFields() {
        if ($this->isTemplateApplied)
            return $this->templateMissingRequired;
        else {
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function getExtraneousFields() {
        if ($this->isTemplateApplied)
            return $this->unrecognizedHeaders;
        else {
            return false;
        }
    }

    /**
     * @return string[]
     */
    public function getSheetNames() {
        // array of strings with names of sheets
       return $this->spreadsheet->getSheetNames();
    }

    /**
     * @return array
     */
    public function getLastError() {
        return $this->lastError;
    }

    /**
     * @param int $headerRow
     */
    public function setHeaderRow($headerRow = 1) {
        $this->headerRow = is_int($headerRow) ? $headerRow : 1;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setMessageText($key, $value) {
        if (key_exists($key,$this->messageText))
            $this->messageText[$key]=$value;
    }

    /**
     * @param $separator
     */
    public function setSeparator($separator) {
        if (is_string($separator) && !empty(trim($separator)))
            $this->separator = $separator;
    }

    /**
     * @param bool $dataOnly
     */
    public function setReadDataOnly($dataOnly = false) {
        $this->dataOnly = is_bool($dataOnly) ? $dataOnly : false;
    }

    /**
     * @param $fields
     * @param $array
     * @param bool $caseSensitive
     * @param string $defaultValue
     */
    public function setFieldLookup($fields, $array, $caseSensitive=false, $defaultValue = "") {

        // $array must be single dimension array.
        if (!empty($fields) && is_array($array) && !empty($array) && $this->countDimensions($array) === 1)  {

            // if $fields is a string  it means it is a single field lookup
            if (is_string($fields))
                $this->setSingleFieldLookup($fields,$array, $caseSensitive, $defaultValue);

            // if $fields is a 1-item array it means it is a single field lookup
            else if(is_array($fields) && sizeof($fields) === 1)
                $this->setSingleFieldLookup($fields[0],$array, $caseSensitive, $defaultValue);

            // if $fields is an array (and we know it's > 1 item), then it's a multi field lookup
            else if(is_array($fields))
                $this->setMultiFieldLookup($fields,$array, $caseSensitive);


        }
    }

    /**
     * @param null $sheetname
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function import($sheetname = null) {

        // set the sheet name if one was provided
        if ($sheetname !== null && trim($sheetname)!== "") $this->sheetname = trim($sheetname);

        // clear out any old import data
        if (isset($this->spreadsheet) && !$this->newSpreadsheet) {
            $this->spreadsheet->disconnectWorksheets();
        }
        $this->dataArray = [];
        $this->errorsArray = [];
        $this->isTemplateApplied = false;
        $this->lastError = [];

        // read the data and set the spreadsheet variable
        $this->readfile();

        // if no sheetname was provided then all of them were read in. So then use the first sheet
        if ($sheetname === null || trim($sheetname)=== "")
            $this->spreadsheet->getSheetNames()[0];

        // Some error checking

        // a sheet name was provided but we didn't get it back, it means it didn't exist
        if ($this->spreadsheet->getSheetNames()[0] !== $this->sheetname) {
            $this->lastError['code'] = 1000;
            $this->lastError['message'] = $this->messageText["invalidSheet"];
            return false;

        }
        // we have some required fields that are missing
        if (!empty($this->templateMissingRequired)) {
            $this->lastError['code'] = 1001;
            $this->lastError['message'] = $this->messageText["requiredField"];
            return false;
        }

        // import was successfull with no basic errors.
        return true;

    }

    /**
     * @param null $limit
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function buildErrorsArray($limit=null) {

        // if the template has not yet been processed, we don't have the info needed to error check
        if (!$this->isTemplateApplied) return false;

        // informational tracking of the number of errors
        $this->sizeofErrorsArray = 0;

        // loop through every row in the spreadsheet starting AFTER the header
        foreach ($this->spreadsheet->getActiveSheet()->getRowIterator($this->headerRow+1) as $row) {
            $rowIndex = $row->getRowIndex();      // row number
            $anyErrors = false;                   // any errors in this particular row
            $holdingArray = [];                   // temporary array of field values
            $skipRecord = true;                   // assume we have no data on this row so we are going to skip it.

            // loop through every field identified in the template (notice this is not looping through columns in the spreadsheet
            foreach ($this->templateMap as $colIndex=>$config) {

                // get the value in the cell
                $value = $this->spreadsheet
                              ->getActiveSheet()
                              ->getCellByColumnAndRow($colIndex,$rowIndex,true)
                              ->getCalculatedValue();

                // We want to skip this row if either (a) all relevant fields are blank, or (b) specific fields are blank that force a skip

                // if $value is empty and we've not been told to ignore that fact
                if (!key_exists('ignoreForSkip',$config) || $config['ignoreForSkip']===false) {
                    if ($value===null || trim($value)===""){

                        // If we are supposed to force an immediate skip on this field if blank
                        if (key_exists('skipOnBlank',$config) && $config['skipOnBlank']===true) {
                            $skipRecord = true;
                            break;
                        }
                    } else {
                        $skipRecord = false; // we have at least one legit non-empty field
                    }
                }




                // track error for fields (blank means no errors yet)
                $fieldErrorText = "";

                // if this field has a type identified, check if the value conforms to the type
                if (isset($config['type'])) {
                    $result = $this->validValue($config,$value);   // returns TRUE or error message
                    if($result !== true) {
                        $fieldErrorText = $result;
                        $anyErrors =  true;
                    }
                }

                // if a 'fieldName' is specified, use it as the key. Otherwise use the template key as the key
                //    side note: The template key was placed in the template when on import() for convenience
                $configFieldValue = isset($config['fieldName']) ? 'fieldName' : 'headerText';
                $fieldKey = $config[$configFieldValue];

                // If there's no error yet, check if there's a 'single field' lookup and whether the value conforms
                if ($fieldErrorText === "") {

                    // is there a 'single field' lookup table for this field?
                    if(key_exists($fieldKey,$this->singleFieldLookupArray)) {

                        // does this value exist in the lookup table?
                        $testValue = $this->singleFieldLookupArray[$fieldKey]['caseSensitive'] ? trim($value) : trim(strtolower($value));
                        if(!key_exists($testValue,$this->singleFieldLookupArray[$fieldKey]['fieldValues'])) {
                            // value was not in the lookup table
                            // if field is blank, place the default key in the generated field
                            if ($value === null || $value === "") {
                                $holdingArray[$this->singleFieldLookupArray[$fieldKey]['valueField']] =
                                    array("value" => $this->singleFieldLookupArray[$fieldKey]['defaultValue'],
                                        "error" => "",
                                        "derivedFrom" => [$fieldKey]
                                    );
                            } else{
                                $fieldErrorText = $this->messageText["invalidSingleLookup"];
                                $anyErrors = true;
                            }
                        } else {
                            $holdingArray[$this->singleFieldLookupArray[$fieldKey]['valueField']] =
                                array( "value" => $this->singleFieldLookupArray[$fieldKey]['fieldValues'][$testValue],
                                    "error" => "",
                                    "derivedFrom" => [$fieldKey]
                                );
                        }
                    }
                }

                // put the value and error (may be blank) in the holding array
                // Don't format value if error to present user with their incorrect information
                if($fieldErrorText === "")
                    $tempValue = $this->formatValue($config,$value);
                else
                    $tempValue = $value;
                $holdingArray[$fieldKey] = ["value"=>$tempValue, "error"=>$fieldErrorText];
            }
            if (!$skipRecord) {
                // now we have all the fields for this row....

                // process any multi-field lookups that were found
                foreach($this->multiFieldLookupArray as $multiFieldLookupArray) {


                    // build a concatenated string of all the fields needed for the lookup
                    $concatFieldList = $multiFieldLookupArray['fieldNames'];
                    $caseSensitive = $multiFieldLookupArray['caseSensitive'];

                    $concatValue = "";
                    foreach($concatFieldList as $fieldName){
                        if(!isset($holdingArray[$fieldName])){
                            //field required for multi field lookup not found but record not skipped
                            //this means either that the developer included an unnecessary lookup table, or the field was allowed to be blank
                            //create a field entry that is blank to allow multifield to be searched for with the field being empty
                            //FIXME: would caspio give empty string or null in this case?
                            //FIXME: should this be allowable?
                            $holdingArray[$fieldName] = array('value' => "", 'error' => "");
                        }
                        $concatValue .= $caseSensitive
                            ? trim($holdingArray[$fieldName]['value']).$this->separator
                            : strtolower(trim($holdingArray[$fieldName]['value'])).$this->separator;
                    }


                    if ($concatValue !== "") $concatValue = substr($concatValue,0,(-1*strlen($this->separator))); // remove trailing separator

                    // if the concatenated string is not in the lookup table, then we have an error
                    if (!key_exists($concatValue,$multiFieldLookupArray['fieldValues'])) {
                        if($holdingArray[$multiFieldLookupArray['fieldNames'][0]]['error'] !== "")
                            $holdingArray[$multiFieldLookupArray['fieldNames'][0]]['error'] .= "\n";
                        //add error message to each multilookup field for ease of processing in UI
                        $keysToWriteErrors = $this->getRootKeyFieldsFromMultiLookupError($holdingArray, $concatFieldList);
                        foreach($keysToWriteErrors as $fieldName){
                            $holdingArray[$fieldName]['error'] .= $this->messageText["invalidMultiLookup"];
                        }
                        $anyErrors = true;
                    } else {
                        $holdingArray[$multiFieldLookupArray['valueField']] =
                            array( "value" => $multiFieldLookupArray['fieldValues'][$concatValue],
                                "error" => "",
                                "derivedFrom" => $multiFieldLookupArray['fieldNames']
                            );
                    }
                }

                // now we are done with all fields for this record

                // if there ANY field has an error, add the whole row to the error array
                if ($anyErrors) {
                    $this->errorsArray[$rowIndex] =$holdingArray;
                    $this->sizeofErrorsArray++;
                }

                // if we've reached the specified limit of errors, end the whole thing;
                if ($limit !== null && $this->sizeofErrorsArray >= $limit) return true;
            }
        }

        // all done with all records.
        return true;
    }
    /**
     * @param array $holdingArray
     * @param array $derivedFrom
     * @return array - Array of keys which contribute to invalid multiFieldLookup
     */
    private function getRootKeyFieldsFromMultiLookupError($holdingArray, $derivedFrom, $returnArray = null) {
        if($returnArray === null)
            $returnArray = array();
        foreach($derivedFrom as $key=>$keyField) {
            if(isset($holdingArray[$keyField]['derivedFrom'])){
                $returnArray = array_merge($returnArray, $this->getRootKeyFieldsFromMultiLookupError($holdingArray, $holdingArray[$keyField]['derivedFrom'], $returnArray));
            } else {
                array_push($returnArray, $keyField);
            }
        }
        return array_unique($returnArray);
    }

    /**
     * @return int
     */
    public function getSizeOfErrorsArray() {
        return $this->sizeofErrorsArray;
    }

    /**
     * @return array
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * @return int
     */
    public function getSizeOfDataArray() {
        return $this->sizeofDataArray;
    }

    /**
     * @param bool $useLookupValues
     * @param null $limit
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function buildDataArray($limit=null) {

        // if the template has not yet been processed, we don't have the info needed to error check
        if (!$this->isTemplateApplied) return false;

        // informational tracking of the number of rows of data
        $this->sizeofDataArray = 0;

        // loop through every row in the spreadsheet starting AFTER the header
        foreach ($this->spreadsheet->getActiveSheet()->getRowIterator($this->headerRow+1) as $row) {
            $rowIndex = $row->getRowIndex();    // row number
            $skipRecord = true;                 // assume the record doesn't have any relevant data

            // loop through every field identified in the template (notice this is not looping through columns in the spreadsheet
            foreach ($this->templateMap as $colIndex=>$config) {

                // if a 'fieldName' is specified, use it as the key. Otherwise use the template key as the key
                //    side note: The template key was placed in the template when on import() for convenience
                $configFieldValue = isset($config['fieldName']) ? 'fieldName' : 'headerText';
                $fieldKey = $config[$configFieldValue];

                // get the cell value
                $value = $this->spreadsheet
                    ->getActiveSheet()
                    ->getCellByColumnAndRow($colIndex,$rowIndex,true)
                    ->getCalculatedValue();

                // We want to skip this row if either (a) all relevant fields are blank, or (b) specific fields are blank that force a skip

                // if $value is empty and we've not been told to ignore that fact
                if (!key_exists('ignoreForSkip',$config) || $config['ignoreForSkip']===false) {
                    if ($value===null || trim($value)===""){
                        // If we are supposed to force an immediate skip on this field if blank
                        if (key_exists('skipOnBlank',$config) && $config['skipOnBlank']===true) {
                            $skipRecord = true;
                            break;
                        }
                    } else {
                        $skipRecord = false; // we have at least one legit non-empty field
                    }
                }
                // if there is a type specified for this field in the template, format the field accordingly
                if (isset($config['type'])) {
                    $value = $this->formatValue($config,$value);
                }
                // Done with this field, put the value in the row
                $this->dataArray[$this->sizeofDataArray][$fieldKey] = $value;

                //Add singleField lookups values to data array
                if(key_exists($fieldKey,$this->singleFieldLookupArray))  {                 // and 'single field' lookup exists for this field
                    // if case-insensitive, set test to lower case.
                    $testValue = $this->singleFieldLookupArray[$fieldKey]['caseSensitive'] ? $value : strtolower($value);

                    // Is the value  in the lookup table
                    if(key_exists(trim($testValue),$this->singleFieldLookupArray[$fieldKey]['fieldValues'])) {
                        // Add looked up key to current row
                        $this->dataArray[$this->sizeofDataArray][$this->singleFieldLookupArray[$fieldKey]['valueField']] = $this->singleFieldLookupArray[$fieldKey]['fieldValues'][$testValue];
                    } else if ($value === null || $value === "") {
                        $this->dataArray[$this->sizeofDataArray][$this->singleFieldLookupArray[$fieldKey]['valueField']] = $this->singleFieldLookupArray[$fieldKey]['defaultValue'];
                    }
                }
            }
            if ($skipRecord) {
                unset($this->dataArray[$this->sizeofDataArray]);
            } else {
                foreach($this->multiFieldLookupArray as $multiFieldLookupArray) {
                    // build the concatenation string of all relevant fields
                    $concatFieldList = $multiFieldLookupArray['fieldNames'];
                    $caseSensitive = $multiFieldLookupArray['caseSensitive'];

                    $concatValue = "";
                    foreach($concatFieldList as $fieldName)
                        $concatValue .= $caseSensitive
                            ? trim($this->dataArray[$this->sizeofDataArray][$fieldName] ).$this->separator
                            : strtolower(trim($this->dataArray[$this->sizeofDataArray][$fieldName] )).$this->separator;

                    if ($concatValue !== "") $concatValue = substr($concatValue,0,(-1*strlen($this->separator))); // remove trailing separator

                    // put the lookup value (e.g ID or true/false) into the value field
                    if (key_exists($concatValue,$multiFieldLookupArray['fieldValues'])) {
                        $this->dataArray[$this->sizeofDataArray][$multiFieldLookupArray['valueField']] = $multiFieldLookupArray['fieldValues'][$concatValue];
                    }

                }

                // if we've reached our limit, get out
                $this->sizeofDataArray++;
                if ($limit !== null && $this->sizeofDataArray >= $limit) return true;
            }
        }
        return true;
    }

    /**
     * @param null $sheetname
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function setActiveSheetByName($sheetname = null) {
        if ($sheetname === null) return false;
        if (!in_array($sheetname, $this->getSheetNames())) return false; // invalid sheetname
        return $this->processData($sheetname);
    }

    /**
     * Builds a lookup array by querying the caspio table/view
     * @param $lookupFields  - string (single field) or array (multiple fields to concatenate) for to lookup (caspio fields)
     * @param $valueField    - caspio field that will contain the value
     * @param $caspio        - caspio API object
     * @param $queryArray    - query array as defined by getCaspioData() function (except 'select' will be overwritten)
     * @param $message       - any error messages are returned here
     * @return array|bool    - result array or false if there's an error
     */
    public function buildLookupFromCaspio($lookupFields, $valueField, $caspio, $queryArray, &$message){


        // error checking
        if (empty($lookupFields) || empty($valueField) || !is_array($queryArray) || empty($queryArray)) {
            $message = "Invalid Parameter";
            return false;
        }

        // prep variables
        $fieldArray = [];       // will contain the list of caspio fields that need to be concatenated for lookup
        $lookupArray =[];       // will contain the resulting array to be returned (key is lookup, value is value)

        //  create select and $field Array differently based on whether $lookupfields is an
        //      array of multiple field names or just a string containing a single field name.
        if (is_array($lookupFields) ) {
            $select = implode(", ",$lookupFields) . ", " . $valueField;
            $fieldArray = $lookupFields;
        } else {
            $select = $lookupFields. ", " . $valueField;
            $fieldArray[] = $lookupFields;
        }

        // place the proper select
        $queryArray['select'] = $select;

        // get the data
        $data=getCaspioData ($caspio, $queryArray, $message);

        // return if there's an error
        if ($data === false) return false;

        $lookupArray['valueField'] = $valueField;
        // build the lookup array (one item per record returned from Caspio)
        foreach($data as $r) {

            // build the lookup value by concatenating all the lookup fields together
            $concatValue = "";      // will contain a concatenated list of caspio fields to use as the key for $lookupArray
            foreach($fieldArray as $fieldName)
                $concatValue .= trim($r->$fieldName).$this->separator;

            $concatValue = substr($concatValue,0,(-1*strlen($this->separator))); // remove trailing separator

            // build the actual array to return
            $lookupArray['fieldValues'][$concatValue] = $r->$valueField;
        }
        return $lookupArray;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function applyTemplate() {

        // figure out the last column of data
        $worksheet = $this->spreadsheet->getActiveSheet();
        $highestColumn = $worksheet->getHighestColumn();     // e.g. 'E'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

        // Loop through every column to see if it is used by the template
        for ($col = 1; $col <= $highestColumnIndex; ++$col) {

            // get value of the header field
            $headerText = trim($worksheet->getCellByColumnAndRow($col, $this->headerRow)->getValue());

            // if it was found in the template, setup template and template map
            if (key_exists($headerText,$this->template)) {
                $this->template[$headerText]["columnIndex"]=$col;        // modify template to add columnIndex for later convenience
                $this->template[$headerText]["headerText"]=$headerText;  // modify template to add headerText for later convenience
                $this->templateMap[$col] = $this->template[$headerText]; // set the template map (looks like template but key is column index)
            } else {

                // field is not in template but keep track in case user wants to know
                $this->unrecognizedHeaders[$headerText] = $col;
            }
        }

        // Now go through all template fields to see which are missing in the data.
        foreach ($this->template as $field=>$config) {

            // if there's no column Index or it's invalid, that means it wasn't found in the data
            if (!isset($config['columnIndex'])  ) {

                // add this to the full missing fields list
                $this->templateMissing[$field] = $config;

                // if the field was required ALSO add it to the missing required fields list.
                if (isset($config['required']) && $config['required']) {
                    $this->templateMissingRequired[$field] = $config;
                }
            }
        }

        // mark that the template has now been processed.
        $this->isTemplateApplied = true;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function readfile() {

        // clear old data
        if (isset($this->spreadsheet) && !$this->newSpreadsheet)
            $this->spreadsheet->disconnectWorksheets();

        $this->dataArray = [];
        $this->errorsArray = [];
        $this->isTemplateApplied = false;

        // ready the reader
        $reader = new Xlsx();

        // 'dataonly' takes less memory but has less data about each cell (like formatting)
        $reader->setReadDataOnly($this->dataOnly);

        // if a sheet name was provided, just get the one sheet (this takes less memory)
        if ($this->sheetname !== "") $reader->setLoadSheetsOnly([$this->sheetname]);

        // get the data
        $this->spreadsheet = $reader->load($this->filename);
        $this->newSpreadsheet = false;

        if (!$this->processData($this->sheetname))
            try {
                $this->spreadsheet->setActiveSheetIndex(0); // if process failed, at least set the active sheet to 0
            }
            catch(Exception $e) {}

    }

    /**
     * @param $config
     * @param $value
     * @return bool|mixed|string
     * @throws Exception
     */
    private function validValue($config, $value) {

        $required = (isset($config['required']) && $config['required']);    // is field required?
        $from = isset($config['from']);                                    // is there a 'from' value?
        $fromValue = $from ? $config['from'] : "";                         // what is the 'from' value?
        $to = isset($config['to']);                                         // is there a 'to' value?
        $toValue = $to ? $config['to'] : "";                                // what is the 'to' value?

        // if its empty and not required then no need to check -- all is good
        if ((trim($value) === "" || $value === null) && !$required ) return true;

        // different test by type
        switch (strtolower($config['type'])) {
            case 'text' :
                // if required but it is an empty string, then there's an error.
                if (empty(trim($value)) && $required) return $this->messageText["requiredValue"];

                // no other checks on text
                return true;
                break;

            case 'datetime':
            case 'date':
            case 'time':

                // if it's text, see if we can turn it into numeric date format
                if (is_string($value)) {
                    $value = strtotime($value);
                    if ($value===false) return $this->messageText["dateInvalid"];
                    $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value);
                }

                // must be numeric by now
                if (!is_numeric($value)) return $this->messageText["dateInvalid"];

                // must exist and be non-zero if it is required
                if ($value === 0 && $required) return $this->messageText["requiredValue"];

                // must not be negative
                if ($value < 0 ) return $this->messageText["dateInvalid"];

                // if there's no further requirements to check, then return true
                if (!$from && !$to) return true;

                // we are going to process date by using it's unix time stamp value
                $unixTimestamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($value);

                // if there is a low end and the date is below that low end, it's an error
                if ($from && $unixTimestamp < strtotime($fromValue)) return $this->messageText["dateTooSmall"].$fromValue;

                // if there is a high end and the date is above that high end, it's an error
                if ($to && $unixTimestamp > strtotime($toValue)) return $this->messageText["dateTooBig"].$toValue;

                // no other checks on  date/time
                return true;
                break;

            case 'number':

                // number must be a valid float
                if (filter_var($value,FILTER_VALIDATE_FLOAT, FILTER_FLAG_ALLOW_FRACTION )===false) return $this->messageText["numberInvalid"];

                // if there is a low end and the number is below that low end, it's an error
                if ($from && $value < $fromValue) return $this->messageText["numberTooSmall"].$fromValue;

                // if there is a high end and the number is above that high end, it's an error
                if ($to && $value > $toValue) return $this->messageText["numberTooBig"].$toValue;

                // no other error checks on number
                return true;
                break;

            case 'integer':
            case 'int':

                // must be a valid integer
                if (filter_var($value,FILTER_VALIDATE_INT )===false) return $this->messageText["intInvalid"];

                // if there is a low end and the number is below that low end, it's an error
                if ($from && $value < $fromValue) return $this->messageText["intTooSmall"].$fromValue;

                // if there is a high end and the number is above that high end, it's an error
                if ($to && $value > $toValue) return $this->messageText["intTooBig"].$toValue;

                // no other error checks on int
                return true;
                break;

            case 'yes/no':
            case 'bool':
            case 'boolean':

                if(is_bool($value) || strtolower($value) === "yes" || strtolower($value) === "no")
                    return true;
                else
                    return $this->messageText["unrecognizedBool"];
                break;

            default:
                //
                return true;    // assume valid
                break;
        }
    }

    /**
     * @param $config
     * @param $value
     * @return false|int|mixed|string
     * @throws Exception
     */
    private function formatValue($config, $value) {
        switch (strtolower($config['type'])) {
            case 'text' :
                return $value;
                break;
            case 'datetime':
            case 'date':
            case 'time':
                $format = (isset($config['format'])) ? $config['format'] : "Y-m-d\TH:i:s\Z";

                // if it's text, see if we can turn it into numeric date format
                if (is_string($value)) {
                    $value = strtotime($value);
                    if ($value===false) return $this->messageText["dateInvalid"];
                    $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value);
                }

                $unixTimestamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($value);
                return ($unixTimestamp === 0 ? $value : gmdate($format,$unixTimestamp));
                break;
            case 'number':
                return filter_var($value,FILTER_SANITIZE_NUMBER_FLOAT , FILTER_FLAG_ALLOW_FRACTION );
                break;
            case 'integer':
            case 'int':
                return intval(filter_var($value,FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
                break;

            case 'yes/no':
            case 'bool':
            case 'boolean':
                if($value === true || strtolower($value) === "yes" || strtolower($value) === "true")
                    return 'true';
                else
                    return 'false';
                break;
            default:
                return $value;
                break;
        }
    }

    /**
     * @param $sheetname
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function processData($sheetname) {
        // if we tried to get a specific sheet, try to set the active sheet to that sheet name
        if ($sheetname === "") return false;
        try {
            $this->spreadsheet->setActiveSheetIndexByName($sheetname);
            $this->dataArray = [];
            $this->errorsArray = [];
            $this->sheetname = $sheetname;
        } catch(Exception $e) {
            return false;
        }

        // as soon as we have both spreadsheet and template, use the template to setup helper arrays and look for errors.
        if (!empty($this->template) && !$this->newSpreadsheet)  $this->applyTemplate();

        return true;
    }

    /**
     * @param $array
     * @return int
     */
    private function countDimensions($array) {
        // return the number of dimensions in an evenly dimensioned array.
        if (is_array(reset($array))) {
            $return = $this->countDimensions(reset($array)) + 1;
        } else {
            $return = 1;
        }
        return $return;
    }

    /**
     * @param $field
     * @param $array
     * @param $caseSensitive
     * @param $defaultValue
     */
    private function setSingleFieldLookup($field, $array, $caseSensitive, $defaultValue) {
        if (trim($field)!=="" && is_array($array) && !empty($array) && is_array($array['fieldValues']) && !empty($array['fieldValues'])) {

            // make sure we have a valid $caseSensitive Field
             $caseSensitive = is_bool($caseSensitive) ? $caseSensitive : false;

            $this->singleFieldLookupArray[$field]['fieldValues'] = $caseSensitive ? $array['fieldValues'] : array_change_key_case($array['fieldValues']);;
            $this->singleFieldLookupArray[$field]['caseSensitive'] = $caseSensitive;
            $this->singleFieldLookupArray[$field]['valueField'] = $array['valueField'];
            $this->singleFieldLookupArray[$field]['defaultValue'] = $defaultValue;
        }
    }

    /**
     * @param $fieldArray
     * @param $array
     * @param $caseSensitive
     */
    private function setMultiFieldLookup($fieldArray, $array, $caseSensitive) {

        if (is_array($fieldArray) && !empty($fieldArray) && is_array($array) && !empty($array) && is_array($array['fieldValues']) && !empty($array['fieldValues'])) {

            // make sure we have a valid $keyField
            $keyFieldName = count($this->multiFieldLookupArray);

            // make sure we have a valid $caseSensitive Field
            $caseSensitive = is_bool($caseSensitive) ? $caseSensitive : false;

            $this->multiFieldLookupArray[$keyFieldName]['fieldNames'] = $fieldArray;
            $this->multiFieldLookupArray[$keyFieldName]['fieldValues'] = $caseSensitive ? $array['fieldValues'] : array_change_key_case($array['fieldValues']);
            $this->multiFieldLookupArray[$keyFieldName]['caseSensitive'] = $caseSensitive;
            $this->multiFieldLookupArray[$keyFieldName]['valueField'] = $array['valueField'];
        }
    }

    /**
     * @param $column
     * @param $increment
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @return \PhpOffice\PhpSpreadsheet\Cell\Coordinate
     */
    public function incrementColumn($column, $increment) {
        $colNumber = Coordinate::columnIndexFromString($column);
        $newIndex = ($colNumber + $increment) > 0 ? $colNumber + $increment : 1;
        return Coordinate::stringFromColumnIndex($newIndex);
    }

// KEY FEATURES
    /*
    *  - Reads data from XLSX spreadsheet using the name of the spreadsheet
    * - Can read one or all sheets within the spreadsheet
    * - Returns spreadsheet object from phpspreadsheet to allow full control if you wish
    * - Remove formatting to minimize size requirements
    * - Various built-in Validation (config template required)
    *   >> See all the expected that are missing (required or not)
    *   >> auto-check cell values conform to field type (date, integer, number, string)
    *   >> auto-check cell values conform to value range (date, integer, number)
    *   >> auto-check cell values exist within a lookup list
    *   >> auto-check multiple fields combined exist within a lookup list
    * - Return an array of errors providing the record number, field value, and error (if any)
    * - Limit the number of errors returned so it doesn't get too large if everything is wrong
    * - Return a pure data array:  [row number][field name]=value;
    * - Replace incoming values with IDs from lookup arrays (single lookups and multi-field concatenation lookup)
    * - Utility function to create Lookup Array from Caspio table or view
    *
   */
// TEMPLATE - Documentation
    /* ------------------------------------------------------------------------------------------------
     *  A template is required , but can be very minimal. The key can point to an empty array, but there needs to be a list of keys.
     *
     *    key : The key value is the text value of the header you are expecting in the sheet (i.e. "First Name")
     *
     *
     *    Array Options:
     *
     *    "fieldName" =>    Used for the Key value of the array created with buildDataArray(); often is the field name in your database;
     *                      Default:  Uses the template key value if not provided
     *
     *    "type" =>         Field type for error checking and formatting;
     *                      "text" - Any value
     *                      "integer", "int" - value must be an integer
     *                      "number" - value must be a number, may contain decimals
     *                      "date", "time", "datetime" - date / time  (see format for details)
     *                      "yes/no", "bool", "boolean" - Value restricted to yes, no, true, or false. Case insensetive
     *
     *   "format" =>        Only used for date/time/datetime. For options, see: http://php.net/manual/en/function.date.php
     *                      default: "Y-m-d\TH:i:s\Z"
     *
     *   "from"=>; "to"=>   Allows you to specify a valid range of values.  Works for integer, number, and date.
     *                      For dates, you can use any string date format, i.e. "2019-01-10".
     *                      [OPTIONAL]
     *
     *   "required" =>      if false then null, blank, empty values are treated as invalid.
     *                      default: false;
     *
     *  "ignoreForSkip" =>  Ignore this field when deciding whether to skip a row because all fields are blank.
     *                      In other words, if all OTHER fields are blank go ahead and skip the record.
     *
     *  "skipOnBlank" =>    if this field is blank or null, the entire record will be skipped immediately
     *
     * ------------------------------------------------------------------------------------------------ */
// ATTRIBUTES - Documentation
    /* ------------------------------------------------------------------------------------------------
     *    dataArray : Two Dimensional array -  Is empty until method buildDataArray is executed; TEMPLATE must exist first;
     *                 key 1:  serialized value (0..n) where n is the number of non-header records in the table
     *                 key 2:  [fieldName] (or if not provided defaults to template key - i.e. header text)
     *                 Value:   the value of the data in the cell represented by the record number (dimension 1) and fieldName (dimension 2)
     *
     *                  This array is created when you by running $object->buildDataArray()
     *                  Reference this property directly to save space as it may be quite large.
     *                  You can free up the memory simply by using $object->dataArray = [];
     *
     *    errorsArray :array -  3D empty until method buildErrorsArray is executed; TEMPLATE must exist first;
     *                 key1:  row number the error was found in the spreadsheet
     *                 key2: [fieldName] (or if not provided defaults to template key - i.e. header text);
     *                                           Every field in the TEMPLATE is represented for the row even if it doesn't have an error
     *                 key3: ['value']  - the value of the data in the cell represented by the record number (dimension 1) and fieldName (dimension 2)
     *                       ['error']  - blank if no errors;  error text if there's an error
     *
     *                  This array is created when you by running $object->buildErrorsArray()
     *                  Reference this property directly to save space as it may be quite large.
     *                  You can destroy this simply by using $object->errorsArray = [];
     *    spreadsheet :  spreadsheet object as defined by https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Spreadsheet.html
     * ------------------------------------------------------------------------------------------------ */
// METHODS - Documentation
    /* ------------------------------------------------------------------------------------------------
     *========================================================
     * CONSTRUCT:
     *========================================================
     *    $myOBJ = new importExcel('filename.xlsx');
     *
     *
     *========================================================
     * PRE-IMPORT CONFIGURATIONS:
     *========================================================
     *    setReadDataOnly($dataOnly = false)  - setting to true saves on space; but you don't get cell formats; returns void
     *---------
     *    setTemplate($template = [])         - set the template array to use; returns void
     *---------
     *    setHeaderRow($headerRow = 1)        - set what row the header is on; data is expected to start on the next row; returns void
     *
     *========================================================
     * IMPORT FUNCTION
     *========================================================
     *    import($sheetname = null)
     *       - parameters
     *         >> if nothing is passed, all sheets will be imported and the active sheet will be set to the first one
     *         >> if a specific sheet name is passed, only that sheet will be loaded
     *       - returns
     *          >> false if failure (use getLastError() to determine what error;
     *          >> true if spreadsheet property was successfully created
     *
     *========================================================
     * POST-IMPORT ERROR CHECKING
     *========================================================
     *    getMissingFields() - return an array containing all fields from the TEMPLATE not found in Workbook
     *       - returns
     *         >> false if template hasn't been set;
     *         >> array of fields (from template) that weren't in the worksheet
     *             > key is the key from the template
     *             > value is the corresponding array from the template
     *---------
     *    getMissingRequiredFields() - same as getMissingFields() except only fields marked as required
     *---------
     *    getExtraneousFields() - same as getMissingFields() except it shows the fields in the spreadsheet not found in TEMPLATE
     *---------
     *    getLastError()  - gets single error array with a 'code' and the 'message'
     *       - returns array with following keys
     *         >> ['code'] = (integer):
     *                 0 : no errors;
     *              1000 : specified sheet not in workbook; triggered in import() method
     *              1001 : At least one required field is missing; triggered in import() method
     *        >> ['message'] = (string) error text
     *---------
     *    setFieldLookup($fields,$array, $primaryField=null, $caseSensitive=false) - set up a lookup array so buildErrorsArray can validate valid values
     *        - parameters (single lookup value scenario)
     *          >> $fieldName (String) -  the key used to identify the field in the template.
     *                                    Generally "fieldName". But uses template key (header text) if fieldName not set
     *          >> $array               - Array built from buildLookupFromCaspio() the name of the field in caspio from which the data was retrieved as a string in ['valueField']
     *                                    as well as 1 dimensional array where all the possible combinations of valid values are the key. in ['fieldValues']
     *          >> $caseSensitive       - optional, default=false;  True=lookup is case-sensitive.
     *          >> $defaultValue        - optional, default=""; Value to place in lookup generated field if nothing is present in the field that triggers the lookup, and nothing does not have a corresponding lookup value
     *                                    If the lookup field is required in template this value will never be used
     *        - parameters (multiple fields combined lookup value scenario)
     *          >> $fieldName (array) -   1 dimensional array of keys used to identify the field in the template.
     *                                    Generally "fieldName". But uses template key (header text) if fieldName not set
     *          >> $array               - Array built from buildLookupFromCaspio() the name of the field in caspio from which the data was retrieved as a string in ['valueField']
     *                                    as well as 1 dimensional array where all the possible combinations of valid values are the key. in ['fieldValues']
     *                                    Concatenate values from various fields with the (default: "<|>") separator.
     *                                    for example, first & last name would be   "shaun<|>wolfe"=>1
     *          >> $caseSensitive       - optional, default=false;  True=lookup is case-sensitive.
     *---------
     *    buildLookupFromCaspio($lookupFields, $valueField, $caspio, $queryArray, &$message)
     *      this is a helper function to create a lookup array from Caspio to feed to setFieldLookup
     *      - Parameters
     *        >> $lookupFields (string|Array) - Either a single field (string) or multiple fields to concatenate (array)
     *        >> $valueField  (string)        - caspio field that contains the value result of the lookup (usually and ID/Key)
     *        >> $caspio   (object)           - Caspio object
     *        >> $queryArray (array)          - same fields as "getCaspioData()" function (except 'select' will be overwritten
     *        >> $message (string)            - if this function fails the message will be placed here.
     *     -  Returns (array | false)         - false if error; or lookup array containing the name of the field in caspio from which the data was retrieved as a string at key 'valueField'
     *                                          and an array (lookup string as key, value as value) at key 'fieldValues'.
     *---------
     *    buildErrorsArray($limit=null) - builds the public property errorsArray (see errorsArray for details)
     *        - parameters
     *          >> limit  (optional) - Maximum number of errors before stopping. No parameter = no limit;
     *        - returns
     *          >> false if template hasn't been set
     *          >> true in all other cases
     *---------
     *    getSizeOfErrorsArray() - returns the number of rows in the ->errorsArray
     *
     *
     *
     *========================================================
     * POST-IMPORT UTILITIES
     *========================================================
     *
     *      getSheetNames() - returns array of strings with the sheet names loaded.
     *---------
     *      setActiveSheetByName($sheetname) - changes which sheet is active (and resets the public properties
     *        - parameters: sheetname, string  of name of sheet to load.
     *        - returns false if sheetname could not be found in the list of sheetnames
     *---------
     *      buildDataArray($useLookupValues=false, $limit=null) - builds the public property dataArray (see dataArray for details);
     *        - parameters
     *          >> $limit  - (optional) when not null, it limits the number of records to be added to the table (mostly for testing)
     *        - returns
     *          >> false if template hasn't been set
     *          >> true in all other cases
     *---------
     *    getSizeOfDataArray() - returns the number of rows in the ->dataArray
     *---------
     *    setMessageText($key,$value) - changes the text that is displayed for various error messages.
     *                                  list of options can be found in < private $messageText >
     *---------
     *      setSeparator($separator)  - sets a string for what will be used to separate concatenated fields for lookup (default is <|>
     *---------
     *      public function incrementColumn($column, $increment)
     *      takes the provided $column letter (i.e. 'C' or 'AB') and increments by some number of columns. Negative increment is a decrement;
     *
     * ------------------------------------------------------------------------------------------------ */


}

