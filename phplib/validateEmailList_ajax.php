<?php
require_once($_SERVER['DOCUMENT_ROOT']."/phplib/validateEmailList_fn.php");   // verifies caspio REST API access

//PROGRAM TO ADD A RECORD TO THE EMAIL QUE (REGULAR CASPIO ACCOUNT) WITH AN AJAX CALL. Can use /js/add2EQM.js for an easy js function to make the ajax call.
$returnMsg = "";

$return["Result"] = false;
$return["ErrorMsg"]="";

// First Check for proper AJAX Call
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) $return["ErrorMsg"] = "Invalid Ajax Call: Invalid headers.";
else if (!isset($_POST['emailList']) || !isset($_POST['delimiter'])) $return["ErrorMsg"] = "Invalid Ajax call: Required data is missing.";
if ($return["ErrorMsg"] === "") {
    $return["Result"] = validateEmailList($_POST['emailList'],$_POST['delimiter']);
}
echo json_encode($return);
