<?php
require_once($_SERVER['DOCUMENT_ROOT']."/REST/caspioAuth.php");   // verifies caspio REST API access
require_once($_SERVER['DOCUMENT_ROOT']."/phplib/addOrReactivate_fn.php");

$return["Result"] = -1;
$return["ErrorMsg"]="";

// First Check for proper AJAX Call
if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    $return["ErrorMsg"] = "Invalid Ajax Call: Invalid headers.";
    // check to make sure all the required POST data was sent
} elseif (!isset($_POST['tablename']) || !isset($_POST['keyFieldName']) || !isset($_POST['fieldValueArray'])) {
    $return["ErrorMsg"] = "Invalid Ajax call: Required data is missing.";
} else {

    // Required Paramaters
    $tablename = $_POST['tablename'];               // in the format /tables/table_name
    $keyFieldName = $_POST['keyFieldName'];         // this is the name of the key field. The value for this field will be returned after the add
    $fieldValueArray = $_POST['fieldValueArray'];   // key-value pair array of fieldname->Value.  Must have every field necessary to add a record

    // Optional Paramaters: These are needed if you want to attempt to first reinstate a record before seeing if it needs added.
    $uniqueFieldName = isset($_POST['uniqueFieldName']) ? $_POST['uniqueFieldName'] : "";
    $uniqueFieldValue = isset($_POST['uniqueFieldValue']) ? $_POST['uniqueFieldValue'] : "";

    // make the attempt to add or reactivate
    $result = addOrReactivate($tablename, $keyFieldName, $fieldValueArray, $uniqueFieldName, $uniqueFieldValue);

    // setup return array
    if ($result === false ) $return["ErrorMsg"] = $Caspio_message;
    $return['Result'] = $result;
}
echo json_encode($return);
// $return['Result'] < 0 if an error
// $return['Result'] === false, if fundemental error
// $return['Result'] > 0 is the keyvalue of the record that was added or reactivated.
// $return['ErrorMsg'] = appropriate error message if any;