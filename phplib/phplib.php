<?php
// setup a function to test if session is already active that works for PHP 5.3  or 5.4+
if(!function_exists('session_status')){
    // works for PHP 5.3
    function session_active(){
        return defined('SID');
    }
}else{
    // works for PHP 5.4+
    function session_active(){
        return (session_status() == PHP_SESSION_ACTIVE);
    }
}
