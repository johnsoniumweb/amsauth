<?php
function getCaspioData($caspioObj, $queryArray,&$message=""){
    global $Caspio_message;
    /* -----------------------------------------------------------------------------------------------------
     * --- LOOP Using SQL Paging to get more then 1000 records if necessary
     * --- RESULTS: either Bool False or array of data
     *
     * Sample USAGE:
          global  $Caspio_apiURL;
            $caspio = new Caspio();
            $message = "";
            $result=getCaspioData ($caspio, array(
                    "table"=>$Caspio_apiURL . "/views/name_of_view/rows",
                    "token"=>$Caspio_token,
                    "select"=> "accountName, AVG(ResultValue) Score",
                    "where"=>  "accountID = ". $accountID . " and ResultValue > 0 and CriteriaType = 4 ".$substatus,
                    "groupby"=> "accountName, EvaluatorType")
                ,$message);

            if ($result === false) {
                echo $message;
            }
            else if (sizeof($result) === 0) {
                // process no records returend
            } else {
            // process records
                foreach ($result as $r) {
                    echo $r->Score;
                }
            }
     *
     *
     * -----------------------------------------------------------------------------------------------------*/

    // set defaults for things not passed;
    $select = (!isset($queryArray['select']) || trim($queryArray['select'])=="") ? "*" : $queryArray['select'];
    $where = (!isset($queryArray['where']) || trim($queryArray['where'])=="") ? "" : $queryArray['where'];
    $groupby = (!isset($queryArray['groupby']) || trim($queryArray['groupby'])=="") ? "" : $queryArray['groupby'];
    $orderby = (!isset($queryArray['orderby']) || trim($queryArray['orderby'])=="") ? "" : $queryArray['orderby'];
    $limit = (!isset($queryArray['limit']) || trim($queryArray['limit'])=="") ? "" : $queryArray['limit'];
    $pages = (!isset($queryArray['pages']) || !is_numeric($queryArray['pages'])) ? 2 : $queryArray['pages'];
    if (!isset($queryArray['table']) || trim($queryArray['table'])=="") {$message="Invalid attempt to retrieve data: Missing 'table' parameter."; return false;};
    if (!isset($queryArray['token']) || trim($queryArray['token'])=="") {$message="Invalid attempt to retrieve data: Missing 'token' parameter."; return false;};
    // Initialize Variables
    $pageNum =1; $pageSize = 1000; $returnedRecords = $pageSize; $maxPages=$pages; $resultsArray = [];

    // if a limit was passed, that means to skip the whole paging feature
    if ($limit != "" ) {
        $caspioQuery = array('q' => '{'
            .'"select":"'.$select.'",'
            .(($where=="")?'':'"where":"'.$where.'",')
            .(($groupby=="")?'':'"groupby":"'.$groupby.'",')
            .(($orderby=="")?'':'"orderby":"'.$orderby.'",')
            .'"limit":'.$limit.'}');

        // make the caspio call --- this presumes you have created the $caspio object and setup the $Caspio_apiURL and $Caspio_token
        $caspioGETResult = $caspioObj->ExecGetRequest($queryArray['table'], trim($queryArray['token']), $caspioQuery);

        if ($caspioGETResult===false) {
            // fatal error usually caused by either an ill-formed query array or caspio session has timed out.
            // We need to stop the loop by settting pageNum to 99 to indicate there was a fatal error.
            $message = "Query Failed: " . $Caspio_message;
            $pageNum = 99;
        } else {
            return $caspioGETResult->Result;
        }
    } else {
        // Loop until less than a full page is returned, or we exceed maximum pages
        while($returnedRecords === $pageSize && $pageNum <= $maxPages){
            // set up whatever query you want -- no restrictions on groupby/sort/etc. (but 'limit' will be ignored if used)

            $caspioQuery = array('q' => '{'
                .'"select":"'.$select.'",'
                .(($where=="")?'':'"where":"'.$where.'",')
                .(($groupby=="")?'':'"groupby":"'.$groupby.'",')
                .(($orderby=="")?'':'"orderby":"'.$orderby.'",')
                .'"pageNumber":'.$pageNum.','
                .'"pageSize":'.$pageSize.'}');

            // make the caspio call --- this presumes you have created the $caspio object and setup the $Caspio_apiURL and $Caspio_token
            $caspioGETResult = $caspioObj->ExecGetRequest($queryArray['table'], trim($queryArray['token']), $caspioQuery);

            if ($caspioGETResult===false) {
                // fatal error usually caused by either an ill-formed query array or caspio session has timed out.
                // We need to stop the loop by settting pageNum to 99 to indicate there was a fatal error.
                $message = "Query Failed: ".$Caspio_message;
                $pageNum = 99;
            } else {
                $returnedRecords = sizeof($caspioGETResult->Result) ;   // need to know # of records returned to know when we are done
                $pageNum++;                                             // ready for the next page
                if ($returnedRecords > 0 )                              // if we got any records, add it to our array
                    $resultsArray = array_merge($resultsArray, $caspioGETResult->Result);
            }
        }
    }
    if ($pageNum == 99) return false;
    else return $resultsArray;

}