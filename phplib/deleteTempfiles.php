<?php
// because this is a cron job, we need to setup the directory information manually
$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

// then change the working directory directory
chdir($_SERVER['DOCUMENT_ROOT']."/phplib");

$tempfileDirectories = array(
    "/tempfiles",
    "/dell/tempfiles",
    "/gne/tempfiles",
    "/ibm/scopedeliver/tempfiles",
    "/ibm-test/scopedeliver/tempfiles",
    "/mdlz/scopedeliver/tempfiles",
    "/mdlz-test/scopedeliver/tempfiles",
    "/vz/APA/tempfiles",
    "/vz/SD/tempfiles"
);

$deletebeforeMin = 60; //60 minutes
$deleted = 0;

foreach($tempfileDirectories as $directory){
    if(file_exists ( $_SERVER['DOCUMENT_ROOT'].$directory )){
        $files = glob($_SERVER['DOCUMENT_ROOT'].$directory.'/*'); // get all file names

        foreach($files as $file){ // iterate files
            if (filemtime($file) <  (time() - ($deletebeforeMin*60))) {
                if(!unlink($file)) echo "Unable to delete tempfile ".$directory."/".$file."\r\n";
                else $deleted++;
            }
        }
    } else echo $directory." does not exist.\r\n";
}

