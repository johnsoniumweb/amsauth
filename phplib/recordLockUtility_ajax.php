<?php
require_once($_SERVER['DOCUMENT_ROOT']."/REST/caspioAuth.php");   // verifies caspio REST API access
require_once($_SERVER['DOCUMENT_ROOT']."/ams_proto/figari/recordLockUtility_fn.php");   // verifies caspio REST API access

//PROGRAM TO ADD A RECORD TO THE EMAIL QUE (REGULAR CASPIO ACCOUNT) WITH AN AJAX CALL. Can use /js/add2EQM.js for an easy js function to make the ajax call.
$returnMsg = "";

$return["Result"] = -1;
$return["ErrorMsg"]="";

// First Check for proper AJAX Call
if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    $return["ErrorMsg"] = "Invalid Ajax Call: Invalid headers.";
    // check to make sure all the required POST data was sent
} elseif (!isset($_POST['path']) || !isset($_POST['kfn']) || !isset($_POST['ufn'])) {
    $return["ErrorMsg"] = "Invalid Ajax call: Required data is missing.";
} else {
    $path = $_POST['path'];
    $uniqueFieldName = $_POST['uniqueFieldName'];
    $keyFieldName = $_POST['keyFieldName'];
    $fieldValueArray = $_POST['fieldValueArray'];
    $return["Result"] = addOrReactivate($path, $uniqueFieldName, $keyFieldName, $fieldValueArray);


}
echo json_encode($return);
