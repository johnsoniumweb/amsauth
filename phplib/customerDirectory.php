<?php
require_once( 'addLog.php');

class amsCustomer {
    private $authIncludeUrl;
    private $customerPath;
    private $customer;

    private $customers      =
        array(
            "/vz/SD/"                   => array( 'auth' => 'security/vzScopeAuth.php', 'sessionPrefix' => "vzScope"),
            "/mdlz/scopedeliver/"       => array( 'auth' => 'security/mdlzScopeAuth.php', 'sessionPrefix' => "mdlzScope"),
            "/ibm/scopedeliver/"        => array( 'auth' => 'security/ibmScopeAuth.php', 'sessionPrefix' => 'ibmScope', 'logTable' => 'ibmScope_log'),
            "/ibm-test/scopedeliver/"        => array( 'auth' => 'security/ibmScopeAuth.php', 'sessionPrefix' => 'ibmScope', 'logTable' => 'ibmScope_log'),
            "/pg/"        => array( 'auth' => 'security/AMSAuth.php', 'sessionPrefix' => 'pgScope', 'logTable' => 'pgScope_Log'),
            "/ams_proto/"        => array( 'auth' => 'security/AMSAuth.php', 'sessionPrefix' => 'AMSProto', 'logTable' => 'AMSProto_Log'),
            "/proto/"        => array( 'auth' => 'security/AMSAuth.php', 'sessionPrefix' => 'AMSProto', 'logTable' => 'AMSProto_Log')

        );


    public function __construct($path){
        if(!$path || $path == "")throw new Exception("Must pass a customer path when creating a customer instance.");
        else if(!array_key_exists($path, $this->customers)) throw new Exception("Customer does not exist.");
        else {
            $this->customerPath = $path;
            $this->customer = $this->customers[$path];
            $this->setAuthIncludeUrl();

        }
    }

    public function getSessionVar($var){
        if($this->customer['sessionPrefix'] && $this->customer['sessionPrefix'] != ""){
            return $_SESSION[$this->customer["sessionPrefix"]."_".$var];
        } else return null;
    }

    private function setAuthIncludeUrl(){
        if (array_key_exists('auth', $this->customer)) {
            if(file_exists($_SERVER['DOCUMENT_ROOT'].$this->customerPath.$this->customer['auth'])){

                $this->authIncludeUrl = $this->customer['auth'];

            } else throw new Exception("Auth file ".$this->customerPath.$this->customer['auth']." does not exist.");
        } else $this->authIncludeUrl = '';
    }

    public function getAuthIncludeUrl()
    {
      return $this->customerPath. $this->authIncludeUrl;
    }

    public function authorize(){
        if($this->authIncludeUrl != ""){
            require_once( $_SERVER['DOCUMENT_ROOT'].$this->getAuthIncludeUrl());

            return authorized();
        } else return "";
    }

    public function isLogEnabled()
    {
        return $this->customer['logTable'] && $this->customer['logTable'] != "";
    }

    private function getCustomerSpecificLogFields(){
        global  $Caspio_apiURL, $Caspio_token;

        $caspio = new Caspio();
        $access_token = $Caspio_token;

        $get_logTable = $caspio->ExecGetRequest($Caspio_apiURL . "/tables/".$this->customer['logTable']."/columns", $access_token, array('q' => '{"select" : "*"}'));

        $fieldNames = array();

        if($get_logTable && sizeof($get_logTable->Result) > 0){
            foreach ($get_logTable->Result as $colNum=>$column) {
                array_push($fieldNames, $column->Name);
            }
        }

        return $fieldNames;
    }

    public function logAction($logFields)
    {
        $logged = false;
        if($this->isLogEnabled()){
            $logArray = array();
            $fieldNames  = $this->getCustomerSpecificLogFields();

            foreach($logFields as $name=>$value){
                foreach($fieldNames as $index=>$custSpecificName){
                    if (strpos(strtolower($custSpecificName), strtolower($name)) !== false){
                        $logArray[$custSpecificName] = $value;
                        unset($fieldNames[$index]);
                        break;
                    }
                }
            }

            $logged = submitLog($this->customer['logTable'], $logArray);
        }

        return $logged;
    }
}



