<?php
require_once  $_SERVER['DOCUMENT_ROOT'].'/PhpExcel/Spout/Autoloader/autoload.php';
ini_set('memory_limit', '128M'); // Up from default 32MB

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Common\Type;

$invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
$directory = $_SERVER['DOCUMENT_ROOT']."/tempfiles/";

$errorMsg = '';

function generateFileName()
{
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_";
    $name = "";
    for($i=0; $i<12; $i++)
        $name.= $chars[rand(0,strlen($chars))];
    return $name;
}

/**
 * Creates an excel file in the directory stored in global $directory and returns the file name.
 *
 * @param $data:                     array of records
 * @param $file:                     fileName to append to. Blank if create new file.
 * @param string $title:             Title of excel worksheet.
 * @param array $passedColTitles:    array of column titles in format array("field" =>"title");
 * @return string                    returns the filename of the excel file.
 */
function createXcelFromArray($data, $file, $title = "", $passedColTitles = array()){
    global $errorMsg, $directory, $invalidCharacters;

    $returnVal = "";
    $data = json_decode( json_encode($data), true);     //make sure that data is in php associative array format

    if(sizeof($data) < 1) $errorMsg = "No data to export.";
    else if($file != "" && !file_exists($directory.$file.'.xlsx'))$errorMsg = 'Cannot append: File not found!';
    else {
        try{
            $workingFile = ($file == "")? generateFileName() : $file."_1" ; //create a tempfile name.

            $defaultStyle = (new StyleBuilder())
                ->setFontName('Calibri')
                ->setFontSize(11)
                ->setShouldWrapText(false)
                ->build();

            $headerStyle = (new StyleBuilder())
                ->setFontName('Calibri')
                ->setFontSize(11)
                ->setFontBold()
                ->setShouldWrapText(false)
                ->build();

            $writer = WriterFactory::create(Type::XLSX); // for XLSX files
            $writer->setTempFolder($directory);
            $writer->setDefaultRowStyle($defaultStyle);
            $writer->setShouldCreateNewSheetsAutomatically(true); // default value

            $writer->openToFile($directory.$workingFile.".xlsx"); // write data to a file or to a PHP stream
            $currentSheet = $writer->getCurrentSheet();

            if($title != "") $currentSheet->setName(substr((str_replace($invalidCharacters, '', $title)),0,29));


            //if we are appending data to a file,
            if($file != ""){ // we need a reader to read the existing file...
                $reader = ReaderFactory::create(Type::XLSX);
                $reader->open($directory.$file.".xlsx");

                foreach ($reader->getSheetIterator() as $sheetIndex => $sheet) {
                    if ($sheetIndex !== 1) $writer->addNewSheetAndMakeItCurrent(); // Add sheets in the new file, as we read new sheets in the existing one

                    foreach ($sheet->getRowIterator() as $row) {
                        $writer->addRow($row);  // ... and copy each row into the new spreadsheet
                    }
                }
            } else {
                //determine the column titles. Default is the field names from the data.
                $columnTitles = array_keys($data[0]);
                $columnTitles = array_combine($columnTitles, $columnTitles);

                if(sizeof($passedColTitles) > 0){
                    foreach($passedColTitles as $field=>$title){
                        if($columnTitles[$field])$columnTitles[$field] = $title;
                    }
                }
                $writer->addRowWithStyle($columnTitles, $headerStyle);    //set column titles.
            }

            $writer->addRows($data);            //add data
            $writer->close();

            if($file != ""){
                unlink($directory.$file.".xlsx");
                rename($directory.$workingFile.".xlsx", $directory.$file.".xlsx");
            }

            $returnVal = $file == ""? $workingFile: $file;

        } catch (Exception $exception) {
            $errorMsg = 'Exception caught: '.$exception;
        }
    }

    return $returnVal;
}


/**
 * @param $tableName                    :name of table to get records.
 * @param $fields                       :list of fields to select for query.
 * @param $where                        :query where statement
 * @param $keyField                     :unique key in table to sort by so we can keep a marker to get all data 1000 records at a time.
 * @param string $title                 :title of the excel worksheet
 * @param array $passedColTitles        :array of column titles in format array("field" =>"title");
 *
 * @return string                       :file name created.
 */
function getTableRecords2Excel($tableName, $fields, $where, $keyField, $title = "", $passedColTitles = array())
{
    global $Caspio_apiURL,$Caspio_token,$Caspio_message, $errorMsg;

    $startID = 0;
    $filename = "";

    if(caspioAuth()){
        $caspio = new Caspio();

        do {
            $found_json = $caspio->ExecGetRequest($Caspio_apiURL . $tableName. "/rows", $Caspio_token, array('q' =>
                '{ "select" : "'.$fields.'", "Where": "'.  (($where != "")? $where." and ": "")  .  $keyField.' > '.$startID.'" , "orderby": "'.$keyField.'","limit": 1000}'));

            if (!$found_json){
                $errorMsg.= 'Error getting data: '.$Caspio_message."<br>";      //query error
                $startID = 0;
            } else if (sizeof($found_json->Result) == 0) {
                if($startID == 0) $errorMsg.= 'No Data.<br>'; //if this is first pass through and no records returned, there is no data.
                $startID = 0;
            } else {
                $records = $found_json->Result;
                $filename = createXcelFromArray($records, $filename, $title, $passedColTitles);

                if(sizeof($records) == 1000 && $filename != "") $startID = $records[999]->{$keyField};  //1000 records returned so we know we need to query for more records. Go add to excel file and run this function again.
                else $startID = 0; //records were returned, but less than the max so we know there are no more records. Go add to the excel file.
            }
        } while ($startID > 0);

    } else $errorMsg.= 'Caspio authorization error: '.$Caspio_message."<br>";      //caspio auth error.

    return $filename;
}

