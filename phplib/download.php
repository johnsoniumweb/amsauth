<?php
/**
 * Created by PhpStorm.
 * User: Shaun Wolfe
 * Date: 12/23/2018
 * Time: 12:32 PM
 */
$tempDirectory = sys_get_temp_dir()."/";

if (!isset($_GET['in']) || empty($_GET['in'])) exit;
if (!isset($_GET['type']) || empty($_GET['type'])) exit;
if (!isset($_GET['out']) || empty($_GET['out'])) $_GET['out']=$_GET['in'];
if (!isset($_GET['u']) || empty($_GET['u'])) $_GET['u']='n';  // unlink / delete setting


switch (strtolower($_GET['type'])) {
    case 'xlsx':
        $inputFile = $tempDirectory.$_GET['in'];
        $headerType ='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        $headerContent = 'attachment; filename="'.$_GET['out'].'.xlsx"';  // force-append the correct file extension
        break;
    default :
        exit;
        break;


}

ob_clean();
flush();

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: '.$headerType);
header('Content-Disposition: '.$headerContent);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
header('Content-Length: ' . filesize($inputFile));

readfile($inputFile);
if (strtolower($_GET['u'])==='y') @unlink($inputFile);
exit;