<?php

//====isLocked===============================================================================
//returns a key value array with elements:
//      'Result' => value:
//                      -1: error
//                      0: not locked
//                      1: locked
//      'Msg'     => error msg if result is -1
//      'LockedSince' => [if 'Result' is 1] # of minutes locked
//      'LockedDate' => [if 'Result' is 1] date locked
//      'LockedBy' => [if 'Result' is 1] User who locked the record
//      'LockedByDisplay' => [if 'Result' is 1] display value for who locked the record
//      'LockedByCurrentUser=> [if 'Result' is 1] bool. true if user locked is user checking
//      'TimedOut' => [if 'Result' is 1] bool. true if lock has timed out
///-------------------------------------------------------------------------------------------------
//@params:
///-------------------------------------------------------------------------------------------------
//tableName             : REQUIRED The full caspio URL to the table to be locked
//$whereInfo            : REQUIRED: array with 2 key value pairs to return the unique record in question:
        //'UniqueValueField' : REQUIRED name of field containing the unique field on how this record can be found
        //'UniqueValue'      : REQUIRED value of unique field used to locate the record
//$lockInfo             :key value pair array:
//      'LockedField' : name of boolean field that tells us if this record is locked. Default is 'Locked'
//      'LockedByField' : name of field that tells us who locked the record [ contains id/email]. Default is 'LockedBy'
//      'LockedByDisplayField' : name of field that tells us who locked the record [ contains id/email]. Default is 'LockedBy'

//      'LockedWhenField' : name of field that tells us Date/time last locked. Default is 'LockedWhen'
//$lockTimeout          :# of minutes until lock times out. Default is 30 min

function isLocked($tableName, $whereInfo, $lockInfo = array(), $lockTimeout = 30){
    global $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $returnVal = array('Result' => -1, 'Msg' => '');
    if ($tableName == "" || !is_array($whereInfo) || !array_key_exists('UniqueValueField', $whereInfo) || !array_key_exists('UniqueValue', $whereInfo) ||
        !is_array($lockInfo) || !array_key_exists('LockedBy', $lockInfo)
    ){
        $returnVal['Msg'] = 'Missing required parameter.';
    } else {
        $caspio = new Caspio();

        $lockedField = array_key_exists('LockedField', $lockInfo)? $lockInfo['LockedField']: 'Locked';
        $lockedByField = array_key_exists('LockedByField', $lockInfo)? $lockInfo['LockedByField']: 'LockedBy';
        $lockedByField_Display = array_key_exists('LockedByDisplayField', $lockInfo)? $lockInfo['LockedByDisplayField']: $lockedByField;

        $lockedWhenField = array_key_exists('LockedWhenField', $lockInfo)? $lockInfo['LockedWhenField']: 'LockedWhen';

        $recordParams = array('q' => '{
             "select": "'.$lockedField." as locked, ".$lockedByField.' as lockedby, '.$lockedByField_Display.' as lockedbydisplay, '.$lockedWhenField.' as lockedwhen",
             "where": "'.$whereInfo['UniqueValueField'].' = \''.$whereInfo['UniqueValue'].'\'"}');

        $query = $caspio->ExecGetRequest($Caspio_apiURL . rtrim($tableName,"/")."/rows", $Caspio_token, $recordParams);

        if($query === false){
            $returnVal['Msg'] = 'Error getting record: '.$Caspio_message;
        } else if(sizeof($query->Result) == 0){
            $returnVal['Msg'] = 'Cannot find record.';
        } else {
            $lockedRecord = $query->Result[0];

            //if record is locked, look at the time it was locked to see if it has timed out
            if($lockedRecord->locked){

                $date_locked = new DateTime($lockedRecord->lockedwhen); // Turn date locked into data object
                $diff = $date_locked->diff(new DateTime()); //get the difference in time compared to now
                $lockedminutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i; //get difference in minutes

                $returnVal['Result'] = 1;
                $returnVal['LockedDate'] = $date_locked->format('Y-m-d H:i:s'); //return date it was locked
                $returnVal['LockedSince'] = $lockedminutes;                     //return number of minutes locked
                $returnVal['LockedBy'] = $lockedRecord->lockedby;               //return who locked the record
                $returnVal['LockedByDisplay'] = $lockedRecord->lockedbydisplay;               //return who locked the record
                $returnVal["LockedByCurrentUser"] = $lockedRecord->lockedby == $lockInfo['LockedBy'];
                $returnVal['TimedOut'] = ($lockedminutes > $lockTimeout);

            } else $returnVal['Result'] = 0; //not locked.
        }
    }

    return $returnVal;
}


//====RecordLock===============================================================================
//attempts to lock record for the current user.
//returns a key value array with elements:
//      'Result' => value:
//                      -1: error
//                      0: could not lock bc already locked and not override.
//                      1: locked
//      'Msg'     => error msg if result is -1
//      'LockedSince' => [if 'Result' is 0/1] # of minutes locked
//      'LockedDate' => [if 'Result' is 0/1] date locked
//      'LockedBy' => [if 'Result' is 0/1 User who locked the record
//      'LockedByDisplay' => [if 'Result' is 0/1 display value for User who locked the record

///-------------------------------------------------------------------------------------------------
//@params:
///-------------------------------------------------------------------------------------------------
//tableName             : REQUIRED The full caspio URL to the table to be locked
//$whereInfo            : REQUIRED: array with 2 key value pairs to return the unique record in question:
//      'UniqueValueField' : REQUIRED name of field containing the unique field on how this record can be found
//      'UniqueValue'      : REQUIRED value of unique field used to locate the record
//$lockInfo             :REQUIRED key value pair array:
//      'LockedField'       : name of boolean field that tells us if this record is locked. Default is 'Locked'
//      'LockedByField'     : name of field that tells us who locked the record [ contains id/email]. Default is 'LockedBy'
//      'LockedBy'          : REQUIRED id/email of user locking record
//      'LockedByDisplayField'   : name of field that tells us the display value of the user locking record. If not passed display will be same as locked by
//      'LockedWhenField'   : name of field that tells us Date/time last locked. Default is 'LockedWhen'

//$lockTimeout          :# of minutes until lock times out. Default is 30 min
//$override        :Force it unlocked regardless of whether it is locked or not (Default: False).
//$notify          :If the record was locked, but had timed out within 12 hours, send the person who had it locked a notice that they lost their lock. (Default: True)

function RecordLock($tableName, $whereInfo, $lockInfo, $lockTimeout = 30, $override = false, $notify = true) {
    global $Caspio_apiURL, $Caspio_token, $Caspio_message;
    $returnVal = array('Result' => -1, 'Msg' => '');

    $notify_timeout_range = 60 * 12; // variable to hold the time range (minutes) in which a notice will be still be sent to a user even if the record is timed out. (12 hours rn)

    if ($tableName == "" || !is_array($whereInfo) || !array_key_exists('UniqueValueField', $whereInfo) || !array_key_exists('UniqueValue', $whereInfo) ||
        !is_array($lockInfo) || !array_key_exists('LockedBy', $lockInfo)
    ){
        $returnVal['Msg'] = 'Missing required parameter.';
    } else {
        $getLock = isLocked($tableName, $whereInfo, $lockInfo, $lockTimeout);

        if($getLock['Result'] < 0) return $getLock; //an error occurred.
        else{

            //go lock if...
            if($getLock['Result'] == 0 ||  //record not locked
                ($getLock['Result'] == 1 && $getLock['TimedOut']) || //lock has timed out
                ($getLock['Result'] == 1 && $getLock['LockedByCurrentUser']) || //locked but by the same user. we will refresh
                ($getLock['Result'] == 1 && $override)  //record is locked and not timed out, but we are overriding
            ) {

                //unlock record if it is not locked, or is locked and timed out, or is locked and this is an override
                $caspio = new Caspio();

                //get current time
                $currentTime = new DateTime();
                $currentTime->setTimezone(new DateTimeZone('UTC'));

                //get the field names. use defaults if not passed.
                $lockedField = array_key_exists('LockedField', $lockInfo)? $lockInfo['LockedField']: 'Locked';
                $lockedByField = array_key_exists('LockedByField', $lockInfo)? $lockInfo['LockedByField']: 'LockedBy';
                $lockedWhenField = array_key_exists('LockedWhenField', $lockInfo)? $lockInfo['LockedWhenField']: 'LockedWhen';

                //create the update array.
                $fieldList = array();
                $fieldList[$lockedField] = true;
                $fieldList[$lockedByField] = $lockInfo["LockedBy"];
                $fieldList[$lockedWhenField] = $currentTime->format('Y-m-d H:i:s');

                //where statement to get the record.
                $recordParams = array('q' => '{"where": "'.$whereInfo['UniqueValueField'].' = \''.$whereInfo['UniqueValue'].'\'"}');
                //attempt the update
                $updateLock = $caspio->ExecUpdateRequest($Caspio_apiURL . rtrim($tableName,"/")."/rows", $Caspio_token, $recordParams, $fieldList);

                if($updateLock === false || $updateLock < 1){
                    $returnVal['Msg'] = 'Error locking record: '.$Caspio_message;

                } else if($updateLock == 0){
                    $returnVal['Msg'] = 'Could not find record to lock.';

                } else {
                    //update successful

                    $returnVal['Result'] = 1;
                    $returnVal['LockedSince'] = 0; //0 minutes since we just locked it.
                    $returnVal['LockedDate'] = $currentTime->format('Y-m-d H:i:s'); //set to the lock time
                    $returnVal['LockedBy'] = $lockInfo["LockedBy"]; //set to this user.
                    $returnVal['LockedByDisplay'] = !array_key_exists("LockedByDisplay", $lockInfo)? $lockInfo["LockedBy"]: $lockInfo["LockedByDisplay"]; //set to this user.

                    $lockInfo['LockedByDisplay'] = $returnVal['LockedByDisplay'];
                    //if we are notifying, send email if record was previously locked and..
                    if($notify && function_exists('sendLockNotice') &&
                        $getLock['Result'] == 1 &&
                        !$getLock['LockedByCurrentUser'] && //this is not the same user who previously had it locked
                        (
                            ($getLock['TimedOut'] && $getLock['LockedSince'] < $notify_timeout_range) ||  //the locked timed out recently
                            (!$getLock['TimedOut'] && $override) //the lock was still in effect, but the user is doing an override.
                        )
                    ){
                        //notify
                        $returnVal['notify'] = sendLockNotice(true, $override, $lockInfo, $getLock); //TBD
                    }
                }
            } else {
                $getLock['Result'] = 0;                 // record is locked
                $returnVal = $getLock;
            }
        }
    }

    return $returnVal;
}


//====RecordUnLock===============================================================================
//attempts to unlock record if the current user has it locked or it is override.
//returns a key value array with elements:
//      'Result' => value:
//                      -1: error
//                      0: could not unlock bc another user has it locked and is not an override.
//                      1: Unlock successful
//      'Msg'     => error msg if result is -1
//      'LockedSince' => [if 'Result' is 0] # of minutes locked
//      'LockedDate' => [if 'Result' is 0] date locked
//      'LockedBy' => [if 'Result' is 0 User who locked the record]

///-------------------------------------------------------------------------------------------------
//@params:
///-------------------------------------------------------------------------------------------------
//tableName             : REQUIRED The full caspio URL to the table to be locked
//$whereInfo            : REQUIRED: array with 2 key value pairs to return the unique record in question:
//      'UniqueValueField' : REQUIRED name of field containing the unique field on how this record can be found
//      'UniqueValue'      : REQUIRED value of unique field used to locate the record
//$lockInfo             :REQUIRED key value pair array:
//      'LockedField'       : name of boolean field that tells us if this record is locked. Default is 'Locked'
//      'LockedByField'     : name of field that tells us who locked the record [ contains id/email]. Default is 'LockedBy'
//      'LockedBy'          : REQUIRED id/email of user unlocking record
//      'LockedWhenField'   : name of field that tells us Date/time last locked. Default is 'LockedWhen'
//$lockTimeout          :# of minutes until lock times out. Default is 30 min
//$notify          :If the record was locked, but had timed out within 12 hours, send the person who had it locked a notice that they lost their lock. (Default: True)
//$override        :Force it unlocked regardless of whether it is locked or not (Default: False).

function RecordUnLock($tableName, $whereInfo, $lockInfo, $lockTimeout = 30, $override = false, $notify = true) {
    global $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $returnVal = array('Result' => -1, 'Msg' => '');

    $notify_timeout_range = 60 * 12; // variable to hold the time range (minutes) in which a notice will be still be sent to a user even if the record is timed out. (12 hours rn)

    if ($tableName == "" || !is_array($whereInfo) || !array_key_exists('UniqueValueField', $whereInfo) || !array_key_exists('UniqueValue', $whereInfo) ||
        !is_array($lockInfo) || !array_key_exists('LockedBy', $lockInfo)
    ){        $returnVal['Msg'] = 'Missing required parameter.';
    } else {
        $getLock = isLocked($tableName, $whereInfo, $lockInfo, $lockTimeout);

        if($getLock['Result'] < 0) return $getLock; //an error occurred.
        if($getLock['Result'] == 0)$returnVal['Result'] = 1;  // record was already unlocked. so just say it was successful bc there is nothing to do
        // attempt to unlock if
        else if($getLock['Result'] == 1 &&
            //lock has timed out. OR //locked but by the same user as trying to unlock OR //this is an override
            ($getLock['TimedOut'] || $getLock['LockedByCurrentUser'] || $override)
        ) {
            $caspio = new Caspio();

            //get the field names. use defaults if not passed.
            $lockedField = array_key_exists('LockedField', $lockInfo)? $lockInfo['LockedField']: 'Locked';
            $lockedByField = array_key_exists('LockedByField', $lockInfo)? $lockInfo['LockedByField']: 'LockedBy';
            $lockedWhenField = array_key_exists('LockedWhenField', $lockInfo)? $lockInfo['LockedWhenField']: 'LockedWhen';

            //create the update array.
            $fieldList = array();
            $fieldList[$lockedField] = false;
            $fieldList[$lockedByField] = null;
            $fieldList[$lockedWhenField] = null;

            //where statement to get the record.
            $recordParams = array('q' => '{"where": "'.$whereInfo['UniqueValueField'].' = \''.$whereInfo['UniqueValue'].'\'"}');
            //attempt the update
            $updateLock = $caspio->ExecUpdateRequest($Caspio_apiURL . rtrim($tableName,"/")."/rows", $Caspio_token, $recordParams, $fieldList);

            if($updateLock === false){
                $returnVal['Msg'] = 'Error unlocking record: '.$Caspio_message;

            } else if($updateLock == 0){
                $returnVal['Msg'] = 'Could not find record to unlock.';

            } else {
                //update successful
                $returnVal['Result'] = 1;

                $lockInfo['LockedByDisplay'] = !array_key_exists("LockedByDisplay", $lockInfo)? $lockInfo["LockedBy"]: $lockInfo["LockedByDisplay"]; //set to this user.

                //if we are notifying, send email if..
                if($notify && function_exists('sendLockNotice') &&
                    !$getLock['LockedByCurrentUser'] && //this is not the same user who previously had it locked
                    (
                        ($getLock['TimedOut'] && $getLock['LockedSince'] < $notify_timeout_range) ||  //the locked timed out recently
                        (!$getLock['TimedOut'] && $override) //the lock was still in effect, but the user is doing an override.
                    )
                ){
                    //notify
                    $returnVal['notify'] = sendLockNotice(false, $override, $lockInfo, $getLock); //TBD

                }
            }
        } else {
            //record is locked! cannot unlock
            $getLock['Result'] = 0;
            return $getLock;
        }
    }


    return $returnVal;
}



