<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/phpspreadsheet/autoload.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;


class exportExcelClass
{
    // DOCUMENTATION -- Meta documentation at bottom of class

    // Public uses settings
    public $spreadsheet;            // spreadsheet object as defined by https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Spreadsheet.html

    private $headerRow = 1;             // row the header is on --- data is assumed to start in the next row
    private $defaultTrue = "TRUE";      // if user doesn't specify, this value will be used for bool true
    private $defaultFalse = "FALSE";    // if user doesn't specify, this value will be used for bool false

    // internal use
    private $newSpreadsheet = true;     // tracks whether the object has default worksheet still
    private $activeSheetName = "";      // tracks the name of the current worksheet
    private $rootFileName = "";         // file name created


    function __construct()
    {
        $this->spreadsheet = new Spreadsheet();

    }
    function __destruct()
    {
        if (isset($this->spreadsheet) && !$this->newSpreadsheet) {
            $this->spreadsheet->disconnectWorksheets();
            unset($this->spreadsheet);
        }

    }

    /**
     * addSheet:  Adds sheets to the workbook using the configuration variable
     * @param $config     -- configuration data
     * @param $sheetInfo  -- (string) name of the sheet to create, or array containing sheetName in key "name" and optionally tab color in key "rgbColor"
     * @param null $data  -- either an array of caspio-formatted data, name to a file containing HTML data, or null
     * @param null $index -- sheet number starting with 0.  If null, will be added to the end
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function addSheet($config, $sheetInfo, $data=null, $index=null) {
        if(is_string($sheetInfo))
            $sheetName = $sheetInfo;
        else {
            if(isset($sheetInfo["name"]))
                $sheetName = $sheetInfo["name"];
            else
                return;
            if(isset($sheetInfo["rgbColor"]))
                $tabColor =  $sheetInfo["rgbColor"];
        }
        // don't do anything of the configuration variable is not an array
        if (is_array($config)) {

            // if this is the first sheet remove the default sheet
            if ($this->newSpreadsheet) $this->spreadsheet->removeSheetByIndex(0);

            // no longer new
            $this->newSpreadsheet = false;

            $myWorkSheet = new Worksheet($this->spreadsheet, $sheetName);

            //if html add new sheet to front of spreadsheet for html reader and move it later
            if (is_string($data) && !empty($data)) {
                $this->spreadsheet->addSheet($myWorkSheet, 0);
            } else {
                $this->spreadsheet->addSheet($myWorkSheet, $index);
            }

            // remember the sheet name as the active sheet name
            $this->activeSheetName = $sheetName;
            $this->spreadsheet->setActiveSheetIndexByName($sheetName);

            if(isset($tabColor)){
                $this->spreadsheet->getActiveSheet()->getTabColor()->setRGB($tabColor);
            }

            // Loop through all the formatting information (i.e ignore "data")
            foreach ($config as $formatType => $formatInfo) {
                switch (strtolower($formatType)) {
                    case "column" :
                    case "col" :
                        foreach ($formatInfo as $column=>$cellInfo) {
                            foreach($cellInfo as $columnType=>$columnTypeValue) {
                                if (strtolower($columnType) === 'format') {
                                    foreach ($columnTypeValue as $formatArray)
                                        $this->rangeFormat($column, $formatArray);

                                } else if (strtolower($columnType) === 'headerformat') {
                                    foreach ($columnTypeValue as $formatArray)
                                        $this->rangeFormat($column.$this->headerRow, $formatArray);

                                } else if (strtolower($columnType) === 'header') {
                                    $this->spreadsheet
                                        ->getActiveSheet()
                                        ->setCellValue($column.$this->headerRow, $columnTypeValue);

                                } else if (strtolower($columnType) === 'comment') {
                                    $this->spreadsheet
                                        ->getActiveSheet()
                                        ->getComment($column.$this->headerRow)->getText()
                                        ->createTextRun($columnTypeValue);

                                } else {
                                    $this->columnFormat($column, $columnType, $columnTypeValue);
                                }
                            }
                        }
                        break;

                    case "row" :
                        foreach ($formatInfo as $rowAddress=>$rowInfo)
                            foreach ($rowInfo as $rowType=>$rowTypeValue) {
                                if (strtolower($rowType) === "height") {
                                    $this->spreadsheet
                                        ->getActiveSheet()
                                        ->getRowDimension($rowAddress)
                                        ->setRowHeight($rowTypeValue);
                                }
                            }
                        break;

                    case "range" :
                        foreach ($formatInfo as $range=>$rangeInfo) {
                            foreach($rangeInfo as $rangeType=>$rangeTypeValue) {
                                if (strtolower($rangeType) === 'format') {
                                    foreach($rangeTypeValue as $formatArray)
                                        $this->rangeFormat($range, $formatArray);
                                } else if (strtolower($rangeType) === 'merge' && $rangeTypeValue===true) {
                                    $this->spreadsheet
                                        ->getActiveSheet()
                                        ->mergeCells($range);
                                }
                            }
                        }
                        break;

                    case "cell" :
                        foreach ($formatInfo as $cellAddress=>$cellInfo)
                            foreach ($cellInfo as $cellType=>$cellTypeValue) {
                                if (strtolower($cellType) === "value") {
                                    $this->spreadsheet
                                        ->getActiveSheet()
                                        ->setCellValue($cellAddress, $cellTypeValue);
                                } else if (strtolower($cellType) === 'comment') {
                                    $this->spreadsheet
                                        ->getActiveSheet()
                                        ->getComment($cellAddress)->getText()
                                        ->createTextRun($cellTypeValue);
                                } else if (strtolower($cellType) === "format") {
                                    foreach ($cellTypeValue as $formatArray)
                                        $this->rangeFormat($cellAddress, $formatArray);
                                }
                            }
                        break;

                    default:   // ignore "data"
                        break;
                }

            }

            //if html sheet
            //after worksheet is populated with config data load in html data and move sheet to given index
            if (is_string($data) && !empty($data)) {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
                $this->spreadsheet = $reader->loadIntoExisting($data, $this->spreadsheet);
                if($index === null)
                    $index = $this->spreadsheet->getSheetCount();
                $this->spreadsheet->setIndexByName($sheetName, $index);
            }

            // If BOTH data configuration and data exists, process it.
            if (key_exists("data",$config) && $data !== null && is_array($data) && !empty($data)) {

                // loop through every data record
                foreach($data as $recordOffset=>$r) {

                    // look for special formatting field as part of the data record
                    if (isset($r->DataRowFormat))
                        $rowFormat = $r->DataRowFormat;
                    else
                        $rowFormat = [];

                    // loop through each data configuration item
                    foreach($config['data'] as $fieldName=>$fieldInfo) {

                        // make the keys case insensitive
                        $fieldInfo = array_change_key_case($fieldInfo);


                        // ----
                        // process the required config setting: "column"
                        // ----

                        // data config should have specified a column. If not it goes into column A
                        $column = key_exists("column",$fieldInfo) ? $fieldInfo['column'] : "A";

                        // initiatize the current row (where data will be written
                        $currentRow = $this->headerRow + 1 + $recordOffset;

                        // grab the value (from data) for this field (from config)
                        if (key_exists("formula",$fieldInfo)) {
                            $value = str_replace("|row#|",(string)$currentRow,$fieldInfo['formula']);
                        } else {
                            $value = $r->$fieldName;
                            $value = ($value === null) ? "" : $value;     // null must be set to empty string
                        }


                        // ----
                        // process the optional config settings for data
                        // ----

                        // does the field have a specified type?
                        if (key_exists("caspiotype",$fieldInfo)){
                            $typeInfo = $fieldInfo['caspiotype'];

                            // date: transform from caspio's date format to excel Excel format (using PHP date format as a middle man
                            if (strtolower($typeInfo)==="date/time" || strtolower($typeInfo)==="datetime")  {
                                if(!empty($value)){
                                    if(key_exists("dateformat", $fieldInfo)){
                                        $time = strtotime(str_replace("T"," ",$value));
                                        $date = date($fieldInfo["dateformat"], $time);
                                        $value = $date;
                                    } else
                                        $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(strtotime(str_replace("T"," ",$value)));
                                }

                                // yes/no (bool): transform from caspio's boolean format to designated value
                            } else if (strtolower($typeInfo)==="yes/no" || strtolower($typeInfo)==="bool" || strtolower($typeInfo)==="boolean"){

                                // assume default text values for true and fauls
                                $trueValue = $this->defaultTrue;
                                $falseValue = $this->defaultFalse;

                                // if type is boolean but value is not boolean, assume false
                                if (is_bool($value)) {

                                    // if user provided information on what text to use for true/false, use it
                                    if (key_exists("booltext",$fieldInfo) && is_array($fieldInfo['booltext'])) {
                                        $boolInfo = $fieldInfo['booltext'];
                                        $trueValue = isset($boolInfo[true]) ? $boolInfo[true] : $trueValue;
                                        $falseValue = isset($boolInfo[false]) ? $boolInfo[false] : $falseValue;
                                    }

                                    // set the value to the correct text value based on the boolean value
                                    $value = $value ? $trueValue : $falseValue;

                                } else {

                                    $value = $falseValue;
                                }
                            }
                        }

                        // put the resulting value in the proper column for this current row
                        $this->spreadsheet
                            ->getActiveSheet()
                            ->setCellValue(($column.$currentRow), $value );

                        if (!empty($rowFormat))
                            $this->rangeFormat($column.$currentRow,$rowFormat);

                    }

                }
            }
        }
    }

    /**
     * @param $config
     * @param $sheetInfo
     * @param $HTML
     * @param null $index
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function addSheetFromHTML($config, $sheetInfo, $HTML, $index=null) {

        // put the HTML text into a file in the temp directory,
        $tempname = tempnam(sys_get_temp_dir(),"htm_");
        $temp = fopen($tempname,"w+");
        fwrite($temp, $HTML);
        fclose($temp);

        // call addSheet using the temporary file we just created as the data source.
        $this->addSheet($config,$sheetInfo,$tempname,$index);

        unlink($tempname);

    }

    /**
     * saveFile: write the spreadsheet object to a .xlsx file
     * @return bool|string
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function saveFile(){
        $this->rootFileName = tempnam(sys_get_temp_dir() , "export_" );
        $writer = IOFactory::createWriter($this->spreadsheet, 'Xlsx');
        $writer->save($this->rootFileName);
        return $this->rootFileName;
    }

    /**
     * setHeaderRow: setter for $headerRow
     * @param $row
     */
    public function setHeaderRow($row) {
        if (is_numeric($row) && $row > 0)
            $this->headerRow = intval($row);
    }

    /**
     * setBooleanDefaults:  setter for $defaultTrue and $defaultFalse
     * @param $newTrueValue
     * @param $newFalseValue
     */
    public function setBooleanDefaults($newTrueValue, $newFalseValue) {
        if (is_string($newTrueValue)) $this->defaultTrue = $newTrueValue;
        if (is_string($newFalseValue)) $this->defaultFalse = $newFalseValue;
    }

    /**
     * setHeaderFilter:  Set the filters on the header row
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function applyHeaderFilter() {
        if(!$this->newSpreadsheet) {
            // figure out the last column of data
            $worksheet = $this->spreadsheet->getActiveSheet();
            $highestColumn = $worksheet->getHighestColumn();     // e.g. 'E'
            $this->spreadsheet->getActiveSheet()->setAutoFilter('A'.$this->headerRow.':'.$highestColumn.$this->headerRow);
        }
    }

    /**
     * @param $column
     * @param $increment
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function incrementColumn($column, $increment) {
        $colNumber = Coordinate::columnIndexFromString($column);
        $newIndex = ($colNumber + $increment) > 0 ? $colNumber + $increment : 1;
        return Coordinate::stringFromColumnIndex($newIndex);
    }
    /**
     * rangeFormat:  applyFromArray for a range of cells
     * @param $range
     * @param $styleArray
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function rangeFormat($range, $styleArray){
        if (is_array($styleArray) && !empty($styleArray)) {
            $this->spreadsheet
                ->getActiveSheet()
                ->getStyle($range)
                ->applyFromArray($styleArray);
        }
    }

    /**
     * columnFormat: whole-column formatting. Currently handles "width" only
     * @param $column
     * @param $format
     * @param $value
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function columnFormat($column, $format, $value) {
        switch (strtolower(trim($format))){
            case 'width' :
                if (strtolower(trim($value))==="auto") {
                    $this->spreadsheet
                        ->getActiveSheet()
                        ->getColumnDimension($column)
                        ->setAutoSize(true);
                } else {
                    $this->spreadsheet
                        ->getActiveSheet()
                        ->getColumnDimension($column)
                        ->setWidth($value);
                }
                break;
            default:
                break;

        }
    }


    // KEY FEATURES
    /*
    *  - Creates a workbook in the system temp directory (presumably for downloading)
     * - Can apply a wide range of cell and column formats
     *      - color, font, size, width, merge, height, etc.
     * - Can process caspio-formatted data and automatically output to spreadsheet
     *
   */

    // CONFIGURATION - Documentation
    /* ------------------------------------------------------------------------------------------------
     *  A configuration array is required
     *
     *    CONFIGURATION OPTIONS
     *    =====================
     *
     *    "column"/"col" => Array where Key is the column letter, like "A", "B", "C"....
     *                      Formats an entire column at once
     *
     *                      "header"(text):  Value to be placed as the header text in the spreadsheet
     *                      "headerformat (array of format arrays): formats "header" only. See "format arrays"
     *                      "width" (# | "auto"): specifies the excel units width of a column, or auto for autosizing
     *                      "comment" (string):  adds an excel comment to the header cell
     *                      "format" (array of format arrays): formats the column. See "format arrays"
     *
     *    "row" =>          Array where Key is the row number, like "1", "2", "3"....
     *                      Formats an entire row at once
     *
     *                      "height" (number):  excel height units
     *
     *    "range" =>        Array. Formats a specified range of cells
     *
     *                      "merge" (bool) : combines the range of cells into one cell
     *                      "format" (array of format arrays): formats the range. See "format arrays"
     *
     *    "cell" =>         Array. Formats a specific cell
     *                      "value" (string): hardcoded value to put into a cell.
     *                                         Sample Formula: "=C1+C2"
     *                                         Sample Number: "4321.12"
     *                                         Sample Text: "Hello World"
     *                                         Sample Date: "12/26/2018" or "2018-12-26"
     *                      "comment" (string):  adds an excel comment to the header cell
     *
     *   "data" =>          key/value Array that configures the placement and format of caspio-formatted data.
     *                      key: the caspio field name (unless it's a formula then the key can be anything unique as it will be ignored)
     *                      values: key/value array:
     *                          key: "column" => string with the column the caspio field will be placed in (Default="A")
     *                          key: "caspioType"=> string "yes/no", "boolean", or "bool" all mean the same thing that the caspio field is "yes/no"
     *                                              string "date/time", or "datetime" indicating the field is of datetime type
     *                          key: "boolText"=> [Only used if caspioType is "bool"]-- array [true|false]=>string representing what text to output if caspio value is true or false. i.e ("Yes","No")
     *                          key: "dateFormat=> [Only used if caspioType is "datetime"]-- string php datetime format string. If not included the date will be converted to excel date format
     *                          key: "formula" => string with a valid excel formula starting with "=".   Specify to substitute current row with |row#|
     *                                            formula example: "=countifs(A:A,A|row#|)"
     *
     *   SPECIAL FORMAT
     *   "DataRowFormat"    This key can be added to any row in the data itself. THe value should be an
     *                      Array of format information (see Format Arrays). Every cell with configured data
     *                      in that row will be formatted with this array.  Sample usage would be for subtotal rows.
     *
     *  FORMAT ARRAYS
     *  ===============
     *
     *  Format arrays ar array structures defined by phpspreadsheet that specifies what formats the cell or cells should take.
     *
     *  This class allows you to specify multiple formatting arrays in a row. Each formatting array is applied
     *  in the order they are configured
     *
     *  Outside the provided examples, the best documentation for the formatting options in these arrays can be found at:
     *     https://phpoffice.github.io/PhpSpreadsheet/1.2.1/PhpOffice/PhpSpreadsheet/Style.html
     *     https://phpspreadsheet.readthedocs.io/en/develop/topics/recipes/#valid-array-keys-for-style-applyfromarray
     *
     * ------------------------------------------------------------------------------------------------ */

    // PROPERTIES - Documentation
    /* ------------------------------------------------------------------------------------------------
     *
     *    spreadsheet :  spreadsheet object as defined by
     *                   https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Spreadsheet.html
     *
    * ------------------------------------------------------------------------------------------------ */

    // METHODS - Documentation
    /* ------------------------------------------------------------------------------------------------
    *
     * addSheet($config, $sheetName, $data=null, $index=null)
     *   adds a new sheet to the workbook
     *   $config (array). See CONFIGURATION documentation
     *   $sheetInfo (array) Array containing sheetName in key "name" and optionally tab color in key "rgbColor"
     *              or (string). Name you want to give the spreadsheet (max 31 characters)
     *   $data (file name or caspio-format data array).
     *              null:   no data
     *              array:  caspio-formatted array of objects to be added to the sheet; See special format "DataRowFormat" for formatting data rows.
     *              string: file name containing HTML code that will be used to create the spreadsheet;
     *    $index (int).  Spreadsheet order (0=first, null=last)
     *
     *
     * addSheetFromHTML($config, $sheetInfo, $HTML, $index=null)
     *   adds a new sheet to the workbook using the HTML text passed
     *   (this function simply saves the $HTML text to a temporary file and then calls "addShee" with that file name)
     *   for explanation of the other parameters, see "addSheet"
     *
     *
     * saveFile()
     *   saves the workbook to an .xlsx formatted file
     *   returns the name of the file (full path but no extension)
     *   - path / file name are determined by : tempnam(sys_get_temp_dir() , "export_" )
     *
     *
     * setHeaderRow($row)
     *   determines what row is the last header row.
     *   data will be written immediately after.
     *
     * public function applyHeaderFilter()
     *   puts a filter on the header row in the spreadsheet
     *
     * public function setBooleanDefaults($newTrueValue, $newFalseValue)
     *   default values for the text written to the spreadsheet when data has true/false
     *   -- the system defaults are TRUE and FALSE
     *   -- normally, however this should be determined by using a lookup field.
     *
     * public function incrementColumn($column, $increment)
     *   takes the provided $column letter (i.e. 'C' or 'AB') and increments by some number of columns. Negative increment is a decrement;
     *
     * ------------------------------------------------------------------------------------------------ */


}

