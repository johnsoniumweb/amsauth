<?php
require_once($_SERVER['DOCUMENT_ROOT']."/phplib/add2EQM_fn.php");   // verifies caspio REST API access
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/phplib.php');
require_once($_SERVER['DOCUMENT_ROOT']."/REST/caspioCorpAuth.php");   // verifies caspio REST API access
require_once($_SERVER['DOCUMENT_ROOT']."/ams_proto/figari/recordLockUtility_fn.php");   // verifies caspio REST API access

//PROGRAM TO ADD A RECORD TO THE EMAIL QUE (CORPORATE CASPIO ACCOUNT) WITH AN AJAX CALL. Can use /js/add2EQM.js for an easy js function to make the ajax call.
$returnMsg = "";
$return["Result"] = -1;
$return["ErrorMsg"]="";

// First Check for proper AJAX Call
if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    $return["ErrorMsg"] = "Invalid Ajax Call: Invalid headers.";
    // check to make sure all the required POST data was sent
} elseif (!isset($_POST['Fn']) || !isset($_POST['tableName']) || !isset($_POST['UniqueValueField']) || !isset($_POST['UniqueValue'])) {
    $return["ErrorMsg"] = "Invalid Ajax call: Required data is missing.";
} else {
    //AJAX variables received
    $fn = $_POST['Fn'];
    $tableName = $_POST['tableName'];
    $uniqueValueField = $_POST['UniqueValueField'];
    $uniqueValue = $_POST['UniqueValue'];
    $lockedBy = $_POST['LockedBy']; //session for user
//    $lockedWhen = (isset($_POST['LockedWhen'])|| trim($_POST['LockedWhen'] !== "")?$_POST['LockedWhen']: "LockedTimeOut";
    $lockedTimeOut =  $_POST['LockedTimeOut'];
    $override = $_POST['override'];
    $lockInfoArray = $_POST['lockInfoArray'];

    $currentTime = date("Y-m-d h:i:s");
    $lockInfo = array("LockedBy"=>"$lockedBy", "LockedWhen"=>"$currentTime", "LockedTimeOut"=>"$lockedTimeOut");
    $whereInfo = array("UniqueValueField"=>$uniqueValueField, "UniqueValue"=>$uniqueValue);

    if ($fn == "RecordLock") {
        RecordLock($tableName, $whereInfo, $lockInfo, $override);
    } elseif ($fn == "RecordUnLock") {
        RecordUnLock($tableName, $whereInfo, $lockInfo, $override);
    } elseif ($fn == "IsNotLocked") {
        IsNotLocked($tableName, $whereInfo, $lockInfo);
    } else {
        $return['Result'] =  false;
        $return['ErrorMsg'] = "Invalid  passed.";
    }
}
echo json_encode($return);



