<?php
$return = array();
$UploadDirectory    = '../tempfiles/'; //specify upload directory ends with / (slash)

$allowedTypes = array('application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/pdf', ' image/jpg', ' image/png');

function generateFileName()
{
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_";
    $name = "";
    for($i=0; $i<12; $i++)
        $name.= $chars[rand(0,strlen($chars))];
    return $name;
}

$return["Success"] = false;
$return["ErrorMsg"] = "";
$return['FileName'] = "";


if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) $return["ErrorMsg"] = "Invalid Ajax Call: Invalid headers.";
else if(!isset($_FILES["inputFile"])) $return["ErrorMsg"] = "Invalid Ajax Call: File missing.";
else if($_FILES["inputFile"]["error"] != UPLOAD_ERR_OK) $return["ErrorMsg"] = "File Upload Error.";
else if(!in_array(strtolower($_FILES['inputFile']['type']), $allowedTypes)) $return["ErrorMsg"] = 'Invalid File Type.';
else {
    //path to include a file that can overwrite the uploaddirectory/ include more validation.
    if(isset($_POST['include']) && $_POST['include'] != ""){
        if(file_exists($_SERVER['DOCUMENT_ROOT'].$_POST['include']."globals.php")) {
            require_once($_SERVER['DOCUMENT_ROOT'].$_POST['include']."globals.php");
        } else $return["ErrorMsg"] = "Include file not found. ";
    }

    $File_Name          = strtolower($_FILES['inputFile']['name']);
    $File_Ext           = substr($File_Name, strrpos($File_Name, '.')); //get file extention
    $Random_Number      = generateFileName(); //Random number to be added to name.
    $NewFileName        = $Random_Number."_".date("Y-m-d").$File_Ext; //new file name

    if(move_uploaded_file($_FILES['inputFile']['tmp_name'], $UploadDirectory.$NewFileName )) {
        $return['Success'] = true;
        $return['FileName'] = $NewFileName;
    } else $return["ErrorMsg"] .= "Error saving file.";
}


header('Content-Type: application/json');
echo json_encode($return);