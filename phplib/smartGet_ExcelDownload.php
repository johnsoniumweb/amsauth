<?php
require_once( 'customerDirectory.php');   // verifies caspio REST API access
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/excelDownload_fns.php');

header('Content-type: text/html; charset=utf-8');

// Global Variables
$return = array();    // start with empty array
$return["FileName"] = "";
$return["Message"]="";


// First Check for proper AJAX Call
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    $return["Message"] = "Invalid Ajax Call: Invalid headers.";
} else if (!isset($_POST['tableName']) || !isset($_POST['keyField']) || !isset($_POST['customerPath']) ) {// check to make sure all the required POST data was sent
    $return["Message"] = "Invalid Ajax call: Required data is missing.";
} else {
    $customerPath = trim($_POST['customerPath']);
    $tableName  = trim($_POST['tableName']);        //table to query
    $keyField   = trim($_POST['keyField']);         //unique key field name

    $select     = !isset($_POST['selectFields'])? "*": trim($_POST['selectFields']);     //select fields.
    $where      = !isset($_POST['whereClause'])? "": trim($_POST['whereClause']);        //where statement for query

    $columnTitles =  !isset($_POST['columnTitles'])? array(): json_decode($_POST['columnTitles'], true); //object {fieldname: 'Column title'}
    $title = !isset($_POST['title'])? "": $_POST['title'];          //the worksheet title.

    if ($tableName == "" || $keyField == ""  ) $return["Message"] = "Must pass a tablename and a keyfield.";
    else {
        try{
            $customer = new amsCustomer($customerPath);
            require_once( $_SERVER['DOCUMENT_ROOT'].$customer->getAuthIncludeUrl());
            $errorMsg = $customer->authorize();

            if($errorMsg === true || $errorMsg == "") $return["FileName"] =  getTableRecords2Excel($tableName, $select, $where, $keyField, $title, $columnTitles);

            $return["Message"] = $errorMsg;

        }
        catch (Exception $e){
            $return["Message"] = 'Exception caught: '.$e->getMessage();
        }
    }
}

echo json_encode($return);