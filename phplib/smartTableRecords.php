<?php
require_once( 'customerDirectory.php');   // verifies caspio REST API access
header('Content-type: text/html; charset=utf-8');

// Global Variables
$return = array();    // start with empty array
$returnMsg = "";

function getTableRecords($tableName,$findSQL)
{
    global $returnMsg,  $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $caspio = new Caspio();
    $queryArray = array('q'=>$findSQL, true);
    $found_json = $caspio->ExecGetRequest($Caspio_apiURL . $tableName."/rows", $Caspio_token, $queryArray);

    if (!$found_json) {
        $returnMsg = "Fatal Error: ".$Caspio_message;
        return -1;
    }
    $numberOfRows = sizeof($found_json->Result);
    if($numberOfRows > 0)  $returnMsg = json_encode($found_json);
    return $numberOfRows;
}

function addTableRecord($tableName, $findSQL, $fieldArray, $keyField)
{
    global $returnMsg,  $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $caspio = new Caspio();

    if ($findSQL != "") {
        $queryArray = array('q'=>$findSQL, true);
        $found_json = $caspio->ExecGetRequest($Caspio_apiURL .  $tableName."/rows", $Caspio_token, $queryArray);

        if (!$found_json) {
            $returnMsg = $findSQL; //"Fatal Error: -".$checkQuery."-".$Caspio_message;
            return -1;
        } else if(sizeof($found_json->Result) != 0) {
            $returnMsg = json_encode($found_json);
            return 0;
        }
    }

    // wasn't found or query check wasn't requested.
    if (trim($keyField) != "" ) {
        $result = $caspio->ExecInsertRequest($Caspio_apiURL . $tableName."/rows", $Caspio_token, $fieldArray,$keyField);
    } else {
        $result = $caspio->ExecInsertRequest($Caspio_apiURL . $tableName."/rows", $Caspio_token, $fieldArray);
    }

    if ((isset($keyField) && !$result) || (!isset($keyField) && $result < 0) ) {
        $returnMsg .= $Caspio_message;
    }
    return $result;
}


function delTableRecord($tableName, $findSQL)
{
    global $returnMsg,  $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $caspio = new Caspio();

    $del_params = array('q'=>$findSQL, true);
    $result = $caspio->ExecDeleteRequest($Caspio_apiURL . $tableName."/rows", $Caspio_token, $del_params);

    if ($result == -1) {
        $returnMsg = $Caspio_message;
    } else if ($result == 0) {
        $returnMsg = "No records were found to update.";
    }
    return $result;
}

function updTableRecord($tableName,$fieldArray, $findSQL)
{
    global $returnMsg,  $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $caspio = new Caspio();

    $queryArray = array('q'=>$findSQL, true);
    $result = $caspio->ExecUpdateRequest($Caspio_apiURL . $tableName."/rows", $Caspio_token, $queryArray, $fieldArray);

    if ($result == -1) {
        $returnMsg = $Caspio_message;
    } else if ($result == 0) {
        $returnMsg = "No records were found to update.";
    }
    return $result;
}


$return["Result"] = -1;
$return["Message"]="";


// First Check for proper AJAX Call
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    $return["Message"] = "Invalid Ajax Call: Invalid headers.";
} else if (!isset($_POST['tableName']) || !isset($_POST['findSQL']) || !isset($_POST['customerPath']) ) {// check to make sure all the required POST data was sent
    $return["Message"] = "Invalid Ajax call: Required data is missing.";
} else {
    $customerPath = trim($_POST['customerPath']);
    $tableName  = trim($_POST['tableName']);                                                            //table to query
    $findSQL    = trim($_POST['findSQL']);                                                                 //query
    $type       = !isset($_POST['type'])? "Get": trim($_POST['type']);
    $fieldArray = !isset($_POST['fieldArray'])? array(): json_decode($_POST['fieldArray'],true);        //array of field names:values for add or update.
    $keyField   = (isset($_POST['keyField']) && trim($_POST['keyField']) !="")? $_POST['keyField'] : "";  //on an add, return the value of this key field.

    if(!in_array($type, array("Add", "Update", "Delete", "Get")))$return["Message"] = "Invalid table action passed.";
    else if ($tableName == "" || ($findSQL == "" && $type != "Add") ) $return["Message"] = "Invalid value was passed.";
    else if(($type == "Add" || $type == "Update") && sizeof($fieldArray) == 0)$return["Message"] = "Must pass a field array on Add or Update.";
    else {
        try{
            $customer = new amsCustomer($customerPath);
            require_once( $_SERVER['DOCUMENT_ROOT'].$customer->getAuthIncludeUrl());
            $returnMsg = $customer->authorize();

            if($returnMsg == "" || $returnMsg){
                if($type == "Get") $return["Result"] = getTableRecords($tableName,$findSQL);
                else {
                    $logActionString = "";

                    if($type == "Add")     {
                        $return["Result"] = addTableRecord($tableName,$findSQL, $fieldArray, $keyField);
                        $logActionString = sprintf("[ADD] tableName(%s); fieldList(%s); checkQuery(%s); keyField(%s)", $tableName, json_encode($fieldArray), $findSQL, $keyField);
                    } else if($type == "Delete")  {
                        $return["Result"] = delTableRecord($tableName,$findSQL);
                        $logActionString = sprintf("[DELETE] tableName(%s); where(%s)", $tableName, $findSQL);
                    } else if($type == "Update")  {
                        $return["Result"] = updTableRecord($tableName,$fieldArray,$findSQL);
                        $logActionString = sprintf("[UPDATE] tableName(%s); where(%s); fieldList(%s);", $tableName, $findSQL, json_encode($fieldArray));
                    }

                    $logResult = sprintf("Result(%u); ErrorMsg(%s);", $return["Result"], $returnMsg);

                    //logging for add/delete/update.
                    if($customer->isLogEnabled()){
                        $customer->logAction(
                            array(
                                "username"  => $customer->getSessionVar("Email"),
                                "source" => "phplib/smartTableRecords.php - ".$type,
                                "action" => $type." Record(s)",
                                "logTable" => $tableName,
                                "logUpdate" => $logActionString,
                                "result" => $logResult
                            )
                        );
                    }
                }
            }

            $return["Message"] = $returnMsg;
        }

        catch (Exception $e){
            $return["Message"] = 'Exception caught: '.$e->getMessage();
        }
    }
}

echo json_encode($return);