<?php
/**
 * Created by PhpStorm.
 * User: Shaun Wolfe
 * Date: 12/26/2018
 * Time: 4:07 PM
 * @param $token
 * @param null $allowedTypes
 * @return int|string
 */

function upload_secure($token, $allowedTypes = null)
{

    // token cannot be empty
    if (empty($token))
        return (int)101;

    // token must be set as a session key
    if (!isset($_SESSION[$token]))
        return (int)102;

    // file Array must exist
    if (!isset($_FILES[$token]))
        return (int)103;

    // any file upload system errors?
    if ($_FILES[$token]["error"] != UPLOAD_ERR_OK)
        return (int)200 + (int)$_FILES[$token]["error"];

    // set the allowable upload types (caller can override)
    $allowedTypes = (is_array($allowedTypes)) && !empty($allowedTypes)
        ? $allowedTypes
        : [
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf',
            'image/jpg',
            'image/png',
            'text/plain'
        ];

    // file type must be in allowable types
    if (!in_array(strtolower($_FILES[$token]['type']), $allowedTypes))
        return (int) 105;

    // get the file extension
    $File_Name = strtolower($_FILES[$token]['name']);
    $File_Ext = pathinfo($File_Name, PATHINFO_EXTENSION); //get file extension

    // create a new file in temp directory as upload_[root file name]_[random sring].[file extension]
    $NewFileName = tempnam(sys_get_temp_dir(), "upload_".rtrim(basename($File_Name,$File_Ext),".")). ($File_Ext === "" ? "": ".".$File_Ext) ; //new file name

    // move the uploaded file to our new name
    if (move_uploaded_file($_FILES[$token]['tmp_name'], $NewFileName) === true) {

        return $NewFileName;  // return the new file name where it can be processed.

    } else {

        return (int) 106;

    }


}
