<?php
//  REQUIRED: Calling PHP program must load the appropriate CaspioAuth program from the REST directory.

//====Input Parameters ===============================================================================
//filename              : REQUIRED the Caspio table/view in the form \tables\filename\
//keyFieldName          : REQUIRED the table key field name (i.e UserKey). The value of this field will be returned back to you
//fieldValueArray       : REQUIRED the field value array that has data for update/insert. must have every field necessary for adding a record.
//
//uniqueFieldName       : OPTIONAL (can be an empty string).
//                          Either holds the unique_field name that will be used to see if it already exists...
//                          Or a '*' to indicate a custom where statement (that will return a unique record) will be passed as the unique field value.
//                          Or is an empty string which indicates to just do a blind add.
//uniqueFieldValue      : If uniqueFieldName is provided, this is REQUIRED, otherwise blank string
//                          This is the value (i.e. 'shaun@agencymaniasolutions.com' that will be used to try and find the
//                          record to reinstate.
// inactiveFieldName    : name of the field that indicates active or inactive. Default = 'Inactive'
//====Return Values ===============================================================================
//Boolean               :   false (boolean) means there was a serious error attempting to call the caspio API. Likely because the
//                              SQL statement was wrong or we couldnt authenticate.
// integer = 0          :   not added because a record with that unique value already exists AND is not inactivated.
// integer > 0          :   the value of the field (identified by the "keyFieldName" paramater) for the record added.
// integer =  -1        :   Couldn't authenticate to the caspio API
// integer =  -2        :   Update was attempted to reinstate but the query failed. See $Caspio_message
// integer =  -3        :   Add was attempted but the query failed. See $Caspio_message
// integer =  -4        :   Query to see if unqique key existed failed. See $Caspio_message
// integer =  -5        :   More than one record returned.
// integer =  -6        :   Record was added but the return key was not returned -- probably because it is invalid.

function addOrReactivate($filename, $keyFieldName, $fieldValueArray, $uniqueFieldName, $uniqueFieldValue, $inactiveFieldName = "Inactive") {
    global $Caspio_apiURL, $Caspio_token, $Caspio_message;

    if (!caspioAuth())  return -1;
    $caspio = new Caspio();
    $access_token = $Caspio_token;

    $uniqueWhere = "";
    if (trim($uniqueFieldName) === "*" && $uniqueFieldValue !== "") {
        $uniqueWhere = $uniqueFieldValue;
    } else if(trim($uniqueFieldName) !== ""){
        $uniqueWhere = addslashes($uniqueFieldName) .' = \'' . addslashes($uniqueFieldValue) . '\'';
    }

        // if a uniqueFieldName is provided, then try to update to reactivate first
    if (trim($uniqueWhere) !== "") {

        // check if the record already exists
        $get_params = array('q' => '{"select": "'.addslashes($keyFieldName)." as xyzKey, ".addslashes($inactiveFieldName).' as xyzInactive",
                          "where": "'. $uniqueWhere . '"}');

        $result = $caspio->ExecGetRequest($Caspio_apiURL.rtrim($filename,"/")."/rows", $access_token, $get_params);
        if ($result === false || $result < 0) {
            $Caspio_message .= "<br>".json_encode($get_params);
            return -4;
        }

        // if it exists and it is not inactive, then return back that this is a duplicate.
        if (!empty($result->Result) && !$result->Result[0]->xyzInactive) return 0;  // means the record was found.
//        else if(!empty($result->Result) && sizeof($result->Result) > 1) return -5;


        // if it exists, just reactivate it using update
        if (!empty($result->Result)) {
            $get_params = array('q' => '{"where": "'. $keyFieldName . ' = \''.$result->Result[0]->xyzKey.'\'"}');

            $fieldValueArray[$inactiveFieldName] = false;  // make sure inactive field name is set to false;
            $result2 = $caspio->ExecUpdateRequest($Caspio_apiURL.rtrim($filename,"/")."/rows", $access_token, $get_params, $fieldValueArray);
            if ($result2 === false || $result2 < 0) return -2;
            return $result->Result[0]->xyzKey;
        }
    }

    // If we get to here, we know we just need to add the record because  either
    //    1)  There was no unique field to test for
    //       -- OR --
    //    2)  The unqiue key value was not found so the record doesn't exist.
    $result = $caspio->ExecInsertRequest($Caspio_apiURL.rtrim($filename,"/")."/rows", $access_token, $fieldValueArray, $keyFieldName);
    if ($result === false) return -3;
    return $result;  // in this case the $result contains the value of the $keyFieldName


}