<?php
$return = array('fn' => '', 'msg' => '');

ini_set('display_errors', false);                       //turn off display errors since we are handling fatal errors by sending them back to the user in the return array.
register_shutdown_function(function(){
    global $data, $return;
    $data = null;

    $error = error_get_last();
    if($error !== null && $error['type'] === E_ERROR) {
        $return['msg'] = 'Fatal Error: '.$error['message'];
        echo json_encode($return);
    }
});

// First Check for proper AJAX Call
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    $return["msg"] = "Invalid Ajax Call: Invalid headers.";
} else if (!isset($_POST['data'])) {// check to make sure all the required POST data was sent
    $return["msg"] = "Invalid Ajax call: Required data is missing. Must pass record data.";
} else {
    $useSpout = !isset($_POST['spout'])? true: $_POST['spout'] == "true";      // an optional parameter. Spout is used to create the download unless false is passed (PhpExcel used instead)

    //include the excel file functions.
    if($useSpout) require_once $_SERVER['DOCUMENT_ROOT']."/phplib/excelDownload_fns.php";
    else require_once $_SERVER['DOCUMENT_ROOT'].'/PhpExcel/excelDownload_fns_PhpExcel.php';

    $data =  json_decode($_POST['data'], true);                     //data in an array of record objects (same as Caspio get return)

    $columnTitles =  !isset($_POST['columnTitles'])? array(): json_decode($_POST['columnTitles'], true); //object {fieldname: 'Column title'}
    $file = !isset($_POST['file'])? "":$_POST['file'] ;             //if a file name is passed, we are expecting an append.
    $title = !isset($_POST['title'])? "": $_POST['title'];          //if a file name is NOT Passed (create new file), this will be the worksheet title.

    $return['fn'] = createXcelFromArray($data, $file, $title, $columnTitles); //ready to create the excel file.
    $return['msg'] = $errorMsg;
}

echo json_encode($return);
