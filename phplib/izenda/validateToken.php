<?php

require_once $_SERVER['DOCUMENT_ROOT']."/phplib/php-jwt/src/JWT.php";
use \Firebase\JWT\JWT;

require_once 'shared.php';

$token = $_GET['access_token'];
$token = JWT::decode($token, JWTKEY, array('HS256'));

//TODO: validate that user permissions have not changed in caspio since token generation


if(time() <= $token->GenerationTime + $token->TTL)
    echo json_encode($token);
else
    //doesn't actually matter what's returned here as long as it isn't a valid izenda user object
    echo "Not authorized";
