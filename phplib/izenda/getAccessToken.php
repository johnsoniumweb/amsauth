<?php

require_once 'shared.php';

$privateKey = "-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgQCmaD1lAa6rN3eTEXzmynGvyiA+P8i7eptp8Z+cBWEiSWIO45VY
8XsDqEZxMx0UtZdo9NS+w0M/6zUfHmyW2BU5HoF4YJ4LZsdeHKYqcBH/mjac3dYT
4xv/20pDgwmiGLoyYoZMOuMX5ZJTycNhHZIr6CX9mxGouWkfIiStD5cBsQIDAQAB
AoGAP9LbOVJb2+96PT4H2bzrvbCYjUeJhd6QnSG/RegKSUw7/9np/iMgO1bfLaud
C8RaPjcpIcFimbFsvnK8014tRQy6bOYIgVAJN2fyyNolw4/J0JyPTes8zG7uidlx
Pu5NNCFo/5Vc9oxAfYAOvbe/xsH8mJvhkPRgSI2M270ctQECQQDUWDqgWbgB1FZQ
Qm/IP7usoa9TvXHzn9PbBHvbRmwwKTij9qh2x3XlL+aGvKJvKPTAKuUNgq1mzHiW
8S4wczF1AkEAyJ5Oh9R2/oHdaypcf+ltlm2m6mcRxvaigwM0lwc9aK/pdpCbeNvL
8x0SfbpXoOlP6DtayekglBtL9rIEEOrrzQJAdNzpxv4Zisg1iI5Hvsl0nBmPyGJm
qFG54rug/pjPSeIlzfcd1+EGPVATauu96bd8m5X/4WOd0wmNBGVPqlJ7UQJAW9Gw
qZRBfgyb9Y9m1JhWstGu4nuX9FMvX/0YlcMXhRF2LfDTZ4ZZmwSEUQz3TEtcajK4
Q88yPPcdVmR1XfTXRQJAJdCsWT9wLlEF2U4SHrwrbRwYEPrvDzdYbyDROyHh2EZ5
11qD8yEhQT/XDAEPKjnT4+vtSbXqLLUpiLWhSHf41w==
-----END RSA PRIVATE KEY-----";
//corresponds to public key: <RSAKeyValue><Modulus>pmg9ZQGuqzd3kxF85spxr8ogPj/Iu3qbafGfnAVhIkliDuOVWPF7A6hGcTMdFLWXaPTUvsNDP+s1Hx5sltgVOR6BeGCeC2bHXhymKnAR/5o2nN3WE+Mb/9tKQ4MJohi6MmKGTDrjF+WSU8nDYR2SK+gl/ZsRqLlpHyIkrQ+XAbE=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>

//Izenda sends a base64 encoded message which is the username and tenant in the user info object
//passed by validatetoken encrypted using the rsa public key stored in the configuration database
$encryptedIzendaMessage = base64_decode($_GET['message']);
$decrypted = null;
//convert private key to phplibrary compatible format
$privKey = openssl_get_privatekey($privateKey);
$success = openssl_private_decrypt($encryptedIzendaMessage, $decrypted, $privKey);

//generate a new token using the decrypted userinfo object and return it
$userObject = json_decode($decrypted);

echo generateToken($userObject->UserName, $userObject->TenantUniqueName);


