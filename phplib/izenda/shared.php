<?php

require_once $_SERVER['DOCUMENT_ROOT']."/phplib/php-jwt/src/JWT.php";
use \Firebase\JWT\JWT;

//FIXME: Replace with better key
const JWTKEY = "somekey";

function generateToken($userName, $tenantUniqueName){

    //TODO: validate that user exists in izenda, if not create user

    //TODO: validate that user's current permissions in caspio match izenda roles

    if($tenantUniqueName === NULL)
        $tenantUniqueName = '';
    $token = array(
        "UserName" => $userName,
        "GenerationTime" => time(),
        //One hour before token expires
        "TTL" => 3600,
        'TenantUniqueName' => $tenantUniqueName,
        'demo' => $_SESSION['demo']
    );
    $token = JWT::encode($token, JWTKEY);
    return $token;
}