<?php
require_once('getCaspioData.php');
require_once($_SERVER['DOCUMENT_ROOT']."/phplib/add2EQM_fn.php");


/*
 *  --------------------------------------------------------------------------
 *   Functions used to manage caspio I/O  (especially with Data Tables Editor
 *
 *   To use this several steps are required:
 *  1. the using php file must include appropriate REST caspio API library must be included before this file
 *  2. the using php file must create a config array in the desigated format (see sample)
 *  3. the uusing php file must have a set of specific functions setup (see sample):

 *  --------------------------------------------------------------------------
 */

//  builds the option list if needed.
function caspioDataManager_updateOptionListCache($config,$configField, $f, &$allOptionListCache) {
    global  $Caspio_apiURL, $Caspio_token;

    $fieldName = "row.".$f;
    $configLookup = $configField['lookup'];      // shortcut
    $inactivesExist = array_key_exists('inactiveField',$configLookup) && trim($configLookup["inactiveField"]!=="");

    // if this is a lookup field and we don't already have the data we need cached, we need to go get it.
    if ( !array_key_exists($fieldName,$allOptionListCache)){           // the cache key is the field name of the ID
        if (!caspioAuth()) {
            $allOptionListCache[$fieldName][] = array("label"=>"no value [API did not authenticate]","value"=>0);
        } else {
            $caspio = new Caspio();
            // lookups can happen in two ways:  Records from a table, or from a field type in caspio called "ListField"
            if (array_key_exists("listField",$configLookup) && is_bool($configLookup['listField']) && $configLookup['listField']) {
                // if we are getting from a list Field, use the appropriate Caspio call

                $tempData =  $caspio->ExecGetRequest($Caspio_apiURL . rtrim($configLookup["tableName"], "/") . "/columns/".$f, $Caspio_token, array());

                // format this data into the same format as if we had retrieved the data from rows from a lookup table.
                if ($tempData !==false && sizeof($tempData->Result) > 0) {
                    $data = [];
                    foreach ($tempData->Result->ListField as $k=>$d) {
                        array_push($data, (object) array("value"=>(int)$k,"label"=>$d, "Inactive"=>false));
                    }
                } else {
                    if ($tempData === false )
                        $data = $tempData;
                    else
                        $data=$tempData->Result; // assign to data (now it looks just like we got it from getCaspioData rather than a List Field
                }
            } else {
                // so it's a normal lookup table (not a list field).   Process accordingly.
                $select = $configLookup["select"];
                $orderby = (array_key_exists("orderby",$configLookup))? $configLookup["orderby"] : "";
                $groupby = (array_key_exists("groupby",$configLookup))? $configLookup["groupby"] : "";
                if ($inactivesExist) {
                    $select .= ", ".$configLookup["inactiveField"]. " as Inactive";
                    $orderby .= (array_key_exists("orderby",$configLookup))? ", ".$configLookup["inactiveField"] : "";
                    $groupby .= (array_key_exists("groupby",$configLookup))? ", ".$configLookup["inactiveField"] : "";
                }

                // uses the "where" clause because we need to find lookup labels even if they have been inactivated.
                // switch from ExecGetRequest to getCaspioData to get more than 1000 records
                $message = "";
                $queryArray = array(
                    "table"=>$Caspio_apiURL . rtrim($configLookup["tableName"],"/")."/rows",
                    "token"=>$Caspio_token,
                    "select"=> addslashes($select),
                    "where" => addslashes($configLookup["where"]));
                if ($orderby!=="") $queryArray["orderby"] = addslashes($orderby);
                if ($groupby!=="") $queryArray["groupby"] = addslashes($groupby);

                $data=getCaspioData ($caspio, $queryArray ,$message);

            }

            // Regardless of whether the option data came from a listField or rows from a lookup table,
            // process it the same because $data now looks identical
            if ($data===false) {
                $allOptionListCache[$fieldName][] = array("label"=>"Could not get Option Label.","value"=>0, "inactive"=>false);
            } else {
                if (sizeof($data) === 0) {
                    $allOptionListCache[$fieldName][] = array("label"=>"No Options Found.","value"=>0, "inactive"=>false);
                } else {
                    $previousLabel = ""; $outArray = [];
                    foreach($data as $r) {
//                        if (trim($r->label) != $previousLabel) {
                            $inactive = $inactivesExist ? $r->Inactive : false;
                            $outArray =  array("label"=>$r->label,"value"=>$r->value, "inactive"=>$inactive);

                            if (array_key_exists('parentValue',$r))
                                $outArray = array_merge($outArray,array("parentValue"=>$r->parentValue));

                            if (array_key_exists("groupFields",$configLookup) && is_array($configLookup['groupFields']) && !empty($configLookup['groupFields']))
                                foreach($configLookup['groupFields'] as $groupField)
                                    $outArray = array_merge($outArray,array($groupField=>$r->$groupField));

                            $previousLabel = trim($r->label);

                            $allOptionListCache[$fieldName][]=$outArray;
//                        }
                    }
                }
            }
        }
    };
}

// Given a record from the table, it will format into a response the client is expecting.
function caspioDataManager_getSingleRecordResponse($config,$r,$params,&$allOptionListCache) {

    foreach ($config['row'] as $f=>$row)  {
        // know whether the current field was configured as 'custom'  (or virtual in the caspio terminology)
        $isCustomField = array_key_exists('custom',$row)        // 'custom' option was provided
            && is_bool($row['custom'])                          // 'custom' option is a boolean
            && $row['custom'];                                  // 'custom' option is true

        $isReferToField = (array_key_exists('lookup',$row)
            && array_key_exists('groupParent',$row['lookup'])        // 'groupParent' option was provided
            && $row['groupParent']!="");                           // 'groupParent' option is not blank

        $isList = array_key_exists('lookup',$row) && array_key_exists('listField', $row['lookup']) && $row['lookup']['listField'] === true;
        // If this is a lookup field, update the options cache. (it will look first to see if it already exists).
        if (array_key_exists('lookup',$row)) caspioDataManager_updateOptionListCache($config,$row, $f, $allOptionListCache);

        if ($isReferToField) {
            
            $value = $allOptionListCache[$row['lookup']['groupParent']][$f];
        } else {
            // set the field value (either from the database record itself or custom value function)
            if ($isCustomField) $value = caspioDataManager_customizeCustomValue($f,$r,$params,$allOptionListCache);
            else if($isList && is_object($r->$f)){
                $main[$f] = $value = array_keys((array) $r->$f);
            } else $value = (empty($r) ? "" : $r->$f) ;
        }

        $main[$f] = caspioDataManager_customizeFormat($f,$value,$config);

        // if this is a lookup field, find the proper lookup label based on the value;
        if (array_key_exists('lookup',$row)) {
            // Find the proper option value
            $optionValue = [];
            if ($value == 0 && array_key_exists("OnZeroLabel",$row['lookup'])) {
                $record[$row['lookup']['id']] = array("label"=>$row['lookup']["onZeroLabel"]);
            } else {
                if (!is_array($allOptionListCache["row.".$f])) {
                    $record[$row['lookup']['id']] = array("label"=>"Invalid Option Structure(".$f."): ".json_encode($allOptionListCache["row.".$f]));
                } else {
                    if($isList) {
                        //if this is a list field, get the selected options and put them in a comma delimited list
                        if(sizeof($value) > 0){
                            $selected = array();
                            foreach($allOptionListCache["row.".$f] as $o) {
                                if (in_array($o["value"], $value)) array_push($selected, $o["label"]);
                            }

                            if(sizeOf($selected) > 0) $optionValue = array("label"=> implode(", ",$selected));              // if the value is found, return the appropriate label
                        }
                    } else {
                        foreach($allOptionListCache["row.".$f] as $o) {
//                    $useValue = array_key_exists("parentValue",$o) ? 'parentValue' : "value";
                            if ($o["value"] == $value) {
                                $optionValue = array("label"=>$o["label"]);              // if the value is found, return the appropriate label
                                break;
//                    } else {
//                        $optionValue = array("label"=>"parent: ".json_encode($o));
                            }
                        }
                    }

                    $record[$row['lookup']['id']] = !empty($optionValue)? $optionValue : array("label"=>"No Valid Options");
                }
            }


        }

    }
    if (!empty($r)) {
        $record['DT_RowId'] = $r->DT_RowID;               // need to put in the unique key
        $record['row'] = $main;
    } else $record = [];

    return $record;
}

// Scrubs the data before inserting/updating database. For example:  $1,000.00 would become 1000.00
function caspioDataManager_scrub($config,&$fieldArray) {
    $row = $config["row"];
    foreach ($fieldArray as $field=>&$value ) {
        if (array_key_exists($field,$row)) {
            $type = (array_key_exists("type",$row[$field])) ? strtolower($row[$field]["type"]) : "text255";
            $stripHTML = ((array_key_exists("allowHTML",$row[$field])) && $row[$field]["allowHTML"] ) ? false : true;
            switch ($type) {
                case "num" :
                case "number" :
                    $value = filter_var($value,FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
                    break;
                case "percent" :
                    $value = filter_var($value/100,FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
                    break;
                case "int" :
                case "integer" :
                    $value = intval(filter_var($value,FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
                    break;
                case "bool" :
                case "boolean" :
                    break;
                case "list" :
                    if(is_array($value)){
                        foreach ($value as $i=>$v){
                            $value[$i] = (int)$v;
                        }
                    }
                    break;
                case "text64000":
                    if ($stripHTML) $value= strip_tags($value);
                    break;
                case "text255":
                default:
                    if ($stripHTML) $value= strip_tags($value);
                    if (strlen($value) > 255) $value = substr($value,0,255);
                    break;
            }

        }
    }
}

//
// function to set match the $_SESSION state with the $config['state']
function caspioDataManage_setState(&$config, $section, $fieldName,$valueArray) {
    $sessionID = isset($config['setup']['sessionID']) ? $config['setup']['sessionID'] : $config['setup']['tableName'];

    // put the array into the field name in the session
    $_SESSION[$sessionID][$section][$fieldName] = $valueArray;

    // reset the config state value;
    $config['state'] = $_SESSION[$sessionID];
}

//
// function to read the data from the caspio table,
// iterating through the cascading fields, if necessary.
function caspioDataManage_readOptions($config,$currentFieldName, $lookupConfig,$id) {
    global $Caspio_apiURL, $Caspio_token;


    // If this field is part of a cascading chain, we need to build a where clause by
    //  climbing back UP the chain to take into account parts of the cascade that are set.
    //  FIELD NAME for the cascade is in the $config, and the previous link is determined by the "cascadeValueField"
    //  VALUE field for each cascade field is maintained in the $config['state']['groupCascade']  field;
    $whereArray  = []; $iterativeLookupConfig = $lookupConfig; $iterativeFieldName = $currentFieldName;
    while (array_key_exists('cascadeValueField',$iterativeLookupConfig)) {            // as long as there is a cascade field, iterate
        $cascadeFieldName = $iterativeLookupConfig['cascadeValueField'];              //   grab the name of the cascade field for convienienc

        if (array_key_exists($cascadeFieldName,$config['row'])                        // if the config field referenced by the cascadeField exists
            && array_key_exists("lookup",$config['row'][$cascadeFieldName]))          //   and it has it's own lookup configuration, we're in business
        {
            $value = $config['state']['groupCascade'][$iterativeFieldName]['value'];    // Value is held in the 'state' cache under the current field name
            $whereArray[] = $cascadeFieldName."='".$value."'";                          //   keep track of this FIELDNAME=VALUE combination
            $iterativeLookupConfig = $config['row'][$cascadeFieldName]['lookup'];       //   Advance the iteration back one link in the chain
            $iterativeFieldName = $cascadeFieldName;                                    //   the current field name is now back one field too.
            }
    }

    // setup for the Caspio API call using information from the $config
    $table = $lookupConfig['tableName'];
    $caspio = new Caspio();

    $_SESSION['debug-listfield2'] = json_encode($lookupConfig);
    $select = $lookupConfig['select'];
    $orderby = (array_key_exists("orderby",$lookupConfig))? $lookupConfig["orderby"] : "";

    $where = array_key_exists('where', $lookupConfig) ? $lookupConfig['where'] : "";  // standard where still applies
    $where .= (trim($where) == "") ? "" : " and ";

    // rest of the  where claus comes from the list of cascading field=value combinations created above.
    $where .= implode(" and ",$whereArray);

    // build query array
    // Get and return the data.
    // switch from ExecGetRequest to getCaspioData to get more than 1000 records
    $message = "";
    $queryArray = array(
        "table"=>$Caspio_apiURL . rtrim($table, "/") . "/rows",
        "token"=>$Caspio_token,
        "select"=> addslashes($select),
        "where" => addslashes($where));
    if ($orderby!=="") $queryArray["orderby"] = addslashes($orderby);

    return getCaspioData ($caspio, $queryArray ,$message);


}

//
// 'options' action Main processor
function caspioDataManager_getOptions(&$config, $params)
{
    global $Caspio_message;

    // make sure the changed field name and ID from previus field were passed.
    if (empty($params) || !isset($params['fieldName']) || !isset($params['id'])) {
        $output['error'] = "Invalid syntax on attempt to get more options";
        return $output;
    }

    // remove the "row." from the prefix if necessary
    $fieldName = strtolower(substr($params['fieldName'],0,4))==="row." ? substr($params['fieldName'],4) : $params['fieldName'];
    $id = $params['id'];


    // check for combinations of lookup/groupParent/cascadeValueField that need to be present for this to work.
    if (   !array_key_exists('row', $config)
        || !array_key_exists($fieldName, $config['row'])
        || (!array_key_exists('lookup', $config['row'][$fieldName])
            && !array_key_exists('groupParent', $config['row'][$fieldName]))
        || (array_key_exists('lookup', $config['row'][$fieldName])
            && !array_key_exists('cascadeValueField', $config['row'][$fieldName]['lookup']))) {
            $output['error'] = "Invalid configuration: ".$fieldName;
        return $output;
    }

    // remember this request in case it is needed for future cascading.
    caspioDataManage_setState($config, "groupCascade", $fieldName,array("value"=>$id));  // fieldname without the row. is the current value;

    if (!array_key_exists('groupParent', $config['row'][$fieldName])
         && array_key_exists('lookup',$config['row'][$fieldName])) {
        // process normally
        $lookup = $config['row'][$fieldName]['lookup'];

        $dataResult = caspioDataManage_readOptions($config,$fieldName, $lookup, $id);

        // if catastrophic query failure, return false
        if ($dataResult === false) {
            $output['error'] = "Invalid call to Database. Could not retrieve Options. Contact support@agencymaniasolutions.com".$Caspio_message;
            return $output;
        } else {
            $previousLabel = ""; $previousValue = ""; $newArray = [];
            foreach($dataResult as $o) {
                if (trim(strtolower($o->label)) != $previousLabel && (trim(strtolower($o->value)) != $previousValue)) {
                    $previousLabel = trim(strtolower($o->label)); $previousValue = trim(strtolower($o->value));
                    $newArray[] = $o;
                }
            }
            $output['options']["row." . $fieldName] = $newArray;
        }
    }
    return $output;
}

//
// 'remove' action Main processor
function caspioDataManager_deleteData($config,$keyvalList, &$errorMessage){
    global  $Caspio_apiURL, $Caspio_token, $Caspio_message;
    if (!caspioAuth())  {$errorMessage = "Could not authenticate to API.";return false;}
    $caspio = new Caspio();

    if ($config["setup"]["inactiveField"] === "") {
        // if Inactive name is blank, then physically delete, otherwise mark as deleted.
        $count = $caspio->ExecDeleteRequest($Caspio_apiURL . rtrim($config["setup"]["tableName"],"/")."/rows", $Caspio_token,
            array('q'=>'{"where":"'.addslashes($config["setup"]["keyField"]).' in ('.addslashes($keyvalList).')"}'));
    } else {
        // if Inactive name is NOT blank, Update to "Inactivate"
        $count = $caspio->ExecUpdateRequest($Caspio_apiURL . rtrim($config["setup"]["tableName"],"/")."/rows", $Caspio_token,
            array('q'=>'{"where":"'.addslashes($config["setup"]["keyField"]).' in ('.addslashes($keyvalList).')"}'),
            array($config["setup"]["inactiveField"]=>true));
    }
    if ($count===false) {$errorMessage = "Database Error attempting Delete: ".$Caspio_message;return false;}
    return $count;
}

//
// 'edit' action Main processor
function caspioDataManager_updateData($config,$keyval, $fields, $params, &$allOptionListCache,&$errorMessage ){
    global  $Caspio_apiURL, $Caspio_token, $Caspio_message;
    if (!caspioAuth())  {$errorMessage="Could not Authenticate to API"; return false;}

    $keyname = $config['setup']['keyField'];

    $where = $keyname." = ".$keyval;
    $caspio = new Caspio();
    $queryArray = array('q'=>'{"where":"'.addslashes($where).'"}');

    // separate the custom and non-custom fields
    $selectArray = []; $fieldArray = []; $customFieldArray=[];
    foreach($config['row'] as $fieldName=>$r)
        if (!array_key_exists('custom',$r) || !is_bool($r['custom']) || !$r['custom']) {   // if custom not config'd or it is but it's false
            $selectArray[] = $fieldName;
            if (array_key_exists($fieldName,$fields) && !array_key_exists('readonly',$r) && !is_bool($r['readonly']) && !$r['custom'])
                $fieldArray[$fieldName] = $fields[$fieldName];
        } else {
            if (array_key_exists($fieldName,$fields))
                $customFieldArray[$fieldName] = $fields[$fieldName];
        }

    caspioDataManager_preCreateOrEdit('edit',$config,$fieldArray,$params);
    caspioDataManager_scrub($config,$fieldArray);

    // update the data; FieldArray is blank if the update is just to a custom field (which is wierd but possible with inline editing)
    if (!empty($fieldArray)) {
        $count = $caspio->ExecUpdateRequest($Caspio_apiURL .rtrim($config['setup']['tableName'],"/")."/rows",$Caspio_token, $queryArray, $fieldArray);
        if ($count===false || $count==-1) {$errorMessage="Could not update: ".json_encode($queryArray).json_encode($fieldArray).$Caspio_message; return false;}
    }

    // build the select clause
    $select = $config["setup"]["keyField"]." as DT_RowID, ".implode(", ",$selectArray);

    // Get the data back (need full record)
    $caspio = new Caspio();
    // intentionally left as ExecGetRequest not getCaspioData to limit change. This is only a single record get so the 1000 limit is not an issue
    $data = $caspio->ExecGetRequest($Caspio_apiURL . rtrim($config["setup"]["tableName"],"/")."/rows", $Caspio_token, array('q'=>'{"select":"'.addslashes($select).'","where":"'.addslashes($where).'"}'));

    if ($data === false) {$errorMessage="Could not retrieve updated data: ".$Caspio_message.json_encode(array('q'=>'{"select":"'.addslashes($select).'","where":"'.addslashes($where).'"}')); return false;}
    if (sizeof($data->Result) === 0) {$errorMessage="Updated record not found."; return false;};

    // add back the custom fields
    $record = $data->Result[0];
    foreach ($customFieldArray as $k=>$c) $record->$k = $c;

    if (function_exists('caspioDataManager_postCreateOrEdit'))
        caspioDataManager_postCreateOrEdit('edit',$config,$record,$params);

    return caspioDataManager_getSingleRecordResponse($config,$record,$params,$allOptionListCache);
}

//
// 'create' action Main processor
function caspioDataManager_addData($config,$fields,$params,&$errorMessage,&$allOptionListCache) {
    global  $Caspio_apiURL, $Caspio_token, $Caspio_message;


    // separate the custom and non-custom fields
    $selectArray = []; $fieldArray = []; $customFieldArray=[];
    foreach($config['row'] as $fieldName=>$r)
        if (!array_key_exists('custom',$r) || !is_bool($r['custom']) || !$r['custom']) {   // if custom not config'd or it is but it's false
            $selectArray[] = $fieldName;
            if (array_key_exists($fieldName,$fields) && !array_key_exists('readonly',$r) && !is_bool($r['readonly']) && !$r['custom'])
                $fieldArray[$fieldName] = $fields[$fieldName];  // array of fields that can be written to the DB.
        } else {
            if (array_key_exists($fieldName,$fields))
                $customFieldArray[$fieldName] = $fields[$fieldName];
        }

    $customizeReactivateUniqueValue =  caspioDataManager_preCreateOrEdit('create',$config,$fieldArray,$params);
    caspioDataManager_scrub($config,$fieldArray);
//    foreach ($fieldArray as &$f) {
//        $fieldArray
//    }

    if (trim($config['setup']['reactivateUnique']) == "")
        $key= addOrReactivate($config['setup']['tableName'], $config['setup']['keyField'], $fieldArray, "", "");
    else
        $key = addOrReactivate($config['setup']['tableName'], $config['setup']['keyField'], $fieldArray, $config['setup']['reactivateUnique'],$customizeReactivateUniqueValue);

    if ($key === false) {$errorMessage="Add Request failed. Contact support@agencymaniasolutions.com"; return false;}
    if ($key < 0) {$errorMessage="Unexpected Error #: ".$key." Message: ".$Caspio_message; return false;}
    if ($key === 0){
        $errorMessage= (array_key_exists("message",$config) && array_key_exists("recordExists",$config["message"])) ?
            $config["message"]["recordExists"] : "Record could not be added because this data already exists.";
        return false;
    }

    $where = $config["setup"]["keyField"]."=".$key;
    // build the select clause
    $select = $config["setup"]["keyField"]." as DT_RowID, ".implode(", ",$selectArray);


    // Get the data
    $caspio = new Caspio();
    // intentionally left as ExecGetRequest not getCaspioData to limit change. This is only a single record get so the 1000 limit is not an issue
    $data = $caspio->ExecGetRequest($Caspio_apiURL . rtrim($config["setup"]["tableName"],"/")."/rows", $Caspio_token, array('q'=>'{"select":"'.addslashes($select).'","where":"'.addslashes($where).'"}'));

    if ($data === false) {$errorMessage="Could not retrieve updated data: ".$Caspio_message.json_encode(array('q'=>'{"select":"'.addslashes($select).'","where":"'.addslashes($where).'"}')); return false;}
    if (sizeof($data->Result) === 0) {$errorMessage="Updated record not found."; return false;};

    // add back the custom fields
    $record = $data->Result[0];
    foreach ($customFieldArray as $k=>$c) $record->$k = $c;

    if (function_exists('caspioDataManager_postCreateOrEdit'))
        caspioDataManager_postCreateOrEdit('create',$config,$record,$params);

    return caspioDataManager_getSingleRecordResponse($config,$record,$params,$allOptionListCache);
}

//
// 'load' and 'get' action Main processor
function caspioDataManager_loadData($config,$action,$params,&$ErrorMessage,&$allOptionListCache) {
    global  $Caspio_apiURL, $Caspio_token;

    // get list of fields: one list for database only fields, the other includes custom fields too
    $dataFieldNames = []; $customFieldNames=[];
    foreach($config['row'] as $fieldName=>$rowConfig ){
        if (!array_key_exists("custom",$rowConfig) || !is_bool($rowConfig['custom']) || !$rowConfig['custom'])
            $dataFieldNames[] = $fieldName;
        else $customFieldNames[] =$fieldName;

    }
    // get where clause: initial setup 'where' plus customized.
    $where = (array_key_exists("where",$config["setup"])) ? $config["setup"]["where"]." " : "";
    $where .= caspioDataManager_customizeWhere($action,$params);

    if ($action === 'get') {
        $keyList = implode(", ",$params);
        $where .= " and ".$config['setup']['keyField']." in (".$keyList.") ";
    }
    // build the select clause
//    $select = $config["setup"]["keyField"]." as DT_RowID, ".implode(", ",$dataFieldNames);
    $select = $config["setup"]["keyField"]." as DT_RowID, *";
    // Get the data
    $caspio = new Caspio();
    // switch from ExecGetRequest to getCaspioData to get more than 1000 records
    $message = "";
    $data=getCaspioData ($caspio, array(
            "table"=>$Caspio_apiURL . rtrim($config["setup"]["tableName"],"/")."/rows",
            "token"=>$Caspio_token,
            "select"=> addslashes($select),
            "where" => addslashes($where))
        ,$message);

    // if catastrophic query failure, return false
    if ($data===false) {
        $ErrorMessage = "Invalid call to Database. A message has been sent to the System Administrators. Please try to load this page again.";
        alertSupport("caspioDataManager V2","Query failure on Load",
            "Message: ".$message."<br>Select:".addslashes($select)."<br>Where: ".addslashes($where)."<br>");
        return false;
    };

    // build the array to send back to the client
    $return = [];
    if (!empty($data)) {
        foreach($data as $r) {
            // format the data into the proper response the client is expecting (including option values)
            $record = caspioDataManager_getSingleRecordResponse($config,$r,$params,$allOptionListCache);
            array_push($return,$record);
        }
    } else {
        // run this once just to get the optionlist cache
        $record = caspioDataManager_getSingleRecordResponse($config,[],$params,$allOptionListCache);
    }

    return $return;
}


function caspioDataManager($config,$post) {
    global $Caspio_message;
    if (session_status() == PHP_SESSION_NONE) session_start();
    $output = [];

    if (!caspioAuth())  { $output['error'] = "Could not authenticate to API subsystem"; return $output;};
    if (!array_key_exists("setup",$config)) { $output['error'] = "Invalid config: setup"; return $output;};
    if (!array_key_exists("keyField",$config["setup"])) { $output['error'] = "Invalid config: keyField"; return $output;};
    if (!array_key_exists("tableName",$config["setup"])) { $output['error'] = "Invalid config: tableName"; return $output;};
    if(!isset($post['action'])) {$output['error'] = "No Action was provided in post."; return $output;}

    $sessionID = isset($config['setup']['sessionID']) ? $config['setup']['sessionID'] : $config['setup']['tableName'];

    $output['data'] = [];

    // handle incoming $_POST variables
    $action = $post['action'];
    $data   = isset($post['data'])   ? $post['data'] : [];
    $params = isset($post['params']) ? $post['params'] : [];


    if ($action === 'load' )
        $_SESSION[$sessionID] = [];
    $config['state'] = $_SESSION[$sessionID];

    // switch to support various actions
    //    load   : get the initial data set.
    //    create : DataTable Editor plugin makes this call when a record needs added.
    //    edit   : DataTable Editor plugin makes this call when a record needs updated.
    //    remove : DataTable Editor plugin makes this call when a record needs deleted.
    //    options: used for getting <option> values in a cascading select situation.
    //    get    : gets one or more data records using the id (or id's) passed in an array under 'params'
    //
    switch ($action) {

        case 'load' :     // initial loading of the data
            $ErrorMessage = "";
            $allOptionListCache = [];
            $data = caspioDataManager_loadData($config,$action,$params,$ErrorMessage,$allOptionListCache);
            if ($data === false)
                $output['error'] = 'Problem loading data: '.$ErrorMessage." (".$Caspio_message.")";
            else {
                if ($ErrorMessage === "") {
                    $output['data'] = $data;   // set the data in the response

                    // for the option list response, we need to only show options that are not 'inactive'
                    if (!empty($allOptionListCache)) {                        // only if there are some options.
                        foreach ($allOptionListCache as $id=>$optionList) {   // loop through each set of options (each <select>)
                            $tempOutput[$id] = [];                            // initiatlize the holding array
                            foreach($optionList as $options)                  // loop through all the <options> in the select
                                if (!$options['inactive'])                    // if not 'inactive' add it to the list
                                    $tempOutput[$id][]=$options;
                        }
                    }
                    if (!empty($tempOutput)) $output["options"] = $tempOutput;  // if we have any options left, put them out.
                } else
                    $output['error'] = $ErrorMessage;
            }

            break;


        case 'get' :     // initial loading of the individual keys (held in $params)
            $ErrorMessage = "";
            $allOptionListCache = [];
            if (empty($params)) {$output['error']="No IDs were provided"; return $output;}
            $data = caspioDataManager_loadData($config,$action, $params,$ErrorMessage,$allOptionListCache);
            if ($data === false)
                $output['error'] = 'Problem loading data: '.$Caspio_message;
            else {
                if ($ErrorMessage === "") {
                    $output['data'] = $data;   // set the data in the response

                    // for the option list response, we need to only show options that are not 'inactive'
                    if (!empty($allOptionListCache)) {                        // only if there are some options.
                        foreach ($allOptionListCache as $id=>$optionList) {   // loop through each set of options (each <select>)
                            $tempOutput[$id] = [];                            // initiatlize the holding array
                            foreach($optionList as $options)                  // loop through all the <options> in the select
                                if (!$options['inactive'])                    // if not 'inactive' add it to the list
                                    $tempOutput[$id][]=$options;
                        }
                    }
                    if (!empty($tempOutput)) $output["options"] = $tempOutput;  // if we have any options left, put them out.
                } else
                    $output['error'] = $ErrorMessage;
            }

            break;

        case 'create':    // add one or more records
            $errors = caspioDataManager_isError($data,$params);
            if ($errors === false) {
                $allOptionListCache =[];
                $idx = 0;
                foreach ($data as  $m) {           // loop through all the records to add (likely just one)
                    $r=$m['row'];
                    $errorMessage = "";
                    $result = caspioDataManager_addData($config,$r,$params,$errorMessage,$allOptionListCache);
                    if ($result === false) {
                        $output['error'] .= ($idx > 0 ? " | " : "").$errorMessage;
                    } else if ($result <= 0) {
                        $output['error'] .= ($idx > 0 ? " | " : "")."Failure accessing database: ".$result;
                    } else {
                        array_push($output['data'], $result);
                    }
                    $idx++;
                }
                unset($output['errror']);
                unset($output['fieldErrors']);
            } else {
                $output['error'] = $errors['error'];
                $output['fieldErrors'] = $errors['fieldErrors'];
                $output['data'] =[];    // optional to blank out data; can also be unset
            }

            break;

        case 'edit':     // update the record
            $errors = caspioDataManager_isError($data,$params);
            $allOptionListCache = [];
            $output['data'] = [];
            if ($errors === false) {
                $idx = 0;
                foreach ($data as $k => $m) {     // loop through all the records to update  $k is unique key for record
                    $r=$m['row'];
                    $message="";
                    $result = caspioDataManager_updateData($config,$k,$r,$params,$allOptionListCache,$message);
                    if ($result === false) {
                        $output['error'] .= ($idx > 0 ? " | " : "").$message;
                    } else if ($result <= 0) {
                        $output['error'] .= ($idx > 0 ? " | " : "")."Failure accessing database: ".$result;
                    } else {
                        array_push($output['data'], $result);
                    }
                    $idx++;
                }
                // put it in the data key
                unset($output['errror']);
                unset($output['fieldErrors']);
            } else {
                $output['error'] = $errors['error'];
                $output['fieldErrors'] = $errors['fieldErrors'];
                $output['data'] =[];    // optional to blank out data; can also be unset
            }

            break;

        case 'remove':
            $keyValueArray = [];
            foreach ($data as $k => $m) {     // loop through all the records to update  $k is unique key for record
                $r = $m['row'];
                if (!in_array($k,$keyValueArray)) $keyValueArray[]=$k;
            }
            if (!empty($keyValueArray)) {
                $errorMessage = "";
                $result = caspioDataManager_deleteData($config,implode(", ",$keyValueArray),$errorMessage);
                if ($result === false) {
                    $output['error'] = $errorMessage;
                } else {
                    unset($output['errror']);
                    unset($output['fieldErrors']);
                    $output['data'] =[];    // optional to blank out data; can also be unset
                }
            }
            break;

        case 'options':
            $output = caspioDataManager_getOptions($config,$params);
            break;
        default:
            $output['error'] = "Invalid Action given. Contact support@agencymaniasolutions.com.";

    }

    // standard output for dataTable and dataTable Editor;
    if ($action !== 'select') {
        // if (array_key_exists('data', $output) && empty($output['data'])) unset($output['data']);
        if (array_key_exists('options', $output) && empty($output['options'])) unset($output['options']);
        if (array_key_exists('error', $output) && empty($output['error'])) unset($output['error']);
        if (array_key_exists('fieldErrors', $output) && empty($output['fieldErrors'])) unset($output['fieldErrors']);
    }

    return $output;
}
