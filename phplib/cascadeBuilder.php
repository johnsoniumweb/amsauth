<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/phplib.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/REST/caspioAuth.php');   // verifies caspio REST API access

class fieldOBJ {
    public $fieldName = 'tables';
    public $allowALL = false;
    public $ifBlank = "";
    public $ifAll = "-- All --";
}


function buildCascadeList($sourceName, $tableORview,  $fieldARR)
// sourceName: name of the caspio table or view
// tableOrview : 'tables' or 'views' depending on whether it is a caspio table or view
// fieldARR :  This is an array of objects.  The objects should be in order you want them sorted/displayed
//              {fieldName:  string  >>> Name of caspio field;
//                allowALL:  true/false >>> do you want to show "-- All --" for this field?
//
//  returns an array of strings in the format required for cascadeSelect javascript.

{
    global $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $returnARR["Success"] = false;
    $returnARR["Data"] = [];
    $returnARR["Message"] = "";

    // CHECK parameters being passed
    if (!isset($sourceName) || $sourceName == "") $returnARR["Message"] = "Invalid Table or View Name.";
    else if (!isset($tableORview) || ($tableORview != "views" && $tableORview != "tables")) $returnARR["Message"] = "Invalid tableORName parameter.";
    else if (sizeof($fieldARR) == 0) $returnARR["Message"] = "No fields were provided.";

    if ($returnARR["Message"] != "") {
        $returnARR['Success'] = false;
        return $returnARR;
    } else {

        // build comma delimited field list for use in the Caspio API select statement
        $fieldList = "";
        foreach ($fieldARR as $fOBJ) {$fieldList .= $fOBJ->fieldName.', ';}
        $fieldList = substr($fieldList, 0, -2);

        // run the caspio API Query
        $caspio = new Caspio();
        $access_token = $Caspio_token;
        $get_params = array('q' => '{
            "select" : "'.$fieldList.'",
            "orderby" :"'.$fieldList.'",
            "groupby" : "'.$fieldList.'",
            "limit" : "1000"
            }');
        $found_json = $caspio->ExecGetRequest($Caspio_apiURL . "/".$tableORview."/".$sourceName."/rows", $access_token, $get_params);

        // check if an error was returned
        if (!$found_json){
            $returnARR['Success'] = false;
            $returnARR['Message'] = "problem with Query: ".$Caspio_message ;
            return $returnARR;
        }

        // move the field names into a separate array of array of field names
        $comboMainARR = [];
        foreach ($found_json->Result as $r) {
            $comboFieldARR = [];
            foreach ($fieldARR as $fOBJ) {
                array_push($comboFieldARR,$r->{$fOBJ->fieldName});
            }
            array_push($comboMainARR,$comboFieldARR);
        }

        // starting for the last field, loop through all fields passed and if they allowALL to be shown,
        // take a copy of all the values so far in the array and replace that field with "--ALL--" then append
        // the copied array back to the main array. This will create some duplicates that we'll get rid of later.
        $index = count($fieldARR);
        while($index) {
            $index = $index - 1;
            if ($fieldARR[$index]->allowALL) {
                $dupTest = [];
                $copyMasterARR = $comboMainARR;
                foreach ($copyMasterARR as $copy) {
                    $i = -1;
                    foreach ($copy as $item) {                      // loop through each field in the array
                        if(++$i == $index)                          // if our current "ALL" field, replace with -- ALL --
                            $copy[$i] = $fieldARR[$index]->ifAll;       // put in the preferred "ALL" text
                        else if (trim($item) == "")                 // if value is blank
                            $copy[$i] = $fieldARR[$index]->ifBlank;     // put in the preferred "BLANK" text
                    }
                    $imploaded =implode("|",$copy);                 // make sure it's not a duplicate.
                    if (!in_array($imploaded,$dupTest)) {
                        array_push($dupTest,$imploaded);
                        array_push($comboMainARR, $copy);
                    }
                }

            }
        }

        // copy the array of array of field names to an array of strings using the <|> delimiter required
        // of the cascadingSelects function.
        $newARR = [];
        foreach ($comboMainARR as $row) {
            $strVAR = "" ;
            foreach ($row as $col) {
                $strVAR .= $col . '<|>';
            }
            array_push($newARR,substr($strVAR,0,-3));
        }

        // get rid of duplicates
//        $newARR = array_unique($newARR) ;

        // add a unique number as the first filed as required by cascadingSelects JS
        $counter = 0;
        foreach ($newARR as $out)
            array_push($returnARR['Data'], ++$counter."<|>".$out) ;

        $returnARR['Success'] = true;
        return $returnARR;
    }
}

// ---------------
// --------------- Main Process
// ---------------

$return['Success'] = false;
$return['Data'] = [];
$return['Message'] = "";

if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    $return["Message"] = "Invalid Ajax Call: Invalid headers.";
} else if (!isset($_POST['app']) || $_POST['app'] == "" )
    $return["Message"] = "App parameter must be provided.";
else if (!caspioAuth())
    $return["Message"] = "Could not connect to Caspio API.";
else {
    $app = $_POST['app'];
    if ($app == "DELL2016") {
        $fieldARR = [];
        //  ----------------------------------------------------------------------------------------
        //  THIS SECTION NEEDS TO BE CUSTOMIZED BASED ON WHAT FIELDS YOU WANT IN WHAT ORDER
        //  ----------------------------------------------------------------------------------------
        $field = new fieldOBJ(); $field->fieldName = "Assessment"; $field->allowALL = true; array_push($fieldARR,$field);
        $field = new fieldOBJ(); $field->fieldName = "Hdr1"; $field->allowALL = true; $field->ifBlank = "{blank}"; $field->ifAll = "-- All --"; array_push($fieldARR,$field);
        $field = new fieldOBJ(); $field->fieldName = "Hdr2"; $field->allowALL = true; $field->ifBlank = "{blank}"; $field->ifAll = "-- All --"; array_push($fieldARR,$field);
        $field = new fieldOBJ(); $field->fieldName = "Hdr3"; $field->allowALL = true; $field->ifBlank = "{blank}"; $field->ifAll = "-- All --"; array_push($fieldARR,$field);
        $field = new fieldOBJ(); $field->fieldName = "Hdr4"; $field->allowALL = true; $field->ifBlank = "{blank}"; $field->ifAll = "-- All --"; array_push($fieldARR,$field);
        $field = new fieldOBJ(); $field->fieldName = "Col2"; $field->allowALL = true; $field->ifBlank = "{blank}"; $field->ifAll = "-- All --"; array_push($fieldARR,$field);
        $field = new fieldOBJ(); $field->fieldName = "Col3"; $field->allowALL = true; $field->ifBlank = "{blank}"; $field->ifAll = "-- All --"; array_push($fieldARR,$field);
        $return = buildCascadeList("DELL2016_Comments","tables",$fieldARR);
        //  ----------------------------------------------------------------------------------------
    } else if ($app == "YOUR APP NAME HERE") {

    } else {
        $return['Success'] = false;
        $return['Message'] = "App specified does not exist";
    }
}

header('Content-Type: application/json');
echo json_encode($return);