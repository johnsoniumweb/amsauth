<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/getCaspioData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/add2EQM_fn.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/phplib/CaspioDataManager_class.php');

/*
 *  --------------------------------------------------------------------------
 *  Functions used to manage caspio I/O  (especially with Data Tables Editor
 *
 *  To use this several steps are required:
 *  1. the using php file must include appropriate REST caspio API library must be included before this file
 *  2. the using php file must create a config array in the designated format (see sample)
 *  3. the using php file must have a set of specific functions setup (see sample):
 *  --------------------------------------------------------------------------
 */

/**
 * Drop-in replacement for the legacy caspioDataManager
 *
 * @param  array $config
 * @param  array $post
 * @return array
 */
function caspioDataManager($config, $post)
{
    if (session_status() == PHP_SESSION_NONE)
    {
        @session_start();
    }

    try {
        $cdm = new CaspioDataManager($config);
        return $cdm->process($post);
    } catch (Exception $e) {
        // something went wrong with the initialization
        return [ 'error' => $e->getMessage() ];
    }
}
