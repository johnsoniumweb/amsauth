<?php
/**
 * Created by PhpStorm.
 * User: Shaun Wolfe
 * Date: 3/4/2018
 * Time: 11:13 AM
 */

function validateEmailList($list, $delimeter) {
    $badEmailList = array();
    $goodEmailList = array();
    if (isset($list) && trim($list) !=="") {
        if (!isset($delimeter) || trim($delimeter)==="")
            $emailArray[]=$list;
        else
            $emailArray = explode($delimeter,$list);

        foreach($emailArray as $e)
            if (trim($e)!=="")
                if(!filter_var(trim($e), FILTER_VALIDATE_EMAIL))
                    $badEmailList[] = trim($e);
                else
                    $goodEmailList[]=trim($e);
    }
    return array("GoodEmails"=>$goodEmailList,"BadEmails"=>$badEmailList);
}

// unit testing
//$testList = "shaun@agencymaniasolutions.com, shaun@test.com, jo";
//$testDelim = ",";
//echo "List: ".$testList."<br>";
//echo "Delim: ".$testDelim."<br>";
//$data = validateEmailList($testList,$testDelim);
//echo "<br>Good Emails:".implode(", ",$data["GoodEmails"])."<br>";
//echo "Bad Emails:".implode(", ",$data["BadEmails"])."<br>";
//echo "<br><br>";
//
