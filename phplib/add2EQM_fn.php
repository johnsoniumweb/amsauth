<?php
//TO USE THESE FUNCTIONS, INCLUDE A CASPIO AUTH FILE /REST/caspioAuth.php OR /REST/caspioCorpAuth.php) IN THE FILE YOU ARE CALLING FROM.

//EQM_Queue FIELDS
//====REQUIRED===============================================================================
//Recipient         : REQUIRED email address of recipient(s). If multiple, separate by a ;
//Body              : REQUIRED email message. Html is ok.
//CustomerName      : REQUIRED the customer this message is related to (ie. Verizon)
//Source            : REQUIRED the program/page this msg was initiated from. Used for debugging.

//====OPTIONAL===============================================================================
//Subject           : [string] email subject. Default is "New AMS Support Msg".
//Cc                : [string] email address of cc(s). If multiple, separate by a ;
//FromAddress       : [string] email address of sender. Default is support@agencymaniasolutions.com
//FromName          : [string] friendly name of sender. Default is AMS Support
//Attachments       : [file | [| file]] path to attachment. /relative to the root. If multiple, separate by a |
//                          *to use a 'friendly name': add 'friendly' file name + extension after the file and a '?' character. Example:
//                              [file.pdf?Friendly_File1.pdf | file.pdf?Friendly_File2.pdf]
//Remove Attachments: [*false | true] if attachments are included, mark this to have the attachments deleted after the email has been sent.
//UserInitiated     : [string] the email address of the user that initiated the notice. For example, a notice is sent out on a SOW status change. You could put the email address of the user that initiated the status change/resulting notice.
//Status            : ['test'] this field is looked at to see if the msg has been sent/failed/in processe by the job. For testing purposes where you want this email to NOT be sent, set as 'test'. Otherwise leave blank/null.
//
//global;
$returnMsg = '';

function chkEQMFields($fieldArray){
    $msg = '';
    if(!array_key_exists('Recipient', $fieldArray)      || !$fieldArray['Recipient']    || $fieldArray['Recipient'] == '') {
        $msg .= 'Missing required field "Recipient". ';
    }
    if(!array_key_exists('Body', $fieldArray)           || !$fieldArray['Body']         || $fieldArray['Body'] == '') {
        $msg .= 'Missing required field "Body". ';
    }
    if(!array_key_exists('CustomerName', $fieldArray)   || !$fieldArray['CustomerName'] || $fieldArray['CustomerName'] == '')   {
        $msg .= 'Missing required field "CustomerName". ';
    }
    if(!array_key_exists('Source', $fieldArray)         || !$fieldArray['Source']       || $fieldArray['Source'] == '') {
        $msg .= 'Missing required field "Source". ';
    }
    if(array_key_exists('Attachments', $fieldArray) &&      $fieldArray['Attachments']  != '') {
        //loop through all attachments and check see if they exist. If they exist, add them to the email. Error if attachment does not exist.
        $attachments = explode("|", $fieldArray['Attachments']);
        foreach ($attachments as $fileString){
            $fileString = urldecode(trim($fileString));
            $file_wFriendly = explode("?", $fileString); //explode out to see if a 'friendly name' was included

            $file = $file_wFriendly[0]; //the file name with the root path on the server.

            $file = trim($file);
            if(!file_exists($_SERVER['DOCUMENT_ROOT'].$file)) $msg .= 'Invalid Attachment: '.$file.'. Remember to use /pathFromRoot. ';
        }
    }
    return $msg;
}


//is passed an array of fields/values to create a record in EQM_Queue. passes back -1 on fail or the QueKey of the new record. ErrorMsgs stored in global $returnMsg
function add2eqm($fieldArray)
{
    global $returnMsg,  $Caspio_apiURL, $Caspio_token, $Caspio_message;

    $result = -1;

    if(!function_exists('caspioAuth')) $returnMsg = "Caspio auth file not included.";
    else if(caspioAuth()) {
        $caspio = new Caspio();
        $access_token = $Caspio_token;

        $returnMsg = chkEQMFields($fieldArray);
        if ($returnMsg == "") {
            $result = $caspio->ExecInsertRequest($Caspio_apiURL . "/tables/EQM_Queue/rows", $access_token, $fieldArray, "QueKey");
            if ($result === false || $result < 0) $returnMsg .= $Caspio_message;
        }
    } else $returnMsg = "Authorization Failure: ".$Caspio_message;
    $Caspio_message = $returnMsg;  // eventually want to phase out $returnMsg;
    return $result;
}

function alertSupport($application, $subject, $body, $cc="") {
    global $Caspio_account;

    if (session_status() == PHP_SESSION_NONE) session_start();

    // we don't want to reveal the token in an email message so we will remember it/ redact it/ restore it later
    $originalToken1 = "xXx";
    if (isset($_SESSION['caspio_token'])) {
        $originalToken1 = $_SESSION['caspio_token'];
        $_SESSION['caspio_token'] = trim($_SESSION['caspio_token'] == "") ? "[Token is blank]" : "[Token Redacted]";
    }
    $originalToken2 = "xXx";
    if (isset($_SESSION[$Caspio_account.'_caspio_token'])) {
        $originalToken2 = $_SESSION[$Caspio_account.'_caspio_token'];
        $_SESSION[$Caspio_account.'_caspio_token'] = trim($_SESSION[$Caspio_account.'_caspio_token'] == "") ? "[Token is blank]" : "[Token Redacted]";
    }

    // standardized alert output
    $cc = (isset($cc ))? trim($cc) : "";
    $fieldArray = array(
        "Recipient" => "Support@agencymaniasolutions.com",
        "FromAddress" => "Support@agencymaniasolutions.com",
        "FromName" => "Internal Alert: ".$application,
        "Cc" => $cc,
        "Subject" => $subject,
        "Source" => "URI:".$_SERVER['REQUEST_URI'],  // prefix with text to ensure it is not blank as it is required.
        "CustomerName" => "AMS Development",
        "Body" => $body."<br><br>SESSION<br><pre>".print_r($_SESSION,TRUE)."</pre><br><br>SERVER<br><pre>".print_r($_SERVER,TRUE)."</pre>"
    );

    // Restore Token and send message;
    if ($originalToken1 !== "xXx") $_SESSION['caspio_token'] = $originalToken1;
    if ($originalToken2 !== "xXx") $_SESSION[$Caspio_account.'_caspio_token'] = $originalToken2;
    return add2eqm($fieldArray);

}