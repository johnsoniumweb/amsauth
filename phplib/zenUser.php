<?php
// Using JWT from PHP requires you to first either install the JWT PEAR package from
// http://pear.php.net/pepr/pepr-proposal-show.php?id=688 or get the JWT project
// from https://github.com/firebase/php-jwt on GitHub.
include_once $_SERVER['DOCUMENT_ROOT']."/phplib/php-jwt/src/JWT.php";

use \Firebase\JWT\JWT;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
    $currentCookieParams = session_get_cookie_params();
    $sidvalue = session_id();
    setcookie(
        'PHPSESSID',//name
        $sidvalue,//value
        0,//expires at end of session
        $currentCookieParams['path'],//path
        $currentCookieParams['domain'],//domai
        true, //secure
        true // httponl
    );
}


// Log your user in.
$key = "WtKTJGDroei1bwuOC8JKwwlq9kC8kMgxW4bxPrAvaKpmWVdg";
$subdomain = "agencymania";
$now       = time();
$token = array(
    "jti"   => md5($now . rand()),
    "iat"   => $now,
    "name"  => $_SESSION['zenUser_Name'],
    "email" => $_SESSION['zenUser_Email']
//    "role" => "user"
//    "organization" => "Mondelez"
//    "name"  => "Shaun Wolfe",
//    "email" => "shaun@agencymaniasolutions.com"
//    "role" => "agent"
//    "name"  => $user->name,
//    "email" => $user->email
);
$jwt = JWT::encode($token, $key);
$location = "https://" . $subdomain . ".zendesk.com/access/jwt?jwt=" . $jwt;
if(isset($_GET["return_to"])) {
    $location .= "&return_to=" . urlencode($_GET["return_to"]);
} else {
    $location .= "&return_to=" . urlencode("https://agencymania.zendesk.com/hc/en-us/requests");
}
// Redirect
header("Location: " . $location);
