<?php
//need to include caspioAuth.php

if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
    $currentCookieParams = session_get_cookie_params();
    $sidvalue = session_id();
    setcookie(
        'PHPSESSID',//name
        $sidvalue,//value
        0,//expires at end of session
        $currentCookieParams['path'],//path
        $currentCookieParams['domain'],//domain
        true, //secure
        true // httponly
    );
}

function submitLog($logTable, $fieldArray) {
    global $Caspio_apiURL, $Caspio_token;

    $addRecord = array();

    if(caspioAuth()){
        foreach($fieldArray as $field=>$value){
            if(!$value || $value == "")$addRecord[$field] = "Unknown";
            else $addRecord[$field] = $value;
        }

        $caspio = new Caspio();
        $access_token = $Caspio_token;
        return $caspio->ExecInsertRequest($Caspio_apiURL . "/tables/".$logTable."/rows", $access_token, $addRecord);
    } else return false;

}