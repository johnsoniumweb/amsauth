<?php
require_once($_SERVER['DOCUMENT_ROOT']."/REST/caspioCorpAuth.php");   // verifies caspio REST API access
require_once($_SERVER['DOCUMENT_ROOT']."/phplib/add2EQM_fn.php");   // verifies caspio REST API access

//PROGRAM TO ADD A RECORD TO THE EMAIL QUE (CORPORATE CASPIO ACCOUNT) WITH AN AJAX CALL. Can use /js/add2EQM.js for an easy js function to make the ajax call.
$returnMsg = "";

$return["Result"] = -1;
$return["ErrorMsg"]="";

// First Check for proper AJAX Call
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) $return["ErrorMsg"] = "Invalid Ajax Call: Invalid headers.";
else if (!isset($_POST['fieldArray'])) $return["ErrorMsg"] = "Invalid Ajax call: Required data is missing.";
else if(caspioAuth()){

    $fieldArray = json_decode($_POST['fieldArray'],true);
    if (sizeof($fieldArray) == 0) $returnMsg = "Invalid field array.";
    else {
        $returnMsg = chkEQMFields($fieldArray);
        if($returnMsg == "") $return["Result"] = add2eqm($fieldArray);
    }

    $return["ErrorMsg"] = $returnMsg;
} else $returnMsg = "Authorization Failure. Could not establish API link.";


echo json_encode($return);
