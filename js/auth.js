function IeVersion() {
    //Set defaults
    var value = {
        IsIE: false,
        TrueVersion: 0,
        ActingVersion: 0,
        CompatibilityMode: false
    };

    //Try to find the Trident version number
    var trident = navigator.userAgent.match(/Trident\/(\d+)/);
    if (trident) {
        value.IsIE = true;
        //Convert from the Trident version number to the IE version number
        value.TrueVersion = parseInt(trident[1], 10) + 4;
    }

    //Try to find the MSIE number
    var msie = navigator.userAgent.match(/MSIE (\d+)/);
    if (msie) {
        value.IsIE = true;
        //Find the IE version number from the user agent string
        value.ActingVersion = parseInt(msie[1]);
    } else {
        //Must be IE 11 in "edge" mode
        value.ActingVersion = value.TrueVersion;
    }

    //If we have both a Trident and MSIE version number, see if they're different
    if (value.IsIE && value.TrueVersion > 0 && value.ActingVersion > 0) {
        //In compatibility mode if the trident number doesn't match up with the MSIE number
        value.CompatibilityMode = value.TrueVersion != value.ActingVersion;
    }
    return value;
}
function iframeSizer(id, padding) {
    var value = IeVersion();
    if(value.IsIE){
        if (value.TrueVersion == "10" && value.ActingVersion == "10"){
            var isIE10 = true;
        }
    }

    padding = (typeof padding == "undefined")? 30: padding;
    var newHeight = padding;

    var div = document.createElement("div");
    div.innerHTML = "<!--[if lte IE 9]><i></i><![endif]-->";

    var isIe9 = (div.getElementsByTagName("i").length == 1);
    if (isIe9 || isIE10) {
        var dx = document.getElementById(id);
        if (dx) {
            var dy = dx.contentWindow.document.body;
            if (dy) newHeight=document.getElementById(id).contentWindow.document.body.scrollHeight + padding;
        }
    }
    else {
        newHeight =  $("#"+id).contents().find("html").height()+padding;
    }

    $("#"+id).height( newHeight);
}


function roundCells(table, searchChar, roundType, precision){ // must include accounting.js
    precision = typeof precision == 'undefined'? 0: precision;
    var divideby = "";
    if(roundType.toLowerCase() == "k") divideby = 1000;
    else if (roundType.toLowerCase() == "m" || roundType.toLowerCase() == "mm" ) divideby = 1000000;
    else if (roundType.toLowerCase() == "b") divideby = 1000000000;

    if(divideby >= 1000){
        table.find("td").each(function(){
            var thisHtml = $(this).html().trim();
            if(searchChar == "" || thisHtml.indexOf(searchChar) > -1){
                thisHtml = thisHtml.replace(searchChar, "");
                var thisNum = parseFloat(thisHtml.replace(/,/g, ''));
                if(!isNaN(thisNum)){
                    var rounded = accounting.formatMoney((thisNum/divideby) , {symbol: searchChar, precision: precision, decimal: '.', thousand: ',', format: "%s%v"});
                    $(this).html(rounded + roundType);
                }
            }
        });
    }

}

function smartAddTableRecord(tableName, checkFirstsql, addFields, returnField,callback) {
    var jsonVar=false;
    $.ajax({
        type: "POST",
        url: "chgData/addTableRecord.php",
        data: {
            tableName : tableName,
            checkQuery : checkFirstsql.trim() == ""? "": JSON.stringify(checkFirstsql),
            fieldArray : JSON.stringify(addFields),
            keyField : returnField
        },
        // Updates are completed, let's see what happened.
        complete: function(data) {
            jsonVar = $.parseJSON(data.responseText);   // parse the JSON data
            callback(jsonVar["Result"],jsonVar["ErrorMsg"]);
        }
    });
    return jsonVar;
}

function smartGetTableRecords(tableName, getSQL, callback) {
    // REQUIRES JQUERY
    // REQUIRES  getData/getTableRecords.php
    // SAMPLE CALL IN:
    //smartGetTableRecords(
    //    "\\tables\\demoScope_Brands",           // caspio file name
    //    {   select: "BrandKey,BrandCategory",          // query to check if exists (if so, don't add & return records)
    //        where: "BrandCategory = 'Chocolate'"},
    //    function(result, message){          //  call back function for when it's all done.
    //        if (result <0) {
    //            console.log('unexpected error: '+message);
    //        } else if (result == 0) {
    //            console.log('no records found');
    //        } else if (result>0) {
    //            var data = JSON.parse(message).Result;   // Message has an array of data in JSON format.
    //            $.each(data,function(i,row) {
    //                console.log(i,row.BrandKey,row.BrandCategory);
    //            });
    //        }
    //    });
    // Return assumng callback(result, message):
    //  result:
    //            -1 : Unexpected failure, Message contains error text
    //             0 : No records found..
    //            >0 : Number of records returned, values are held as json array in message
    //  message:
    //   error string: if result == -1, the reason will be in this Error Message.
    //          blank: if Result is 0, message is blank.
    //     json Array: if the result > 0, the  message will hold a stringified array of records, each record is a json string of values.
    var jsonVar=false;
    $.ajax({
        type: "POST",
        url: "getData/getTableData.php",
        data: {
            tableName : tableName,
            checkQuery : JSON.stringify(getSQL)
        },
        // Updates are completed, let's see what happened.
        complete: function(data) {
            jsonVar = $.parseJSON(data.responseText);   // parse the JSON data
            callback(jsonVar["Count"],jsonVar["Message"]);
        }
    });
    return jsonVar;
}

function smartChangeTableRecords(tableName, getSQL,fields, callback) {
    // REQUIRES JQUER
    // REQUIRES  chgData/addTableRecord.php
    // SAMPLE CALL IN:
    //smartChangeTableRecords(
    //    "\\tables\\demoScope_Brands",                         // caspio file name
    //    {select: "BrandKey",
    //        where: "BrandCategory = 'Chocolates'"}, // find records
    //    {BrandCategory : "Chocolate"},              // update fields
    //    function(count, message){                   // Callback function.
    //        if (count <0) {
    //            //  Unexpected error with database.
    //            console.log('unexpected error: '+message);
    //        } else if (count == 0) {
    //            // no records found
    //            console.log(message);
    //        } else {
    //            // Records updated
    //            console.log(count + " record(s) were updated.");
    //        }
    //    });
    //
    // SAMPLE RETURN:
    //  given,  function(count, message)
    //  count:
    //            -1 : Unexpected failure, see error message
    //             0 : No records found
    //            >0 : Number of records successfully updated
    //  message
    //          blank: if add was successful.
    //         string: If count == -1, then it's the reason. If Count == 0, then it's 'No Records Found.'
    var jsonVar=false;
    $.ajax({
        type: "POST",
        url: "chgData/chgTableRecords.php",
        data: {
            tableName : tableName,
            findSQL : JSON.stringify(getSQL),
            fieldArray : JSON.stringify(fields)
        },
        // Updates are completed, let's see what happened.
        complete: function(data) {
            jsonVar = $.parseJSON(data.responseText);   // parse the JSON data
            callback(jsonVar["Count"],jsonVar["ErrorMsg"]);
        }
    });
    return jsonVar;
}

function smartDeleteTableRecords(tableName, getSQL, callback) {
    var jsonVar=false;
    $.ajax({
        type: "POST",
        url: "chgData/delTableRecords.php",
        data: {
            tableName : tableName,
            findSQL : JSON.stringify(getSQL)
        },
        // Updates are completed, let's see what happened.
        complete: function(data) {
            jsonVar = $.parseJSON(data.responseText);   // parse the JSON data
            callback(jsonVar["Count"],jsonVar["ErrorMsg"]);
        }
    });
    return jsonVar;
}


function iframeSizer(id, padding) {
    var value = IeVersion();
    if(value.IsIE){
        if (value.TrueVersion == "10" && value.ActingVersion == "10"){
            var isIE10 = true;
        }
    }

    padding = (typeof padding == "undefined")? 30: padding;
    var newHeight = padding;

    var div = document.createElement("div");
    div.innerHTML = "<!--[if lte IE 9]><i></i><![endif]-->";

    var isIe9 = (div.getElementsByTagName("i").length == 1);
    if (isIe9 || isIE10) {
        var dx = document.getElementById(id);
        if (dx) {
            var dy = dx.contentWindow.document.body;
            if (dy) newHeight=document.getElementById(id).contentWindow.document.body.scrollHeight + padding;
        }
    }
    else {
        newHeight =  $("#"+id).contents().find("html").height()+padding;
    }

    $("#"+id).height( newHeight);
}
//function iframeSizer(id, padding) {
//    padding = (typeof padding == "undefined")? 30: padding;
//    if(document.getElementById(id)){
//        var docBody=document.getElementById(id).contentWindow.document.body;
//        if (docBody !== null)
// //           document.getElementById(id).style.height= 'auto';
//        document.getElementById(id).height= docBody.scrollHeight + padding + "px";
//    } }

    function loadDatapage(appKey){
        var resolvedAppKey = (!isProduction) ? appKey : cbTestPrefix + appKey.substring(5);
        try {
              if (cbLoadFormat == 'F2')
                  f_cbload(true, cbProductionDomain, resolvedAppKey);
              else
                  f_cbload(resolvedAppKey, "https:");
        }
        catch(v_e){}
    }
