//  Global variables
var appName = "AMS Authentification";
var productName = "AuthDeliver™";
var appSignature = "AMS Auth";
var appSupport = "Support Provided by: AMS";
var domain = "agencymaniasolutions.com";
var productPath = "/authtest/";
var cbProductionDomain = "c1arw315.caspio.com";
var cbTestDomain = "b6.caspio.com";


// [F1]=b6 style | [F2]=c1arw315 style
var cbProductionLoadFormat = "F2";
var cbTestLoadFormat = "F1";
var cbTestPrefix = "12434";


if(window.location.hostname.indexOf('localhost') !== -1){
    var isProduction = false;
    var rootPath="http://localhost"+productPath;
    var cbLoadFormat = cbTestLoadFormat;
}else if(window.location.hostname.indexOf('.test.') !== -1){
    var isProduction = false;
    var rootPath="https://"+window.location.hostname+productPath;
    var cbLoadFormat = cbTestLoadFormat;
} else {
    var isProduction = true;
    var rootPath= "https://"+domain+productPath;
    var cbLoadFormat = cbProductionLoadFormat;
}
